#ifndef PRCHEATEDSEEDING_H 
#define PRCHEATEDSEEDING_H 1

// Include files
// from Gaudi

//#define DEBUG_HISTO 
//uncomment this line if you want to print monitoring histograms

#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/ISequencerTimerTool.h"

#include "PrKernel/IPrDebugTool.h"
#include "PrKernel/PrHitManager.h"
#include "PrSeedTrack.h"
#include "PrGeometryTool.h"
#include "TfKernel/RecoFuncs.h"

#include "MCInterfaces/IIdealStateCreator.h"
#include "GaudiAlg/GaudiHistoTool.h"

#include "GaudiAlg/IHistoTool.h"

/** @class PrCheatedSeeding PrCheatedSeeding.h
 *  Stand alone seeding for the FT T stations
 *  This code is a hack which represents the code used for the upgrade tracker TDR
 *  It needs to be superseded by a proper implementation!
 *
 * - InputName: Name of the input container for the forward tracks. Set to '""' to not reuse the FT part of the forward tracks.
 * - OutputName: Name of the output container
 * - HitManagerName: Name of the hit manager
 * - DecodeData: Decode the data (default: false, as normally done in the Forward Tracking)
 * - XOnly: Only reconstruct tracks with the x-layers?
 * - MaxChi2InTrack: Max Chi2 value in internal track fit.
 * - TolXInf: Lower bound for search "road"
 * - TolXSup: Upper bound for search "road"
 * - MinXPlanes: Minimum number of x-planes a track needs to have
 * - MaxChi2PerDoF: Maximum Chi2/nDoF a track can have.
 * - MaxParabolaSeedHits: Maximum number of hits which are use to construct a parabolic search window.
 * - TolTyOffset: Tolerance for the offset in y for adding stereo hits.
 * - TolTySlope: Tolerance for the slope in y for adding stereo hits.
 * - MaxIpAtZero: Maximum impact parameter of the track when doing a straight extrapolation to zero. Acts as a momentum cut.
 * - DebugToolName: Name of the debug tool
 * - WantedKey: Key of the particle which should be studied (for debugging).
 * - TimingMeasurement: Do timing measurement and print table at the end (?).
 * - PrintSettings: Print all values of the properties at the beginning?
 *
 *  @author Olivier Callot
 *  @date   2013-02-14
 *  2014-06-26 : Yasmine Amhis Modification
 *  2014-02-12 : Michel De Cian (TDR version) 
 */

class PrCheatedSeeding : public GaudiHistoAlg {
public:
  /// Standard constructor
  PrCheatedSeeding( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~PrCheatedSeeding( ); ///< Destructor

  virtual StatusCode initialize();    ///< Algorithm initialization
  virtual StatusCode execute   ();    ///< Algorithm execution
  virtual StatusCode finalize  ();    ///< Algorithm finalization

 

protected:
  
 
  void makeLHCbTracks( LHCb::Tracks* result );
  
  /** @brief Print some information of the hit in question
   *  @param hit The hit whose information should be printed
   *  @param title Some additional information to be printed
   */
 
  
 
  
  
private:
  std::string     m_inputName;
  std::string     m_outputName;
  std::string     m_hitManagerName;
  float           m_maxChi2InTrack;
  float           m_maxIpAtZero;
  float           m_tolXInf;
  float           m_tolXSup;
  unsigned int    m_minXPlanes;
  float           m_maxChi2PerDoF;
  bool            m_xOnly;
  unsigned int    m_maxParabolaSeedHits;
  
  float           m_tolTyOffset;
  float           m_tolTySlope;
  
  bool            m_decodeData;
  bool            m_printSettings;
  

  PrHitManager*   m_hitManager;
  PrGeometryTool* m_geoTool;

  //== Debugging controls
  std::string     m_debugToolName;
  int             m_wantedKey;
  IPrDebugTool*   m_debugTool;

  std::vector<PrSeedTrack>       m_trackCandidates;
  std::vector<PrSeedTrack>       m_xCandidates;

  bool           m_doTiming;
  ISequencerTimerTool* m_timerTool;
  int            m_timeTotal;
  int            m_timeFromForward;
  int            m_timeXProjection;
  int            m_timeStereo;
  int            m_timeFinal;

  const IHistoTool* m_histoTool;
  GaudiHistoTool* m_ghtool;

  /// The ideal state creator, which makes a state out of an MCParticle
  IIdealStateCreator* m_idealStateCreator;

  int m_diffNumIDs;
  


};
#endif // PRSEEDINGALGORITHM_H
