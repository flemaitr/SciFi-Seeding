// Include files 

// from Gaudi
#include "GaudiKernel/ToolFactory.h" 
#include "Event/FTLiteCluster.h"
#include "GaudiKernel/IRndmGenSvc.h"
// local
#include "PrFTHitManager.h"

//#include  "Kernel/FTChannelID.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrFTHitManager
//
// 2012-03-13 : Olivier Callot
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( PrFTHitManager )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrFTHitManager::PrFTHitManager( const std::string& type,
                                const std::string& name,
                                const IInterface* parent ):PrHitManager ( type, name , parent ),
  m_geometryBuilt(false)
{
  //Tuples::Tuple tuple = GaudiTupleAlg::nTuple("TestFTHitManager", "Events");
  declareInterface<PrFTHitManager>(this);
  declareProperty( "XSmearing",   m_xSmearing = -1. );
  declareProperty( "ZSmearing",   m_zSmearing = -1. );
  declareProperty( "fracPosOffset" , m_fracOffset = 0.125 );
  declareProperty( "XerrOffset" , m_XerrOffset = 0.05 );
  declareProperty( "coeffClusterSize" , m_coeffCluster  = 0.03); 
  declareProperty( "printSettings" , m_printSettings = true);
  declareProperty( "doTuple", m_doTuple = false);
  declareProperty( "HackSize1" , m_hackSize1 = false);
  declareProperty( "FixError" , m_fixError = false);
  declareProperty( "SizeFix" ,m_SizeFixed = 0);
  m_Cluster =0;
}
//=============================================================================
// Destructor
//=============================================================================
PrFTHitManager::~PrFTHitManager() {}

//=========================================================================
//  Create the zones in the hit manager, 'zone' is a method of the base class
//=========================================================================
void PrFTHitManager::buildGeometry ( ) {  
  always()<<"Building Geometry"<<endmsg;
  always() << "========================================"             << endmsg
           << " XerrOffset           = " <<  m_XerrOffset            << endmsg
           << " CoeffCluster         = " <<  m_coeffCluster          << endmsg
           << " fracOffset           = " <<  m_fracOffset            << endmsg
           << " doTuple              = " <<  m_doTuple               << endmsg
           << " HackSize1            = " <<  m_hackSize1             << endmsg
           << " FixErrors            = " <<  m_fixError              << endmsg
           << " SizeFixed            = " <<  m_SizeFixed             << endmsg;
  
  
  // -- only build geometry once.
  if(m_geometryBuilt)
  {
    always()<<"Geometry alredy Build"<<endmsg;
    return;
  }
  
  IRndmGenSvc* randSvc = svc<IRndmGenSvc>( "RndmGenSvc", true );
  StatusCode sc = m_gauss.initialize( randSvc, Rndm::Gauss( 0., 1. ) );
  if ( sc.isFailure() ){
    error() << "Could not initialize Rndm::Gauss generator" << endmsg;
    return;
  }
  m_ftDet = getDet<DeFTDetector>( DeFTDetectorLocation::Default );
  //version 20 is the detector with monolayer and fibremat structure, including the dead regions
  if ( msgLevel( MSG::DEBUG) )  debug() << "DETECTOR VERSION: " << m_ftDet->version() << endmsg;
  always() <<"FT HIT MANAGER: DETECTOR VERSION: " <<m_ftDet->version()<<endmsg;
  
  DetectorSegment segUp;
  DetectorSegment segDown;
  
  for ( std::vector<DeFTLayer*>::const_iterator itL = m_ftDet->layers().begin();  //loop over layers
        m_ftDet->layers().end() != itL; ++itL ){
    
    int id = (*itL)->params()->param<int>("layerID");  //ask layerID
    
    info()<<"Building the geometry of Layer # "<<id<<endmsg;
    //Constructor of FTChannelID (layer, module, mat, sipmId, sipmCell)
    
    LHCb::FTChannelID tmpUp( id, 0u, 0u, 0u, 0u ); //"0u" means "unsigned 0" //mat 0 is TOP 
    const DeFTFibreMat* ftMatUp = m_ftDet->findFibreMat(tmpUp);
    segUp = ftMatUp->createDetSegment( tmpUp, 0. );
    LHCb::FTChannelID tmpDown( id, 0u , 1u , 0u , 0u);//Mat 1 is Bottom
    const DeFTFibreMat* ftMatDown = m_ftDet->findFibreMat(tmpDown);
    segDown = ftMatDown->createDetSegment( tmpDown, 0.);
    zone( 2*id   )->setGeometry( segUp);
    zone( 2*id+1 )->setGeometry( segDown);
    //or methods that are indirectly using them, like dxOnAFibre() or isInside(x,y) 
    //(see https://svnweb.cern.ch/trac/lhcb/browser/Rec/trunk/Pr/PrKernel/PrKernel/PrHitZone.h?rev=164716).
    zone( 2*id+1 )->setBoundaries( -4090, 4090., -3030., 50.0); //check these boudaries for zone down (with new FTChannelID)
    zone( 2*id   )->setBoundaries( -4090., 4090., -50.0, 3030. ); //check these boudaries for zone up (with new FTChannelID)
    
    if ( msgLevel( MSG::DEBUG) ) debug() << "Layer " << id << " Zone " << 2*id << " z " << zone(2*id)->z() 
                                         << " angle " << zone(2*id)->dxDy() << endmsg;
    // debug()<< "Layer " << id << " ZoneUp " << 2*id << " z " << zone(2*id)->z()          
    //         << " angle(dxDy) " << zone(2*id)->dxDy() << endmsg;
    debug()<<"Zone =\t " << 2*id<< "\t Layer =\t"<< id <<"\t z(0) = \t"<<zone(2*id)->z(0.)
            <<"Zone =\t " << 2*id+1<<"\t Layer = \t"<<id <<"\t z(0) = \t"<<zone(2*id+1)->z(0.)
            <<endmsg;
      }
  if ( msgLevel( MSG::DEBUG) ) debug() << "XSmearing " << m_xSmearing << " ZSmearing " << m_zSmearing << endmsg;
  m_geometryBuilt = true;
  
}


//=========================================================================
//  Decode an event.
//=========================================================================
void PrFTHitManager::decodeData ( ) {
  
  if ( msgLevel( MSG::DEBUG) ) debug() << "I AM IN DECODEDATA " << endmsg;
  
  typedef FastClusterContainer<LHCb::FTLiteCluster,int> FTLiteClusters;
  FTLiteClusters* clus = get<FTLiteClusters>( LHCb::FTLiteClusterLocation::Default );
  //  debug << " Retrieved " << clus->size() <<"clusters" <<endmsg;
  
  if ( msgLevel( MSG::DEBUG) ) debug() << "Retrieved " << clus->size() << " clusters" << endmsg;
  const DeFTFibreMat* ftMat = nullptr;
  const DeFTFibreMat* anaFtMat = nullptr;
  unsigned int oldFibreMatID = 99999999;
  float zDisplacement = 0.;
  DetectorSegment seg ;
  float fraction = -100;
  //Loop over Clusters
  for ( FTLiteClusters::iterator itC = clus->begin(); clus->end() != itC; ++itC ) {
    anaFtMat = m_ftDet->findFibreMat( (*itC).channelID() );
    if(anaFtMat->FibreMatID() != oldFibreMatID)  { 
      oldFibreMatID =  anaFtMat->FibreMatID();
      ftMat =  anaFtMat; 
      if ( nullptr == ftMat ) {
        always() << "FiberMat not found for FT channelID " << (*itC).channelID() << endmsg; 
        oldFibreMatID = 99999999;   
      }
    }
    
    fraction = (*itC).fraction() + m_fracOffset; //0.125;
    
    // if(m_hackSize1 && (*itC).size()==1 && (*itC).fraction()==0 )
    // {
    //   fraction = (*itC).fraction()+0.5;
    // }
    
    LHCb::FTChannelID id = (*itC).channelID();
    seg = ftMat->createDetSegment( id, fraction );   //create detector segment
    
    
    // PrintOut when Association of cluster and ftMat is not correct
    if(id.mat()!=ftMat->isBottom())
      always()<<"Mismatched ChannelID with FTFibreMat due to mat"<<endmsg;
    if((unsigned)id.module()!=(unsigned)ftMat->module())
      always()<<"Missmatched Module"<<endmsg;
    if((unsigned)id.quarter()!=(unsigned)ftMat->quarter())
      always()<<"Mismatched quarter"<<endmsg;
    if((unsigned) id.layer()!= (unsigned)  ftMat->layer())
      always()<<"Mismatched layer"<<endmsg;
    
    //== Add some smearing if needed
    if ( m_xSmearing > 0. || m_zSmearing > 0. ) {
      always()<<"WARNING !!! You are smearing"<<endmsg;
      float x0 = seg.x(0.);
      float z0 = seg.z(0.);
      float dxDy = 0.001 * ( seg.x(1000.)-seg.x(0.) );
      float dzDy = 0.001 * ( seg.z(1000.)-seg.z(0.) );
      float yMin = seg.yMin();
      float yMax = seg.yMax();
      
      if ( m_xSmearing > 0. ) x0 = x0 + m_xSmearing * m_gauss();
      if ( m_zSmearing > 0. ) z0 = z0 + zDisplacement;
      DetectorSegment tmp( x0, z0, dxDy, dzDy, yMin, yMax );
      seg = tmp;
    }
    int lay  = (*itC).channelID().layer();  
    int Zone = ( (*itC).channelID().mat() > 0 ) ? 1 : 0;  //if yes, it is bottom (now bottom is 1)
    int code = 2*lay + Zone; //here code = 0,2,4,6,8,10 for Mat == 0 (UP)
    PrHit* aHit = newHitInZone( code );

    if (m_fixError == false)
    {
      float errX = m_XerrOffset + m_coeffCluster * (*itC).size(); 
      //ad hoc stuff which has to recover the difference between supposed real data and the clusterization simulation
      aHit->setHit( LHCb::LHCbID( (*itC).channelID() ), (*itC).size(), (*itC).charge(), seg, errX , Zone, lay );
    }
    
    if(m_fixError == true)
    {
      float errX = m_XerrOffset + m_coeffCluster * m_SizeFixed;
      //ad hoc stuff which has to recover the difference between supposed real data and the clusterization simulation  
      aHit->setHit( LHCb::LHCbID( (*itC).channelID() ), m_SizeFixed, (*itC).charge(),seg, errX , Zone, lay );
    }
    
    if ( msgLevel( MSG::DEBUG) ) debug() << " .. hit " << (*itC).channelID() 
                                         << " zone " << Zone << " x " << seg.x(0.) << endmsg;
  } 
  for ( unsigned int lay = 0; nbZones() > lay ; ++lay ) {
    std::sort( zone(lay)->hits().begin(), zone(lay)->hits().end(), PrHit::LowerByCoord() );
  }
    //always()<<"****Number of Read Clusters = "<< m_Cluster <<endmsg;
}


//=============================================================================
