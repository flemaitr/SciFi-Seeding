
// Include files
// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
#include "Event/Track.h"
#include "Event/StateParameters.h"
#include "Math/CholeskyDecomp.h"
// local
#include "PrSeedingNew.h"
#include "PrPlaneCounter.h"
#include "Event/MCTrackInfo.h"
#include "PrPlaneCounter.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Linker/LinkedTo.h"
#include "Linker/LinkedFrom.h"
#include "Linker/AllLinks.h"
#include "Event/FTCluster.h"
#include "Event/FTLiteCluster.h"
#include "Event/MCParticle.h"
//-----------------------------------------------------------------------------
// Implementation file for class : PrSeedingNew
//
// 2015-03-11 : renato quagliani
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( PrSeedingNew )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================


PrSeedingNew::PrSeedingNew( const std::string& name,
                            ISvcLocator* pSvcLocator)
: 
#ifdef TRUTH_MATCH
GaudiTupleAlg( name , pSvcLocator ) ,
#else
  GaudiAlgorithm( name, pSvcLocator) ,
#endif
  m_hitManager(nullptr)
  ,m_geoTool(nullptr)
  ,m_debugTool(nullptr)
  ,m_timerTool(nullptr)
  ,m_zones(24)
{
  declareProperty( "InputName" ,          m_inputName= LHCb::TrackLocation::Forward);
  declareProperty( "OutputName",         m_outputName=LHCb::TrackLocation::Seed);
  declareProperty( "HitManagerName",  m_hitManagerName= "PrFTHitManager");
  declareProperty( "DecodeData",        m_decodeData= false);
  declareProperty( "XOnly",                  m_xOnly= true);
  declareProperty( "MinXPlanes",          m_minXPlanes = 4);
  declareProperty( "NCases" ,              m_nCases = 3); //Max
  declareProperty( "TimingMeasurement",  m_doTiming= false);
  declareProperty( "PrintSettings",        m_printSettings= true);
  declareProperty( "UseCubicCorrection", m_useCubic = true);
  declareProperty( "RemoveClones" ,   m_removeClones = false); // to be studied how to do it
  declareProperty( "RemoveClonesX",   m_removeClonesX = true);
  declareProperty( "FlagHits",               m_FlagHits = false);//to be studied how to do it
  declareProperty( "RemoveFlagged",          m_removeFlagged = false);// to be studied
  declareProperty( "SizeToFlag" ,   m_SizeFlag = 10);
  
  //--------------------X-Search Parametrisation (now High momentum Default)
  //1st / Last Layer search windows
  declareProperty( "HighPAlpha_Tight", m_alphaCorrection_Tight = 2900.0 *Gaudi::Units::mm);
  declareProperty( "TolHp_Tight" , m_TolHp_Tight = 80 *Gaudi::Units::mm);
  declareProperty( "HighPAlpha",    m_alphaCorrection = 120.64 *Gaudi::Units::mm); //L0 Selection tight
  declareProperty( "TolHp",           m_TolHp = 250.0 *Gaudi::Units::mm); //L0 selection
  //Add of third hit in T2
  declareProperty( "x0Corr",           m_x0Corr = 0.0023); //L1 selection
  declareProperty( "m1",                m_m1 = 0.); //L1 selection Bending 1
  declareProperty( "m2",                m_m2 = 0.); //L1 selection Bending 2
  declareProperty( "x0Offset",        m_x0Offset =0.); //L1 selection x0Offset
  declareProperty( "yOff1",             m_yOff1 = +3.0); //L1 selection tollerance
  declareProperty( "yOff1_Loose",   m_yOff1_Loose = +6.0);
  declareProperty( "yOff2",             m_yOff2 = -3.0); //L1 selection  tollerance
  declareProperty( "yOff2_Loose",   m_yOff2_Loose = -6.0);
  declareProperty( "maxParabolaSeedHits", m_maxParabolaSeedHits = 8);
  //Look up in remaining X layers
  declareProperty( "maxDelta_Par",     m_maxDeltaPar = 2.0 *Gaudi::Units::mm); //L2 selection
  declareProperty( "maxDelta_Par_Loose", m_maxDeltaPar_Loose = 1.2);
  //Look up in remaining X layers and Track Fitting  X
  declareProperty( "dRatio0",              m_dRatio0= -0.000246); 
  declareProperty( "dRatio1",              m_dRatio1= -4.68e-10);
  declareProperty( "dRatio2",              m_dRatio2= -1.25e-11);
  declareProperty( "CConst" ,             m_ConstC = 2.458e8); //Backward Projection
  //Cut for MaxChi2 vs X0
  declareProperty( "HitSigmaOffCut_X" ,  m_nSigmaOffX= 3);
  declareProperty( "X0Max_X" ,         m_X0Max = 650 *Gaudi::Units::mm);
  //--------------------UV search  Parametrisation (inherit from PrSeedingXLayers ( to be modified)
  declareProperty("TolCoord",            m_coord = 0.005);
  declareProperty("RemoveHole",          m_removeHole = false);
  //---Added
  declareProperty("AlphaLinearUV"   ,     m_alphaUV = 2.0);
  declareProperty("DoAsymm"       ,  m_doAsymmUV = false);
  //---
  declareProperty( "TolTyOffset",       m_tolTyOffset          = 0.002);
  declareProperty( "TolTySlope",        m_tolTySlope          = 0.015);
  declareProperty( "TolXStereo",         m_tolXStereo         = 2700.* Gaudi::Units::mm      );
  declareProperty( "Force12" ,              m_force12 = false);
  declareProperty( "TriangleFix" ,        m_useFix               = true);
  declareProperty( "TriangleFix2nOrder",  m_useFix2ndOrder = false);
  declareProperty( "TolTriangle",        m_tolTriangle          = 10.* Gaudi::Units::mm        );
  declareProperty( "TolYOffset",         m_tolYOffset           = 100000.* Gaudi::Units::mm    );
  declareProperty( "maxChi2HitsX", m_maxChi2HitsX = 5.5);
  declareProperty( "maxChi2PerDoF",  m_maxChi2PerDoF = 8.5);
  //-------------------Clone Removal Settings
  declareProperty( "minNCommonX",     m_nCommonX = 2); //for clone removal
  declareProperty( "minNCommonUV",     m_nCommonUV = 8); //for clone removal
  declareProperty( "nUsed",               m_nUsed = 1);
  //-------------------Flag Hits Settings
  declareProperty( "Flag_MaxChi2" ,      m_MaxChi2Flag = 1.0);
  declareProperty( "Flag_MaxX0"  ,         m_MaxX0Flag = 50 *Gaudi::Units::mm);
  
  //-------------------TruthMatching Setting
  declareProperty( "DoHistos",         m_doHistos = false);
  declareProperty( "DoTuple",          m_doTuple = true);
  declareProperty( "DoAssociation",  m_doAssociation = true);
  declareProperty( "UseSeedTrack", m_UseSeed = true);
  declareProperty( "UseLong" ,        m_UseLong = true);
  declareProperty( "UseDown" ,       m_UseDown = true);
  declareProperty( "NoElectrons" ,    m_NoElectrons= true);
  declareProperty( "MinPCut" ,         m_PCut = 5000. *Gaudi::Units::MeV);
  declareProperty( "Cheat" ,           m_Cheat = false);
  declareProperty( "MinPurity" ,       m_Purity = 0.79);
  declareProperty( "MinEfficiency",   m_Efficiency = 0.79);
  
  // Parameters for debugging
}
//=============================================================================
// Destructor
//=============================================================================
PrSeedingNew::~PrSeedingNew() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrSeedingNew::initialize() {
#ifdef TRUTH_MATCH
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
#else 
  StatusCode sc = GaudiAlgorithm::initialize();
#endif
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  m_hitManager= tool<PrHitManager>( m_hitManagerName );
  m_geoTool = tool<PrGeometryTool>("PrGeometryTool");
  m_hitManager->buildGeometry();
  m_debugTool = 0;
  if ( "" != m_debugToolName ) {
    m_debugTool = tool<IPrDebugTool>( m_debugToolName );
    info()<<"Debug tool "<<m_debugToolName<<" loaded."<<endmsg;
  } else {
    m_wantedKey = -100;  //no debug
  }  
  if ( m_doTiming) {
    m_timerTool = tool<ISequencerTimerTool>( "SequencerTimerTool/Timer", this );
    m_timeTotal   = m_timerTool->addTimer( "PrSeeding total" );
    m_timerTool->increaseIndent();
    m_timeFromForward = m_timerTool->addTimer( "Convert Forward" );
    m_timeXProjection = m_timerTool->addTimer( "X Projection" );
    m_timeStereo      = m_timerTool->addTimer( "Add stereo" );
    m_timeFinal       = m_timerTool->addTimer( "Convert tracks" );
    m_timerTool->decreaseIndent();
  }
  if( m_decodeData ) info() << "Will decode the FT clusters!" << endmsg;
  if( m_FlagHits)  info()<<"Will Flag the Hits" << endmsg;
  if( m_removeFlagged) info()<<"Will Not re-use Flagged"<<endmsg;
  
  // if( m_inputName == "") info()<<"Standalone Seeding"<<endmsg;
  // if( !(m_inputName == "")) info()<<"Forward tracks as input"<<endmsg;
  if( m_removeClones) info()<<"Will Remove Clones"<<endmsg;
  if( m_xOnly) info()<<"Will use Only X Layers" <<endmsg;
  if( m_useFix && !m_xOnly) info()<<"WIll Use the triangle Fix"<<endmsg;
  
  if( m_printSettings){
    info() <<"======================================="<<endmsg
           << "===============GLOBAL SETTINGS============"<<endmsg
           <<" InputName             = "    <<  m_inputName             << endmsg
           << " OutputName           = "    <<  m_outputName           << endmsg
           << " HitManagerName       = "    <<  m_hitManagerName     << endmsg
           << " DecodeData           = "    <<  m_decodeData          << endmsg
           << " XOnly                = "    <<  m_xOnly                    << endmsg
           << " MinXPlanes           = "    <<  m_minXPlanes            << endmsg
           << " NCases                    = "     << m_nCases                  << endmsg
           << " TimingMeasurement  = "    <<  m_doTiming              << endmsg
           << " UseCubicCorrection   = "    <<  m_useCubic               << endmsg
           << " RemoveClones         = "    << m_removeClones         << endmsg 
           << " RemoveClonesX       = "<<m_removeClonesX << endmsg
           <<" FlagHits                   = "     << m_FlagHits                 << endmsg
           << " RemoveFlagged             = "<<m_removeFlagged <<endmsg
           << "=================FindX Layer Settings=========="<<endmsg
           << " 1st - Last Layer Alpha Rotation  = "<< m_alphaCorrection <<endmsg
           << " 1st - Last Layer Search Window = " << m_TolHp << endmsg
           << " Third Hit T2 search x0 Correction = " << m_x0Corr << endmsg
           << " Third Hit T2 symm search Window  = " << m_yOff1 << endmsg
           << " Max Parabola Seed Hits  = " << m_maxParabolaSeedHits<<endmsg
           << " LookUp Remaingin X Layers MaxDelta = " << m_maxDeltaPar<<endmsg
           << " dRatiio_X 0           = " << m_dRatio0<<endmsg
           << " dRatio_X  1 (linCorr) = "<< m_dRatio1<<endmsg
           << " dRatio_X  2 (quadCorr)= "<<m_dRatio2<<endmsg
           << " CConst BackProj       = "<<m_ConstC<<endmsg
           << "*****Fit X Setting******"<<endmsg
           << " N Sigma HitOffCut     = "<<m_nSigmaOffX <<endmsg
           << " X0 Max                = "<<m_X0Max <<endmsg
           << "=================ADD UV Layer Settings=========="<<endmsg
           << "doAsymmUV          = "<<m_doAsymmUV <<endmsg
           << "AlphaAsymm         = "<<m_alphaUV <<endmsg
           << "TolCoord               = " <<m_coord<<endmsg
           << "TolTyOffset            = " <<  m_tolTyOffset           << endmsg
           << "TolTySlope             = " <<  m_tolTySlope            << endmsg
           << "TolXStereo             = " <<m_tolXStereo             << endmsg
           << "Use Triangle Fix       = " <<m_useFix                  << endmsg
           << "Tol Triangle           = "  << m_tolTriangle          << endmsg
           << "TolYOffset             = "  << m_tolYOffset           << endmsg
           << "*******XY Fit Settings"<<endmsg
           << "MaxChi2HitsX           = " <<m_maxChi2HitsX<<endmsg
           << "MaxChi2PerDoF          = " <<m_maxChi2PerDoF <<endmsg
           << "==================Clone Removal and Flag Hits Settings=========="<<endmsg
           << "Min Hits Commons X     = " <<m_nCommonX <<endmsg
           << "Min Hits Commons UV    = " <<m_nCommonUV <<endmsg
           << "Number Used            = " <<m_nUsed <<endmsg
           << "Flag_MaxChi2           = " << m_MaxChi2Flag<<endmsg
           << "Flag_MaxX0             = " <<m_MaxX0Flag <<endmsg
           << "===================TRUTH MATCHING SETTINGS==============="<<endmsg
           << " DoHistos              = "
           << " DoTuple               = "    <<  m_doTuple           << endmsg
           << " Cheat                 = "   <<  m_Cheat               <<endmsg
           << " DoAssociation         = "   << m_doAssociation      <<endmsg
           << " UseSeedTracks         = "      <<m_UseSeed<<endmsg
           << " UseLongTracks         = "      <<m_UseLong<<endmsg
           << " UseDownstreamTracks   = " <<m_UseDown<<endmsg
           << " RemoveElectrons       = "    <<m_NoElectrons<<endmsg
           << " Momentum P Cut        = "      <<m_PCut <<endmsg
           << " Min Efficiency        = "         <<m_Efficiency <<endmsg
           << " Min Purity            = "          <<m_Purity<<endmsg
           << "===================================================="<< endmsg;
  }
#ifdef TRUTH_MATCH
  setHistoTopDir("FT/");
#endif
  m_timer = 0;
  m_ctimer = 0;
  m_counter = 0;
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PrSeedingNew::execute() {
#ifdef TRUTH_MATCH
  using namespace Tuples ;
#endif
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  // Increment timer counter
  m_counter++;

  if ( m_doTiming ) 
  {
    m_timerTool->start( m_timeTotal );
    m_timerTool->start( m_timeFromForward );
  }
  LHCb::Tracks* result = new LHCb::Tracks();
  put( result, m_outputName );
  if( m_decodeData ){
    debug()<<"Decoding Data"<<endmsg;
    m_hitManager->decodeData();
    debug()<<"Decoding Done"<<endmsg;
  }
  int multiplicity[24];
  int multiplicityTot = 0;
  char zoneName[100];
  // UNFLAG USED TO ALL HITS
  debug()<<"UNFLAGGING ALL HITS"<<endmsg;
  for ( unsigned int zone = 0; m_hitManager->nbZones() > zone; ++zone ) {
    multiplicity[zone]=0;
    sprintf (zoneName, "Multiplicity_InZone_%i",zone);
    for ( PrHits::const_iterator itH = m_hitManager->hits( zone ).begin();
          m_hitManager->hits( zone ).end() != itH; ++itH ) {
      (*itH)->setUsed( false );
      multiplicity[zone]++;
      multiplicityTot++;
    }
  }
  
  //========================================================
  // Remove Seed segment if we pass the  Forward as Input
  //========================================================
  
  if ( "" != m_inputName){
    debug()<<"Removing Seed Segment from Forward Tracking"<<endmsg;
    for(int i = 0; i < 24; i++){
      PrHitZone* zone = m_hitManager->zone(i);
      std::stable_sort( zone->hits().begin(),  zone->hits().end(), compLHCbID());
    }
    LHCb::Tracks* forward = get<LHCb::Tracks>( m_inputName );
    //Loop over forward tracks container
    for ( LHCb::Tracks::iterator itT = forward->begin(); forward->end() != itT; ++itT ) {
      //Vector of LHCbID
      std::vector<LHCb::LHCbID> ids;
      ids.reserve(20);
      // Loop over LHCbIDs of the forward track
      for ( std::vector<LHCb::LHCbID>::const_iterator itId = (*itT)->lhcbIDs().begin();
            (*itT)->lhcbIDs().end() != itId; ++itId ) {
        if ( (*itId).isFT() && (*itId).ftID().layer() < 12 ) {
          LHCb::FTChannelID ftId =(*itId).ftID();
          int zoneNb = 2 * ftId.layer() + ftId.mat(); //zones top are even (0, 2, 4, ....,22)  and zones bottom are odd
          //Load the PrHitZone
          PrHitZone* zone = m_hitManager->zone(zoneNb);
          
          // -- The hits are sorted according to LHCbID, we can therefore use a lower bound to speed up the search
          
          PrHits::iterator itH = std::lower_bound(  zone->hits().begin(),  zone->hits().begin(), *itId, lowerBoundLHCbID() );
          
          for ( ; zone->hits().end() != itH; ++itH ) {
            if( *itId < (*itH)->id() ) break;
            if ( (*itH)->id() == *itId ) (*itH)->setUsed( true );
          }
          ids.push_back( *itId );
        }
      }
      
      // Forward output loaded to TTrack container with History flag
      LHCb::Track* seed = new LHCb::Track;
      seed->setLhcbIDs( ids );
      seed->setType( LHCb::Track::Ttrack );
      seed->setHistory( LHCb::Track::PrSeeding );
      seed->setPatRecStatus( LHCb::Track::PatRecIDs );
      seed->addToStates( (*itT)->closestState( 9000. ) );
      result->insert( seed );
    }
    
   
    
    //=======================================
    // Sort hits according to x for each zone
    //=======================================
    for(int i = 0; i < 24; i++){
      PrHitZone* zone = m_hitManager->zone(i);
      std::stable_sort( zone->hits().begin(),  zone->hits().end(), compX());
    }
  }
  //=======================================
  // Fill zones
  //=======================================
  m_zones.clear();
  for(int i = 0; i < 24; i++){
    m_zones[i] = m_hitManager->zone(i);
  }
  //==========================================================
  //END FLAGGING HITS FROM FORWARD
  //==========================================================
  // Hits are ready to be processed
  //==========================================================
#ifdef TRUTH_MATCH
  //It creates the Map to do the association LHCBID --> vector<MCPARTICLE*>
  //the m_map object is filled with Key = LHCBIS , Value = vector<MCPARTICLE*>
  if(m_doAssociation)
    {
  createMap();
    }
#endif
  
  m_trackCandidates.clear();
  if ( m_doTiming ) {
    m_timerTool->stop( m_timeFromForward );
  }
  //========================================================
  //------------------MAIN SEQUENCE IS HERE-----------------
  //========================================================
  double time = static_cast<double>(getRealTime());
  double ctime = static_cast<double>(rdtsc());
  // ----- Loop through lower and upper half
  for ( unsigned int part= 0; 2 > part; ++part ){
    
    //----- Loop For difference Cases 
    for (unsigned int icase = 0; m_nCases>icase;++icase)
    {  
      findXProjectionsHighMomentum( part,icase);
      if(m_removeClonesX) removeClonesX(m_nCommonX);
      if ( ! m_xOnly ) addStereo2( part, icase ); //to be written (method to addStereoLayers)
      
      //Remove Clones at the end of each single case?
      
      if(m_removeClones) removeClones(m_nCommonUV);
      
      //Flag found Hits at the end of each single case?
      
      if (m_FlagHits && (icase ==0 || icase ==1))
        flagHits(icase);
      
    }//end Case Loop 
    
    //if not done before do it here?
    //if( m_removeClones)
    //{
    //removeClones();
    //}
  }
  m_ctimer += static_cast<double>(rdtsc()) - ctime;
  m_timer += static_cast<double>(getRealTime()) - time;
  // Convert to LHCb Tracks
  debug()<<"Making LHCb Tracks"<<endmsg;
  makeLHCbTracks( result );
  debug()<<"Making LHCb Tracks Done"<<endmsg;
#ifdef TRUTH_MATCH
  m_map.clear();
#endif
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode PrSeedingNew::finalize() {
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;
  info() << "    Total internal execution time: " << m_timer << " sec" << endmsg;
  info() << "Per event internal execution time: " << m_timer / m_counter << " sec" << endmsg;
  info() << "    Total internal execution time: " << m_ctimer << " cycles" << endmsg;
  info() << "Per event internal execution time: " << m_ctimer / m_counter << " cycles" << endmsg;
#ifdef TRUTH_MATCH
  return GaudiTupleAlg::finalize();  // must be called after all other actions
#else 
  return GaudiAlgorithm::finalize();
#endif
}
//==================
//Add Stereo void
//==================
void PrSeedingNew::addStereo2(unsigned int part, unsigned int iCase)
{
  //#ifdef TRUTH_MATCH
  double zT1_1 = 0.;
  double zT1_2 = 0.;
  double zT2_1 = 0.;
  double zT2_2 = 0.;
  double zT3_1 = 0.;
  double zT3_2 = 0.;
  
  for(unsigned int kk = 2 ; 22>kk ;kk++)
  {
    if(m_hitManager->zone(kk)->isX()) continue; 
    if(m_hitManager->zone(kk)->planeCode()==1)
    {                                    
      zT1_1 = m_hitManager->zone(kk)->z();                              
    }
    if(m_hitManager->zone(kk)->planeCode()==2)
    {
      zT1_2 = m_hitManager->zone(kk)->z();                                  
    }                                                                           
    if(m_hitManager->zone(kk)->planeCode()==5)                                 
    {
      zT2_1 = m_hitManager->zone(kk)->z();                                  
    }                                                                          
    if(m_hitManager->zone(kk)->planeCode()==6)                                 
    {
      zT2_2 = m_hitManager->zone(kk)->z();
    }                                                                           
    if(m_hitManager->zone(kk)->planeCode()==9)                                 
    {                                    
      zT3_1 = m_hitManager->zone(kk)->z();                                  
    }
    if(m_hitManager->zone(kk)->planeCode()==10)                                 
    {
      zT3_2 = m_hitManager->zone(kk)->z();                                  
    }
  }
  //#endif 
  
  
  PrSeedTracks xProjections;
  for( PrSeedTracks::iterator itT1 = m_xCandidates.begin();m_xCandidates.end() != itT1; ++itT1)
  {
    if( !(*itT1).valid()) continue;
    xProjections.push_back( *itT1);  
  }
  unsigned int firstZone = part+2; //1st station U Layers
  unsigned int lastZone = part+22;
  if(m_useFix)
  {
    firstZone = 2;
    lastZone = 22;
  }
  
  for( PrSeedTracks::iterator itT = xProjections.begin();xProjections.end() != itT; ++itT)
  {
    PrHits myStereo;  
    myStereo.reserve(30);
    const unsigned int stepSize = m_useFix ? 1:2;
    for(unsigned int kk = firstZone; lastZone >kk;kk+=stepSize)
    {
      if(m_hitManager->zone(kk)->isX()) continue;
     
      double dxDy = m_hitManager->zone(kk)->dxDy();
      double lambda = std::fabs(dxDy);
      //dxDy()<<"part = "<<part<<"\n dxDy = "<<dxDy<<"\n PlaneCode ="<<m_hitManager->zone(kk)->planeCode()<<endmsg;
      double zPlane = m_hitManager->zone(kk)->z();
      double xPred = (*itT).x(zPlane);
      float xMin = xPred + m_tolXStereo* dxDy;
      float xMax = xPred - m_tolXStereo* dxDy;
      if(m_doAsymmUV)
      {
        if(part==0) //upper
        {
          if(dxDy>0) //U-Layers
          {
            xMin =xPred-lambda*(m_tolXStereo);//180
            xMax =xPred+m_alphaUV;
            if(kk%2==1 && m_useFix) //you are looking on opposite side for U layers when you look for y>0
            {
              xMin = xPred-25.0*lambda;//25 is the max value of yMin
              xMax = xPred+m_alphaUV;
            }
          }
          if(dxDy<0)//V-Layers
          {
            xMin = xPred - m_alphaUV;
            xMax = (xPred)+lambda*(m_tolXStereo);
            if(kk%2 ==1 && m_useFix){
              xMin = xPred - m_alphaUV;
              xMax = xPred + 25.0*lambda;
            }
          }
        }
        if(part==1)//lower
        {
          if(dxDy>0)
          {
            xMin = xPred - m_alphaUV;
            xMax = xPred + (m_tolXStereo)*lambda;
            if(kk%2==0 && m_useFix)
            {
              xMin=xPred - m_alphaUV;
              xMax=xPred + 25.0*lambda;
            }
          }
          if(dxDy<0)
          {
            xMin =  xPred - (m_tolXStereo)*lambda;
            xMax =  xPred + m_alphaUV;
            if(kk%2==0 && m_useFix)
            {
              xMin= xPred - 25.0*lambda;
              xMax= xPred + m_alphaUV;
            }
          }
        }
      }
      
      
      if ( xMin > xMax ) {
        always()<<"Wrong xMin xMax at UV"<<endmsg;
        float tmp = xMax;
        xMax = xMin;
        xMin = tmp;
      }
      ///we are in the look for kk .... so the xMin/xMax has to be defined before
#ifdef TRUTH_MATCH
      bool AssocXCandidate = Assoc((*itT));
      (*itT).setAssocX(AssocXCandidate);
      bool hitAssoc = false;
      int keyXProje = (*itT).key();
#endif
      PrHits::iterator itH = m_hitManager->zone(kk)->hits().begin();
      for ( ;    m_hitManager->zone(kk)->hits().end() != itH; ++itH ) { 
        if ( (*itH)->x() < xMin ) continue;
        if ( (*itH)->x() > xMax ) continue;
        if (m_useFix2ndOrder){
          if(part==0 && kk%2==1)
          {//this is a more sophisticated triangle fixing removal
            if(dxDy>0)
            {
              if( ( (*itH)->x()-xPred) >.1 ) continue;
              if( ( (*itH)->x()-xPred) < -.4 - 1.5/18.*(*itH)->yMax()) continue;
            }
            if(dxDy<0)
            {
              if( ( (*itH)->x()-xPred) < -.1) continue;
              if( ( (*itH)->x()-xPred) > +.4 + 1.5/18.*(*itH)->yMax()) continue;
            } 
          }
          if((part==1) && kk%2==0)
          {//this is a even more sophisticated triangle Fixing(should remove a bit of pollution
            if(dxDy>0)
            {
              if( ( (*itH)->x()-xPred) < -.1 ) continue;
              if( ( (*itH)->x()-xPred) > + .4 - 1.5/18.*(*itH)->yMin()) continue;
            }
            if(dxDy<0)
            {
              if( ( (*itH)->x()-xPred) > .1) continue;
              if( ( (*itH)->x()-xPred) < -.4 + 1.5/18.*(*itH)->yMin()) continue;
            } 
          }
        }
#ifdef TRUTH_MATCH
        LHCb::MCParticle * partic = m_map[(*itH)->id()].front();
        int key = -1000;
        hitAssoc = false;
        if(partic!=nullptr)
        {
          key = partic->key();
          int keyXProje = (*itT).key();
          if(keyXProje != -999)
          {
            if(key==keyXProje)
            {
              //always()<<"We find a hit!!!!"<<endmsg;
              hitAssoc = true;
            }
          }
        }
        if(m_doTuple)
        {
          
          using namespace Tuples;
          Tuple tupleHitUV = nTuple("SeedingUV/HitSelection","EventiUV");
          tupleHitUV->column("Case",(int)iCase);
          tupleHitUV->column("XCandidate_Assoc",AssocXCandidate);
          tupleHitUV->column("hitUVAssoc",hitAssoc);
          tupleHitUV->column("hitUV_kk",kk);
          tupleHitUV->column("hitX_at0",(*itH)->x());
          tupleHitUV->column("xMin",xMin);
          tupleHitUV->column("xMax",xMax);
          tupleHitUV->column("hitUV_dxDy",dxDy);
          tupleHitUV->column("hitZ",(*itH)->z());
          tupleHitUV->column("zPlane",zPlane);
          tupleHitUV->column("part",part);
          tupleHitUV->column("key_xProje",keyXProje);
          tupleHitUV->column("key_Hit",key);
          tupleHitUV->column("xPred",xPred);
          tupleHitUV->column("yOnTrack",(xPred-(*itH)->x())/dxDy);
          tupleHitUV->column("UV_Hit_yMin",(*itH)->yMin());
          tupleHitUV->column("UV_Hit_yMax",(*itH)->yMax());
          tupleHitUV->column("Hit_Coord",((*itH)->x()-xPred)/dxDy/zPlane);
          tupleHitUV->column("XProje_Phi",(*itT).Phi());
          tupleHitUV->column("XProje_X0",(*itT).X0());
          tupleHitUV->column("XProje_P",(*itT).PAssoc());
          tupleHitUV->column("XProje_ID",(*itT).ID());
          tupleHitUV->column("XProje_isLong",(*itT).isLong());
          tupleHitUV->column("XProje_isDown",(*itT).isDown());
          tupleHitUV->column("XProje_Eta25",(*itT).Eta25());
          tupleHitUV->column("XProje_fromB",(*itT).fromB());
          tupleHitUV->column("XProje_fromD",(*itT).fromD());
          tupleHitUV->column("XProje_cx",(*itT).cx());
          tupleHitUV->column("XProje_OVTX_Z",(*itT).OVTX_Z());
          tupleHitUV->column("XProje_OVTX_X",(*itT).OVTX_X());
          tupleHitUV->column("XProje_OVTX_Y",(*itT).OVTX_Y());
          tupleHitUV->column("XProje_trigger",(*itT).trigger());
          tupleHitUV->column("XProje_Chi2DoF",(*itT).chi2PerDoF());
          tupleHitUV->column("Purity",(*itT).Purity());
          tupleHitUV->column("Efficiency",(*itT).Efficiency());
               
          tupleHitUV->write();
        }
        
#endif
        // if(part==0 && kk%2 == 0)
        // {
        //   if((*itH)->yMin()<0)
        // }
          
        // if(part==1 && kk%2 ==1)
        // {
          
        // }
        
        //   const double yOnTrack = ( xPred - (*itH)->x() ) / dxDy;
        //   if ( yOnTrack  < (*itH)->yMin() - m_tolTriangle  ) {
        //     continue;
        //   }
        //   if ( yOnTrack  > (*itH)->yMax() + m_tolTriangle  ) {
        //     continue; 
        //   }
        //   if ( (0 == part) && (yOnTrack < -m_tolYOffset) ) {
        //     continue;
        //   }
        //   if ( (1 == part) && (yOnTrack > m_tolYOffset) ) { 
        //     continue; // bottom
        //   }  
        //if(part ==0)
        (*itH)->setCoord( ((*itH)->x() - xPred) / dxDy  / zPlane );
        if ( 1 == part && (*itH)->coord() > m_coord ) {
          continue;
        }
        if ( 0 == part && (*itH)->coord() <  -m_coord ) {
          continue;
        }
        if (m_removeFlagged && (*itH)->isUsed()) continue;
        if(m_removeHole)
        {
          double deltaat0 = (*itH)->x()-xPred;
          if( std::pow(deltaat0/7.7,2)+std::pow(xPred/83.,2) < 1 ) continue;
        }
        myStereo.push_back( *itH );
      }
    }
    
    //#ifdef TRUTH_MATCH
    double zMidT1 = (zT1_1+zT1_2)/2.;
    double zMidT2 = (zT2_1+zT2_2)/2.;
    double zMidT3 = (zT3_1+zT3_2)/2.;
    double xPred_zMidT1 = (*itT).x(zMidT1);
    double xPred_zMidT2 = (*itT).x(zMidT2);
    double xPred_zMidT3 = (*itT).x(zMidT3);
    //always()<<"xPred_zMidT1 = "<< xPred_zMidT1<<endmsg;
    
    (*itT).setxPredT1Mid(xPred_zMidT1);
    (*itT).setxPredT2Mid(xPred_zMidT2);
    (*itT).setxPredT3Mid(xPred_zMidT3);
    //#endif
    //Check here pairing of hits
    std::stable_sort( myStereo.begin(), myStereo.end(), PrHit::LowerByCoord() );
    PrPlaneCounter plCount;
    //Save position of this x candidate, for later use
    //My Stereo is a collection of Hits in the stereo Layers
    int i = 4;
    if(iCase == 0) i=5;
    unsigned int firstSpace = m_trackCandidates.size();
    PrHits::iterator itBeg = myStereo.begin();//first hit in U-V layer
    //PrHits::iterator itEnd = itBeg + 5; //fifth Hit in U-V layer +5 stndrd
    PrHits::iterator itEnd = itBeg + i;//was4
    
    while ( itEnd < myStereo.end() ) {
      // why should this increase, somehow momentum is missing or?
      double tolTy = m_tolTyOffset + m_tolTySlope * fabs( (*itBeg)->coord() );
      //double tolDeltaXMIDPairing = 
      // Take the next 4 hits and check if inside a given tolerance?
      if ( (*(itEnd-1))->coord() - (*itBeg)->coord() < tolTy ) {
        while( itEnd+1 < myStereo.end() &&
               (*itEnd)->coord() - (*itBeg)->coord() < tolTy ) {
          ++itEnd;
        }plCount.set( itBeg, itEnd );
        if( i-1 < plCount.nbDifferent()){//changed to 4 stndrd
          PrSeedTrack temp( *itT );
#ifdef TRUTH_MATCH
          double absAvgCoord = 0.;
          double avgCoord = 0.;
          int nUVAdded = 0;
#endif
          for ( PrHits::iterator itH = itBeg; itEnd != itH; ++itH ) {
            temp.addHit( *itH );
#ifdef TRUTH_MATCH
            nUVAdded+=1;            
            absAvgCoord+=std::fabs((*itH)->x()-temp.x((*itH)->z()));
            avgCoord+=(*itH)->x()-temp.x((*itH)->z());
#endif
            
          }
#ifdef TRUTH_MATCH
          
          avgCoord = avgCoord/(double)temp.hits().size();
          absAvgCoord = absAvgCoord/(double)temp.hits().size();
          temp.setAvgCoord(avgCoord);
          temp.setNUVAdded(nUVAdded);
          temp.setAbsAvgCoord(absAvgCoord);
          //always()<<"xPred_zMidT1 \t" <<xPred_zMidT1<<endmsg;
          temp.setxPredT1Mid(xPred_zMidT1);
          temp.setxPredT2Mid(xPred_zMidT2);
          temp.setxPredT3Mid(xPred_zMidT3);
#endif 
          bool ok = fitSimultaneouslyXY( temp , 0, iCase);
          //ok = fitTrack( temp );
          //ok = fitTrack( temp );
          int step = 1;
          int n_Max = 13;
          if (m_force12)
          {
            n_Max=12;
          }
          if(temp.hits().size()>13) ok = false;
          while ( !ok && temp.hits().size() > i+5 ) {//10 stndrd
            ok = removeWorstAndRefit( temp , step  , iCase);
            step++;
            if(temp.hits().size()>n_Max) ok = false;
          }
#ifdef TRUTH_MATCH
          if(m_doAssociation)
          {
            bool assoc = Assoc(temp);
            temp.setAssoc(assoc);
            setChi2( temp );
            //if(m_doTuple)
            //{
            using namespace Tuples;
            Tuple tupleTrackPostReFit = nTuple("Seeding/AfterFitAndReFitUV/Track","Eventi");
            tupleTrackPostReFit->column("Case",(int)iCase);
            tupleTrackPostReFit->column("step",step);
            tupleTrackPostReFit->column("OK_FromFit", ok);
            tupleTrackPostReFit->column("Assoc",assoc);
            tupleTrackPostReFit->column("Chi2",temp.chi2());
            tupleTrackPostReFit->column("MaxChi2OnHit",temp.MaxChi2());
            tupleTrackPostReFit->column("Assoc",temp.assoc());
            tupleTrackPostReFit->column("TxInf",temp.txinf());
            tupleTrackPostReFit->column("Chi2DoF",temp.chi2PerDoF());
            tupleTrackPostReFit->column("NDOF",(int)temp.hits().size());
            tupleTrackPostReFit->column("MaxDistance",temp.MaxDist());
            tupleTrackPostReFit->column("X0",temp.X0());
            tupleTrackPostReFit->column("x0Beg",temp.x0Beg());
            tupleTrackPostReFit->column("ax",temp.ax());
            tupleTrackPostReFit->column("bx",temp.bx());
            tupleTrackPostReFit->column("cx",temp.cx());
            tupleTrackPostReFit->column("ay",temp.ay());
            tupleTrackPostReFit->column("by",temp.by());
            tupleTrackPostReFit->column("AbsAvgDistance",temp.AbsAvgDist());
            tupleTrackPostReFit->column("AvgDistance",temp.avgDist());
            tupleTrackPostReFit->column("NAssocSameTrack",temp.getN1());
            tupleTrackPostReFit->column("NAssoc",temp.getN2());
            tupleTrackPostReFit->column("NHits",temp.getN3());
            tupleTrackPostReFit->column("Purity",temp.Purity());
            tupleTrackPostReFit->column("Efficiency",temp.Efficiency());
            tupleTrackPostReFit->column("P_Associated",temp.PAssoc());
            tupleTrackPostReFit->column("xSlope_9000",temp.xSlope(9000));
            tupleTrackPostReFit->column("xSlope_zRef",temp.xSlope(m_geoTool->zReference()));
            
            tupleTrackPostReFit->column("Phi",temp.Phi());
            tupleTrackPostReFit->column("isInVelo",temp.isInVelo());
            tupleTrackPostReFit->column("ID",temp.ID());
            tupleTrackPostReFit->column("isInUT",temp.isInUT());
            tupleTrackPostReFit->column("isDown",temp.isDown());
            tupleTrackPostReFit->column("isLong",temp.isLong());
            tupleTrackPostReFit->column("isElectron",temp.isElectron());
            tupleTrackPostReFit->column("over5",temp.over5());
            tupleTrackPostReFit->column("Eta25",temp.Eta25());
            tupleTrackPostReFit->column("OVTX_X",temp.OVTX_X());
            tupleTrackPostReFit->column("OVTX_Y",temp.OVTX_Y());
            tupleTrackPostReFit->column("OVTX_Z",temp.OVTX_Z());
            tupleTrackPostReFit->column("fromD",temp.fromD());
            tupleTrackPostReFit->column("fromB",temp.fromB());
            tupleTrackPostReFit->column("DeltaFormTInf",temp.deltaFromTinf());
            tupleTrackPostReFit->column("txPickedComb",temp.txpicked());
            tupleTrackPostReFit->column("TxInf",temp.txinf());
            tupleTrackPostReFit->column("AvgCoord",temp.AvgCoord());
            tupleTrackPostReFit->column("AbsAvgCoord",temp.AbsAvgCoord());
            tupleTrackPostReFit->column("Chi2XProje",temp.chi2XProj());
            tupleTrackPostReFit->column("nDoFX",temp.nDoFX());
            tupleTrackPostReFit->column("xPredT1",temp.xPredT1());
            tupleTrackPostReFit->column("xPredT2",temp.xPredT2());
            tupleTrackPostReFit->column("xPredT3",temp.xPredT3());
            double xHitsT1 = -999999.;
            double xHitsT2 = -999999.;
            double xHitsT3 = -999999.;
            bool hasT1_Pair = false;
            bool hasT2_Pair = false;
            bool hasT3_Pair = false;
            bool hasT1_1 = false;
            bool hasT1_2 = false;
            bool hasT2_1 = false;
            bool hasT2_2 = false;
            bool hasT3_1 = false;
            bool hasT3_2 = false; 
            double tempT1_1 = -99999.;
            double tempT1_2 = -99999.;
            double tempT2_1 =-99999.;
            double tempT2_2 =-99999.;
            double tempT3_1=-99999.;
            double tempT3_2=-99999.;
            
            for(PrHits::iterator it = temp.hits().begin();it!=temp.hits().end(); ++it)
            {
              if((*it)->isX()) continue;
              if((*it)->planeCode()==1){
                tempT1_1 = (*it)->x(); 
                hasT1_1 = true;
              }
              if((*it)->planeCode()==2){
                tempT1_2 = (*it)->x();
                hasT1_2 = true;
              }
              if((*it)->planeCode()==5){
                tempT2_1 = (*it)->x();
                hasT2_1 = true;
              }
              if((*it)->planeCode()==6){
                tempT2_2 = (*it)->x();
                hasT2_2 = true;
              }
              if((*it)->planeCode()==9){
                tempT3_1 = (*it)->x();
                hasT3_1 = true;
              }
              if((*it)->planeCode()==10){
                tempT3_2 = (*it)->x();
                hasT3_2 = true;
              }
            }
            if(hasT1_1&& hasT1_2){
              hasT1_Pair = true;
              xHitsT1 = (tempT1_1+tempT1_2)/2.;
            }
            if(hasT2_1&&hasT2_2){
              hasT2_Pair = true;
              xHitsT2 = (tempT2_1+tempT2_2)/2.;
            }
            if(hasT3_1&&hasT3_2){
              hasT3_Pair = true;
              xHitsT3 = (tempT3_1+tempT3_2)/2.;
            }
            tupleTrackPostReFit->column("hasT1Pair",hasT1_Pair);
            tupleTrackPostReFit->column("xHitsT1",xHitsT1);
            tupleTrackPostReFit->column("hasT2Pair",hasT2_Pair);
            tupleTrackPostReFit->column("xHitsT2",xHitsT2);
            tupleTrackPostReFit->column("hasT3Pair",hasT3_Pair);
            tupleTrackPostReFit->column("xHitsT3",xHitsT3);
            tupleTrackPostReFit->write();
          }
#endif
          
          if ( ok ) {
            setChi2( temp );
            float maxChi2 = m_maxChi2PerDoF + 6*temp.xSlope(9000)*temp.xSlope(9000);
            
#ifdef TRUTH_MATCH
            if(m_doHistos)
            {
              
              plot(temp.chi2PerDoF(),"UV/beforeCut/SelectedOk/Chi2DOF_All","Track Chi2/DOF;Counts",-0.5,30,300);    
              if(temp.assoc())
              {
                plot(temp.chi2PerDoF(),"UV/beforeCut/SelectedOk/Chi2DOF_True","Track Chi2/Counts",-0.5,30,300); 
              }
              if(!temp.assoc())
              {
                plot(temp.chi2PerDoF(),"UV/beforeCut/SelectedOk/Chi2DOF_Fake","Track Chi2/Counts",-0.5,30,300);
              }
            }  
#endif 
            double minSize = i+4;
            //number of hits hard coded??? More than in PatSeeding? 
            if ( temp.hits().size() > minSize ||
                 temp.chi2PerDoF() < maxChi2 ) {
              m_trackCandidates.push_back( temp );
            }
            itBeg += i-1;//standard
          }
        }
      }
      ++itBeg;
      itEnd = itBeg + i;//standard
    }
    
    //firstSpace = m_trackCandidate.size() //when we create more than one track out of the input xtrack
    
    //=== Remove bad candidates: Keep the best for this input track
    if ( m_trackCandidates.size() > firstSpace+1 ) {
      for ( unsigned int kk = firstSpace; m_trackCandidates.size()-1 > kk ; ++kk ) {
        if ( !m_trackCandidates[kk].valid() ) continue;
        for ( unsigned int ll = kk + 1; m_trackCandidates.size() > ll; ++ll ) {
          if ( !m_trackCandidates[ll].valid() ) continue;
          if ( m_trackCandidates[ll].hits().size() < m_trackCandidates[kk].hits().size() ) {
          m_trackCandidates[ll].setValid( false );
          } else if ( m_trackCandidates[ll].hits().size() > m_trackCandidates[kk].hits().size() ) {
            m_trackCandidates[kk].setValid( false );
            //         //equal size, take the one with the better chi2?
          } else if ( m_trackCandidates[kk].chi2() < m_trackCandidates[ll].chi2() ) {
            m_trackCandidates[ll].setValid( false );
          } else {
            m_trackCandidates[kk].setValid( false );
          }
        }
      }//end loop track Candidates for removal bad tracks
    }//loop candidates removal 
  }//end loop xProjections
  m_xCandidates.clear(); //At the end of each case delete it or you
}



//====================================================================
//Find XZ plane projections of tracks
//====================================================================

void PrSeedingNew::findXProjectionsLowMomentum(unsigned int part, unsigned int iCase)
{
  //Will be a copy paste of the LowMomentum once we run the cases
  //I am doing anything in this step
  if ( msgLevel(MSG::DEBUG) ) debug()<<"Entering the Case Loop"<<endmsg;
    
  //just do the 1st one here //1st layer and last one
  int firstZone = part;
  int lastZone  = 22 + part; 
  //Case 1
  if ( 1 == iCase ) firstZone = part + 6; // firstZone = 2nd X-Layer in T1
  //Case 2
  if ( 2 == iCase ) lastZone  = 16 + part;
}


void PrSeedingNew::findXProjectionsHighMomentum(unsigned int part, unsigned int iCase)
{
  m_xCandidates.clear();
  PrHits parabolaSeedHits;
  std::vector<PrHits> xHitsLists; //vector of list of Hits
  PrHits xHits;
  
  //just do the 1st one here //1st layer and last one
  int firstZoneId;
  int lastZoneId;
  if (0 == iCase) {
    firstZoneId = s_T1X1 | part;
    lastZoneId  = s_T3X2 | part;
  } else if ( 1 == iCase ) {
    firstZoneId = s_T1X2 | part;
    lastZoneId  = s_T3X2 | part;
  } else if ( 2 == iCase ) {
    firstZoneId = s_T1X2 | part;
    lastZoneId  = s_T3X1 | part;
  } else if ( 3 == iCase ) {
    firstZoneId = s_T1X1 | part;
    lastZoneId  = s_T3X1 | part;
  }
  if (msgLevel(MSG::DEBUG)) debug()<<"\t Loading Case Hit in first and last Zone"<<endmsg;
  PrHitZone* fZone = m_zones[firstZoneId];
  PrHitZone* lZone = m_zones[lastZoneId];
  if (msgLevel(MSG::DEBUG)) debug()<<"Hits in last and first Zone Loaded"<<endmsg;
  //load hits in first zone and last one
  PrHits& fHits = fZone->hits();
  PrHits& lHits = lZone->hits();
  double zFirst     = fZone->z(0.); //First  Zone Z value
  double zLast      = lZone->z(0.); //First  Zone Z value
  double DeltaZ = zLast-zFirst;     //Delta First-Last
  
  
  //std::vector<PrHitZone*> xZones({
  //  m_zones[s_T1X1 | part],
  //  m_zones[s_T1X2 | part],
  //  m_zones[s_T2X1 | part],
  //  m_zones[s_T2X2 | part],
  //  m_zones[s_T3X1 | part],
  //  m_zones[s_T3X2 | part]
  //});
  std::vector<PrHitZone*> xZones;
  xZones.reserve(4);
  for (int xZoneId : {s_T1X1, s_T1X2, s_T2X1, s_T2X2, s_T3X1, s_T3X2}) {
    xZoneId |= part;
    if (xZoneId != firstZoneId && xZoneId != lastZoneId) {
      xZones.push_back(m_zones[xZoneId]);
    }
  }

  //PrHits::iterator itLBeg = lHits.begin();  
  if (msgLevel(MSG::DEBUG)) debug()<<"Hits in the InBetween Zones Loaded"<<endmsg;
  if (msgLevel(MSG::DEBUG)) debug()<<"Will Loop over Hits in first Zone"<<endmsg;
  //for ( PrHits::iterator itF = fHits.begin(); fHits.end() != itF; ++itF ) {  
  for (PrHit* fHit : fHits) {
    if ( 0 != iCase && fHit->isUsed() && m_removeFlagged ) continue;
    //define search window as a function of the x in the first layer
    double tx_inf =  fHit->x()/zFirst; 
    double xProjeInf = tx_inf*zLast;
    double tolHp = m_TolHp;
    if(iCase ==1) 
    {
      tolHp = m_TolHp*1.5;
    }
    if(iCase ==2)
    {
      tolHp = m_TolHp*3.5;
    }
    
    //For all cases except case = 0
    double minXl_Loose =  xProjeInf + tx_inf*m_alphaCorrection  -tolHp;
    double maxXl_Loose =  xProjeInf + tx_inf*m_alphaCorrection  +tolHp;
    //Only for Case = 0
    double minXl_Tight = xProjeInf + tx_inf*m_alphaCorrection_Tight- m_TolHp_Tight;
    double maxXl_Tight = xProjeInf + tx_inf*m_alphaCorrection_Tight+ m_TolHp_Tight;
#ifdef TRUTH_MATCH
    // double xProjeCorrected = xProjeInf + tx_inf*m_alphaCorrection;
    if(m_doHistos)
    {
      
      // plot2D(minXl_Tight,tx_inf,"L0Selection/minXl_highP_Tight_vs_tinf","minXl (tight) vs t_{x}^{inf};minXl;t_{x}^{inf}",-3000.,3000.,-0.5,0.5,200,200);
      // plot2D(maxXl_Tight,tx_inf,"L0Selection/maxXl_highP_Tight_vs_tinf","maxXl (tight) vs t_{x}^{inf};minXl;t_{x}^{inf}",-3000.,3000.,-0.5,0.5,200,200);
      
      // plot2D(maxXl_Loose,tx_inf,"L0Selection/maxXl_highP_Loose_vs_tinf","maxXl (Loose)  vs t_{x}^{inf};maxXl;t_{x}^{inf}",-4000.,4000.,-0.5,0.5,200,200);
      // plot2D(minXl_Loose,tx_inf,"L0Selection/minXl_highP_Loose_vs_tinf","minXl (Loose)  vs t_{x}^{inf};minXl;t_{x}^{inf}",-4000.,4000.,-0.5,0.5,200,200);
      
      // plot2D(maxXl_Tight,fHit->x(),"L0Selection/maxXl_highP_Tight_vs_X_first" ,"maxXl (tight) vs X first (high P)",-4000.,4000.,-3000,3000.,200,200);
      // plot2D(minXl_Tight,fHit->x(),"L0Selection/minXl_highP_Tight_vs_X_first" ,"minXl (tight) vs X first (high P)",-4000.,4000.,-3000,3000.,200,200);
      
      // plot2D(maxXl_Loose,fHit->x(),"L0Selection/maxXl_highP_Loose_vs_X_first" ,"maxXl (Loose) vs X first (high P)",-4000.,4000.,-3000,3000.,200,200); 
      // plot2D(minXl_Loose,fHit->x(),"L0Selection/minXl_highP_Loose_vs_X_first" ,"minXl (Loose) vs X first (high P)",-4000.,4000.,-3000,3000.,200,200); 
    }
#endif
    
    double maxXl = maxXl_Loose;
    double minXl = minXl_Loose;
    if (iCase == 0){
      //itLBeg = std::lower_bound( lHits.begin(),lHits.end(), minXl_Tight , lowerBoundX());
      maxXl =  maxXl_Tight;
      minXl =  minXl_Tight;
    }else {
      //itLBeg = std::lower_bound( lHits.begin(), lHits.end(), minXl_Loose, lowerBoundX() ); 
      maxXl = maxXl_Loose;
      minXl = minXl_Loose;
    }
    if (msgLevel(MSG::DEBUG)) debug()<<"iCase "<<iCase<<"\t X last \t "<<minXl<<"\n\t\t\t Max X last \t "<<maxXl<<endmsg;
    //Last zone Hits iterator begin
    //PrHits::iterator itL = itLBeg;
    if (msgLevel(MSG::DEBUG)) debug()<<"Will Loop over Last Layer"<<endmsg;
    //while ( itL != lHits.end() && (*itL)->x() < maxXl && (*itL)->x() > minXl) 
    PrHits::iterator itL = std::lower_bound(lHits.begin(), lHits.end(), minXl, lowerBoundX());
    PrHits::iterator itLEnd = std::upper_bound(lHits.begin(), lHits.end(), maxXl, upperBoundX());
    PrHit* lHit;
    for (; itLEnd != itL; ++itL) {
      lHit = *itL;
    //for (PrHit* lHit : lHits) {
      if(nullptr == lHit){ 
        if (msgLevel(MSG::DEBUG)) debug()<<"Not Picking Hits in Last layer in the good search Window"<<endmsg;
        break;
      }
      //if (minXl > lHit->x() || maxXl < lHit->x()) continue;
      //if (maxXl < lHit->x()) break;
      if ( 0 != iCase && lHit->isUsed() && m_removeFlagged) continue;
      double tx_pickedcombination = (lHit->x()-fHit->x())/DeltaZ; 
      
      parabolaSeedHits.clear();
      parabolaSeedHits.reserve(8);
      
      
#ifdef TRUTH_MATCH
      
      //========================================================
      //==============2 Hit Combination association=============
      //========================================================
      
      debug()<<"2 Hit Association Inside Search Window"<<endmsg;
      bool HitFirstLastAssoc = false;
      double PAssoc = 0;
      LHCb::MCParticle *associatedMCPart = nullptr;
      
      //Get the vector of MCParticles associated to the Hits 
      //in F(first) L(last) station and check when they are the same
      if(m_doAssociation)
      {
        std::vector<LHCb::MCParticle*> partsHitFirst = m_map[fHit->id()];
        std::vector<LHCb::MCParticle*> partsHitLast = m_map[lHit->id()];
        for(std::vector<LHCb::MCParticle*>::iterator itPar1 = partsHitFirst.begin(); itPar1 != partsHitFirst.end(); ++itPar1)
        {
          if((*itPar1)==nullptr) break;
          for(std::vector<LHCb::MCParticle*>::iterator itPar2 = partsHitLast.begin(); itPar2 != partsHitLast.end(); ++itPar2)
          {
            if(*itPar2 == nullptr) break;
            if((*itPar1)==(*itPar2))
            {
              HitFirstLastAssoc = true;
              PAssoc = (*itPar1)->p();
              associatedMCPart = (*itPar1);
              break;
            }
          }
          if(!associatedMCPart) continue;
        }
        debug()<<"Doing Plot for 2 Hit Associated"<<endmsg;
      }
      
      
      //=========PUT HERE ALL THE PLOTS TO FIGURE OUT 
      //========THE SEARCH WINDOWS AND THE ROTATION ANGLE
      double xProjeCorrected = xProjeInf + tx_inf*m_alphaCorrection;
      double xProjeCorrected2 = xProjeInf+tx_inf*m_alphaCorrection_Tight;
      double deltaFromInfProje = lHit->x()-xProjeInf;
      double deltaFromInfProje2 = lHit->x()-xProjeCorrected2;
      //if(iCase == 0 && tx_inf*deltaFromInfProje<0) continue;
      if(m_doHistos)
      {
        
        if(iCase == 0)
        {
          plot(lHit->x()-xProjeInf,"FirstLastHit/FirstCase/DeltaHitProjection_All","#Delta (X_{Hit}^{Last} - x_{inf}^{proj} All;#Delta (X_{Hit}^{Last} - x_{inf}^{proj};Counts",-400.,400.,200);
          plot(lHit->x()-xProjeCorrected,"FirstLastHit/FirstCase/DeltaHitProjection_Corrected_All","#Delta (X_{Hit}^{Last} - x_{inf}^{proj} Corrected All;#Delta (X_{Hit}^{Last} - x_{inf}^{proj};Counts",-400.,400.,200);
          plot2D(tx_inf,lHit->x()-xProjeInf,"FirstLastHit/FirstCase/TxVsInfProje_All","t_{x} infinte projection vs #Delta(x_{Last} - x_{inf}^{proje} True;t_{x}^{inf};#Delta(x_{last}-x_{inf})",-0.6,0.6,-400.,400.,200,300);
          plot2D(tx_inf,lHit->x()-xProjeCorrected,"FirstLastHit/FirstCase/TxVsInfProjeCorr_All","t_{x} infinte projection vs #Delta(x_{Last} - x_{inf}^{proje} Corr All;t_{x}^{inf};#Delta(x_{last}-x_{inf})",-0.6,00.6,-400.,400.,200,300);
          
          if(HitFirstLastAssoc)
          {
            plot(lHit->x()-xProjeInf,"FirstLastHit/FirstCase/DeltaHitProjection_True","#Delta (X_{Hit}^{Last} - x_{inf}^{proj} True;#Delta (X_{Hit}^{Last} - x_{inf}^{proj};Counts",-400.,400.,200);
            plot(lHit->x()-xProjeCorrected,"FirstLastHit/FirstCase/DeltaHitProjection_Corrected_True","#Delta (X_{Hit}^{Last} - x_{inf}^{proj} Corrected True;#Delta (X_{Hit}^{Last} - x_{inf}^{proj};Counts",-400.,400.,200);
            plot2D(tx_inf,lHit->x()-xProjeInf,"FirstLastHit/FirstCase/TxVsInfProje_True","t_{x} infinte projection vs #Delta(x_{Last} - x_{inf}^{proje} True;t_{x}^{inf};#Delta(x_{last}-x_{inf})",-0.6,0.6,-1000.,1000.,200,300);
            plot2D(tx_inf,lHit->x()-xProjeCorrected,"FirstLastHit/FirstCase/TxVsInfProjeCorr_True","t_{x} infinte projection vs #Delta(x_{Last} - x_{inf}^{proje} Corr True;t_{x}^{inf};#Delta(x_{last}-x_{inf})",-0.6,0.6,-400.,400.,200,300);
            plot2D(lHit->x()-xProjeCorrected,PAssoc,"FirstLastHit/FirstCase/DeltaFromInfProjCorrVsP","#Delta(x_{Last} - x_{inf}^{proje} True Vs P Associated;#Delta(x_{last}-x_{inf});P [MeV]",-400.,400.,0,100000.,200,200);
            plot2D(lHit->x()-xProjeInf,PAssoc,"FirstLastHit/FirstCase/DeltaFromInfProjVsP","#Delta(x_{Last} - x_{inf}^{proje} True Vs P Associated;#Delta(x_{last}-x_{inf});P [MeV]",-400.,400.,0,100000.,200,200);
          }
          
        }
        if(iCase == 1)
        {
          plot(lHit->x()-xProjeInf,"FirstLastHit/SecondCase/DeltaHitProjection_All","#Delta (X_{Hit}^{Last} - x_{inf}^{proj} All;#Delta (X_{Hit}^{Last} - x_{inf}^{proj};Counts",-400.,400.,200);
          plot(lHit->x()-xProjeCorrected,"FirstLastHit/SecondCase/DeltaHitProjection_Corrected_All","#Delta (X_{Hit}^{Last} - x_{inf}^{proj} Corrected All;#Delta (X_{Hit}^{Last} - x_{inf}^{proj};Counts",-400.,400.,200);
          
          
          plot2D(tx_inf,lHit->x()-xProjeInf,"FirstLastHit/SecondCase/TxVsInfProje_All","t_{x} infinte projection vs #Delta(x_{Last} - x_{inf}^{proje} True;t_{x}^{inf};#Delta(x_{last}-x_{inf})",-0.6,0.6,-1000.,1000.,200,300);
          plot2D(tx_inf,lHit->x()-xProjeCorrected,"FirstLastHit/SecondCase/TxVsInfProjeCorr_All","t_{x} infinte projection vs #Delta(x_{Last} - x_{inf}^{proje} Corr All;t_{x}^{inf};#Delta(x_{last}-x_{inf})",-0.6,0.6,-400.,400.,200,300);
          
          if(HitFirstLastAssoc)
          {
            plot(lHit->x()-xProjeInf,"FirstLastHit/SecondCase/DeltaHitProjection_True","#Delta (X_{Hit}^{Last} - x_{inf}^{proj} True;#Delta (X_{Hit}^{Last} - x_{inf}^{proj};Counts",-400.,400.,200);
            plot(lHit->x()-xProjeCorrected,"FirstLastHit/SecondCase/DeltaHitProjection_Corrected_True","#Delta (X_{Hit}^{Last} - x_{inf}^{proj} Corrected True;#Delta (X_{Hit}^{Last} - x_{inf}^{proj};Counts",-400.,400.,200);
            
            plot2D(tx_inf,lHit->x()-xProjeInf,"FirstLastHit/SecondCase/TxVsInfProje_True","t_{x} infinte projection vs #Delta(x_{Last} - x_{inf}^{proje} True;t_{x}^{inf};#Delta(x_{last}-x_{inf})",-0.6,0.6,-1000.,1000.,200,300);
            plot2D(tx_inf,lHit->x()-xProjeCorrected,"FirstLastHit/SecondCase/TxVsInfProjeCorr_True","t_{x} infinte projection vs #Delta(x_{Last} - x_{inf}^{proje} Corr True;t_{x}^{inf};#Delta(x_{last}-x_{inf})",-0.6,0.6,-400.,400.,200,300);
            plot2D(lHit->x()-xProjeInf,PAssoc,"FirstLastHit/SecondCase/DeltaFromInfProjVsP","#Delta(x_{Last} - x_{inf}^{proje} True Vs P Associated;#Delta(x_{last}-x_{inf});P [MeV]",-400.,400.,0,100000.,200,200);
            plot2D(lHit->x()-xProjeCorrected,PAssoc,"FirstLastHit/SecondCase/DeltaFromInfProjCorrVsP","#Delta(x_{Last} - x_{inf}^{proje} True Vs P Associated;#Delta(x_{last}-x_{inf});P [MeV]",-400.,400.,0,100000.,200,200);
          }
        }
        if(iCase == 2)
        {
          plot(lHit->x()-xProjeInf,"FirstLastHit/ThirdCase/DeltaHitProjection_All","#Delta (X_{Hit}^{Last} - x_{inf}^{proj} All;#Delta (X_{Hit}^{Last} - x_{inf}^{proj};Counts",-400.,400.,200);
          plot(lHit->x()-xProjeCorrected,"FirstLastHit/ThirdCase/DeltaHitProjection_Corrected_All","#Delta (X_{Hit}^{Last} - x_{inf}^{proj} Corrected All;#Delta (X_{Hit}^{Last} - x_{inf}^{proj};Counts",-400.,400.,200);
          
          plot2D(tx_inf,lHit->x()-xProjeInf,"FirstLastHit/ThirdCase/TxVsInfProje_All","t_{x} infinte projection vs #Delta(x_{Last} - x_{inf}^{proje} True;t_{x}^{inf};#Delta(x_{last}-x_{inf})",-0.6,0.6,-400.,400.,200,300);
          plot2D(tx_inf,lHit->x()-xProjeCorrected,"FirstLastHit/ThirdCase/TxVsInfProjeCorr_All","t_{x} infinte projection vs #Delta(x_{Last} - x_{inf}^{proje} Corr All;t_{x}^{inf};#Delta(x_{last}-x_{inf})",-0.6,0.6,-400.,400.,200,300);
          
          if(HitFirstLastAssoc)
          {
            plot(lHit->x()-xProjeInf,"FirstLastHit/ThirdCase/DeltaHitProjection_True","#Delta (X_{Hit}^{Last} - x_{inf}^{proj} True;#Delta (X_{Hit}^{Last} - x_{inf}^{proj};Counts",-400.,400.,200);
            plot(lHit->x()-xProjeCorrected,"FirstLastHit/ThirdCase/DeltaHitProjection_Corrected_True","#Delta (X_{Hit}^{Last} - x_{inf}^{proj} Corrected True;#Delta (X_{Hit}^{Last} - x_{inf}^{proj};Counts",-400.,400.,200);
            plot2D(tx_inf,lHit->x()-xProjeInf,"FirstLastHit/ThirdCase/TxVsInfProje_True","t_{x} infinte projection vs #Delta(x_{Last} - x_{inf}^{proje} True;t_{x}^{inf};#Delta(x_{last}-x_{inf})",-0.6,0.6,-400.,400.,200,300);
            plot2D(tx_inf,lHit->x()-xProjeCorrected,"FirstLastHit/ThirdCase/TxVsInfProjeCorr_True","t_{x} infinte projection vs #Delta(x_{Last} - x_{inf}^{proje} Corr True;t_{x}^{inf};#Delta(x_{last}-x_{inf})",-0.6,0.6,-400.,400.,200,300);
            plot2D(lHit->x()-xProjeInf,PAssoc,"FirstLastHit/ThirdCase/DeltaFromInfProjVsP","#Delta(x_{Last} - x_{inf}^{proje} True Vs P Associated;#Delta(x_{last}-x_{inf});P [MeV]",-400.,400.,0,100000.,300,200);
            plot2D(lHit->x()-xProjeCorrected,PAssoc,"FirstLastHit/ThirdCase/DeltaFromInfProjCorrVsP","#Delta(x_{Last} - x_{inf}^{proje} True Vs P Associated;#Delta(x_{last}-x_{inf});P [MeV]",-400.,400.,0,100000.,200,200);          
          }
        }
        debug()<<"2 Hit Association Done"<<endmsg;
      }
      
#endif
      double x0 = fHit->x()-tx_pickedcombination*zFirst;
      double CorrX0 = m_x0Corr*x0;
      if(iCase == 1 || iCase ==2)
      {
        CorrX0 = (m_x0Corr)+1.5/1400*x0;
      }
      double x0new = x0*(1.+m_x0Corr);
      //Loop over Zones in T2
      //unsigned int counter = 0;
      //bool skipZone = true;
      //if( iCase !=0 ) skipZone = false;
      //for ( std::vector<PrHitZone*>::iterator itZ = xZones.begin(); xZones.end() != itZ; ++itZ )
      for (PrHitZone* xZone : {m_zones[s_T2X1 | part], m_zones[s_T2X2 | part]}) {
        double xProjected = x0 + xZone->z(0.)*tx_pickedcombination;
        double xProjectedCorrected = xProjected+CorrX0;
        double TollerancePlus = m_yOff1;
        double TolleranceMinus = m_yOff2;
        if(iCase ==1) {
          TollerancePlus = m_yOff1_Loose;
          TolleranceMinus = m_yOff2_Loose;
        }else if(iCase == 2) {
          TollerancePlus = m_yOff1_Loose*2;
          TolleranceMinus = m_yOff2_Loose*2;
        }
        
        double xMax =  xProjectedCorrected + m_m1*(x0+m_x0Offset)+TollerancePlus;
        double xMin =  xProjectedCorrected + m_m2*(x0+m_x0Offset)+TolleranceMinus;
        if( x0>0. )
        {
          xMax =  xProjectedCorrected  + m_m1*(x0-m_x0Offset)-TolleranceMinus;
          xMin =  xProjectedCorrected  + m_m2*(x0-m_x0Offset)-TollerancePlus;
        }
#ifdef TRUTH_MATCH
        if(m_doHistos){
          plot2D(x0,tx_inf,"L1Selection/x0vsTx_inf_All","x_{0} Vs t_{x}^{inf} ;x_{0};t_{x}^{inf}" , -3000. , 3000., -0.6,0.6,300,200);
        }
        
#endif
        if( xMax<xMin && msgLevel(MSG::DEBUG)) debug()<<"\t\t\t\t\t Wrong xMax/xMin"<<endmsg;
        PrHits::iterator itH = std::lower_bound(xZone->hits().begin(), xZone->hits().end(), xMin, lowerBoundX() );
        PrHit* mHit;
        for (; xZone->hits().end() != itH; ++itH ) {
          mHit = *itH;
        //for (PrHit* mHit : xZone->hits()) {
         //itH->x() or itH.x()
          //if ( mHit->x() < xMin ) continue; //or break? in principle we have the lower_bound before
          if ( mHit->x() > xMax ) break;
          // we can try to avoid this test
          if ( mHit->isUsed() && m_removeFlagged) continue; //Not re use Hits in the middle which are flagged?
          
#ifdef TRUTH_MATCH
          
          //3Hit Matching??
          bool HitFirstLastThirdAssoc = false;
          if(m_doHistos)
          {
            
            if(HitFirstLastAssoc)
            {
              debug()<<"Trying to see if 3rd Hit is associated"<<endmsg;
              std::vector<LHCb::MCParticle*> Parts3  = m_map[mHit->id()];
              for(std::vector<LHCb::MCParticle*>::iterator itP3 = Parts3.begin(); itP3 != Parts3.end(); ++itP3){
                if((*itP3) ==nullptr) break;
                if((*itP3) ==associatedMCPart)
                  HitFirstLastThirdAssoc = true;
              }
            }
            debug()<<"Doing Histo for Associated 3 Hit"<<endmsg;
            if(iCase == 0)
            {
              plot(mHit->x()-xProjectedCorrected,"L1Selection/FirstCase/DeltaXFromLinearProjectionCorrected_All","Three Hit Combination #Delta_{X}^{Corrected} All;#Delta (x_{T2} - x_{Projected};Counts",-4.,4.,200);
              plot2D(x0,mHit->x()  - xProjectedCorrected,"L1Selection/FirstCase/DeltaXT2CorrectedSelected_All"," Three Hit Combination #Delta_{X}^{Corrected} vs x_{0};x_{0};#Delta T2 Corrected",-3000.,3000.,-8.,8.,300,100);
              if(HitFirstLastThirdAssoc)
              { 
                plot(mHit->x()-xProjectedCorrected,"L1Selection/FirstCase/DeltaXFromLinearProjectionCorrected_True","Three Hit Combination #Delta_{X}^{Corrected} All;#Delta (x_{T2} - x_{Projected};Counts",-4.,4.,200);
                plot2D(x0,mHit->x()-xProjectedCorrected,"L1Selection/FirstCase/DeltaXT2CorrectedSelected_True","3 Hit Combination #Delta vs x_{0} (True All P);x_{0};#Delta T2 Corrected",-3000.,3000.,-6.,6.,300,100);
              }
            }
            
            if(iCase ==1)
            {
              plot(mHit->x()-xProjectedCorrected,"L1Selection/SecondCase/DeltaXFromLinearProjectionCorrected_All","Three Hit Combination #Delta_{X}^{Corrected} All;#Delta (x_{T2} - x_{Projected};Counts",-4.,4.,200);
              plot2D(x0,mHit->x()  - xProjectedCorrected,"L1Selection/SecondCase/DeltaXT2CorrectedSelected_All"," Three Hit Combination #Delta_{Corrected} vs x_{0};x_{0};#Delta T2 Corrected",-3000.,3000.,-8.,8.,300,100);
              if(HitFirstLastThirdAssoc)
              {
                plot(mHit->x()-xProjectedCorrected,"L1Selection/SecondCase/DeltaXFromLinearProjectionCorrected_True","Three Hit Combination #Delta_{X}^{Corrected} All;#Delta (x_{T2} - x_{Projected};Counts",-4.,4.,200);
                plot2D(x0,mHit->x()-xProjectedCorrected,"L1Selection/SecondCase/DeltaXT2CorrectedSelected_True","3 Hit Combination #Delta vs x_{0} (True All P);x_{0};#Delta T2 Corrected",-3000.,3000.,-6.,6.,300,100);
              }
            }
            if(iCase ==2)
            {
              plot(mHit->x()-xProjectedCorrected,"L1Selection/ThirdCase/DeltaXFromLinearProjectionCorrected_All","Three Hit Combination #Delta_{X}^{Corrected} All;#Delta (x_{T2} - x_{Projected};Counts",-4.,4.,200);
              plot2D(x0,mHit->x()  - xProjectedCorrected,"L1Selection/ThirdCase/DeltaXT2CorrectedSelected_All"," Three Hit Combination #Delta_{Corrected} vs x_{0};x_{0};#Delta T2 Corrected",-3000.,3000.,-8.,8.,300,100);
              if(HitFirstLastThirdAssoc)
              {
                plot(mHit->x()-xProjectedCorrected,"L1Selection/ThirdCase/DeltaXFromLinearProjectionCorrected_All","Three Hit Combination #Delta_{X}^{Corrected} All;#Delta (x_{T2} - x_{Projected};Counts",-4.,4.,200);
                plot2D(x0,mHit->x()-xProjectedCorrected,"L1Selection/ThirdCase/DeltaXT2CorrectedSelected_True","3 Hit Combination #Delta vs x_{0} (True All P);x_{0};#Delta T2 Corrected",-3000.,3000.,-6.,6.,300,100);
              }
            }
          }   
#endif 
          if (msgLevel(MSG::DEBUG)) debug()<<"Filling Parabola Seed Hits"<<endmsg;
          parabolaSeedHits.push_back(mHit);
        }
        if (parabolaSeedHits.size() > 0 && msgLevel(MSG::DEBUG)) debug()<<"ParabolaSeedHits Size \t ="<<parabolaSeedHits.size()<<endmsg;
        //Look for another Hit in last layer
        //end loop to pick up Hits in the 2 inner Layers (was only)
      }
      if (parabolaSeedHits.size()==0) continue;//if we don't fine any parabola Seed Hits in the middle 2 Layers then search for another XLast Hit
      // sort the parabola seed hits wrt to distance to the linear projection
      // merged parabolaSeedHits T2-1 & T2-2
      //=======================================================
      // We have 1 Hit in 1st 1 Hit in last and a
      // vector of Hits for in-between
      //=======================================================
      //std::vector<PrHits> xHitsLists; //vector of list of Hits
      xHitsLists.clear();
      
      //=======================================================
      //Sort the ParabolaSeedHits for in-between layers in increasing distance from the Projected Corrected position only when we have more than 1 ParabolaSeedHit
      //=======================================================
      
      if(parabolaSeedHits.size()>=1)
      {
#ifdef TRUTH_MATCH
        plot(parabolaSeedHits.size(),"L1Selection/ParabolaSeedHits_Size","ParabolaSeedHits Size;Size;Counts",.5,20.5,20);
#endif
        //Principle of the Lambda funtion, Hits sorted wrt distance from linear Projection 1st-3rd layer
        std::stable_sort( parabolaSeedHits.begin(),parabolaSeedHits.end(),
                          [x0new,tx_pickedcombination](const PrHit* lhs, const PrHit* rhs)
                          ->bool{return std::fabs(lhs->x() - (x0new + lhs->z()*tx_pickedcombination)) < std::fabs(rhs->x() - (x0new + rhs->z(0)*tx_pickedcombination));} );
      }
      if (msgLevel(MSG::DEBUG)) debug()<<"The Lambda Function Sorting end"<<endmsg;
      
      unsigned int maxParabolaSeedHits = m_maxParabolaSeedHits;
      if(parabolaSeedHits.size()<m_maxParabolaSeedHits)
      {
        maxParabolaSeedHits = parabolaSeedHits.size();
      }
      for (unsigned int i = 0; i<maxParabolaSeedHits;++i) //build a parabola for each 3 hit combination
      {
        //if (maxParabolaSeedHits==0) break; //maybe a repetition
        double a = 0;
        double b = 0;
        double c = 0; 
        //PrHits xHits;
        xHits.clear();
        if (m_useCubic) {
          solveParabola2(fHit,parabolaSeedHits[i],lHit,a,b,c); //Extrapolation with dRatio
        } else {
          solveParabola(fHit,parabolaSeedHits[i],lHit,a,b,c); //Extrapolation without dRatio
        }
        
        if (msgLevel(MSG::DEBUG)) debug()<<"Parabola Par"
                                         <<"\n a \t"<<a
                                         <<"\n b \t"<<b
                                         <<"\n c \t"<<c<<endmsg;
        
        //===================================================
        // Look in all the other layers except the
        // 1st/last/zone except the parabolaSeedHit
        //===================================================
        //Loop on all the xZones
        double m_dRatio = m_dRatio0 + c * m_dRatio1 + c * c * m_dRatio2;
        //for ( std::vector<PrHitZone*>::iterator itZ = xZones.begin(); xZones.end() != itZ; ++itZ )
        for (PrHitZone* xZone : xZones) {
          //skip xZone if planeCode is equal to the Last/First and parabolaSeedHits
          // if( ((int)(*itZ)->planeCode() == (int)(*itF)->planeCode()) || 
          //     ((int)(*itZ)->planeCode()==(int)(*itL)->planeCode() ))
          //   continue; 
          // if((int)(*itZ)->planeCode() == (int)parabolaSeedHits[i]->planeCode())continue;
          if (msgLevel(MSG::DEBUG)) debug()<<"Selecting ParSeedHits"<<endmsg;
          //if (   xZone->planeCode() == fZone->planeCode()
          //    || xZone->planeCode() == lZone->planeCode()
          //    || xZone->planeCode() == parabolaSeedHits[i]->planeCode()) {
          //  continue;
          //}
          if (xZone->planeCode() == parabolaSeedHits[i]->planeCode()) continue;
          
          double dz   = xZone->z() - m_geoTool->zReference();
          double xAtZ = a * dz * dz + b * dz + c; //Parabolic computation
          if (m_useCubic) {
            xAtZ= a * dz * dz * (1. + m_dRatio * dz) + b * dz + c; //with Cubic Correction
          }
          double xMaxAtZ = xAtZ + m_maxDeltaPar;   //std::fabs(tx_pickedcombination)+0.5;
          double xMinAtZ = xAtZ - m_maxDeltaPar;     //std::fabs(tx_pickedcombination)-0.5;
          if (iCase == 1) {
            xMaxAtZ = xAtZ + m_maxDeltaPar_Loose;
            xMinAtZ = xAtZ - m_maxDeltaPar_Loose;
          } else if (iCase == 2) {
            xMaxAtZ = xAtZ +m_maxDeltaPar_Loose;
            xMinAtZ = xAtZ -m_maxDeltaPar_Loose;
          }
          
          
          //Best projected Hit
          PrHit* bestProj = nullptr;
          double  bestDist = m_maxDeltaPar; //2.0 mm at the moment (Larger)? tighter? (see offline Seeding)
          if (iCase == 1) {
            bestDist = m_maxDeltaPar_Loose*1.5;
          } else if (iCase == 2 ) {
            bestDist = m_maxDeltaPar_Loose*2.5;
          }
          
          //Loop in all the Hits in a given X Zones( here still in the loop in xZones
          //Sort Hits from xMinAtZ to greater X
          if (xMinAtZ > xMaxAtZ) {
            if (msgLevel(MSG::DEBUG)) debug()<<"Bad Settings!!!!!!"<<endmsg;
          }
          PrHits::iterator itH = std::lower_bound(xZone->hits().begin() ,xZone->hits().end(),xMinAtZ,lowerBoundX());
          PrHit* hit;
          for (; xZone->hits().end() != itH; ++itH ) {
            hit = *itH;

          //for (PrHit* hit : xZone->hits()) {
            if (hit->isUsed() && m_removeFlagged)  continue;
            //if (hit->x() < xMinAtZ ) continue;
            if (hit->x() > xMaxAtZ ) break;
            
#ifdef TRUTH_MATCH
            
            bool Assoc4 = false;
            std::vector<LHCb::MCParticle*> PartFirst = m_map[fHit->id()];
            std::vector<LHCb::MCParticle*> PartLast = m_map[lHit->id()];
            std::vector<LHCb::MCParticle*> PartsSeedHit  = m_map[parabolaSeedHits[i]->id()];
            std::vector<LHCb::MCParticle*> Parts4  = m_map[hit->id()];
            for(std::vector<LHCb::MCParticle*>::iterator itP1 = PartFirst.begin(); itP1 != PartFirst.end(); ++itP1)
            {
              if((*itP1) ==nullptr) break;
              for(std::vector<LHCb::MCParticle*>::iterator itP2 = PartLast.begin(); itP2 != PartLast.end(); ++itP2)
              {
                if((*itP2) ==nullptr) break;
                if((*itP1) != (*itP2) ) continue;
                for(std::vector<LHCb::MCParticle*>::iterator itP4 = Parts4.begin(); itP4 != Parts4.end(); ++itP4)
                {
                  if((*itP4) ==nullptr) break;
                  if((*itP1) != (*itP4) )continue;
                  for(std::vector<LHCb::MCParticle*>::iterator itPar = PartsSeedHit.begin(); itPar != PartsSeedHit.end(); ++itPar)
                  {
                    if((*itPar)== nullptr) break;
                    if((*itPar) == (*itP1))
                      Assoc4 = true;
                  }
                }
              }
            }
            if(iCase == 0)
            {
              plot(hit->x() - xAtZ, "L2Selection/LookUpHits/FirstCase/DeltaSearch_All","Hit_{X} - xAtZ All;#(Delta x_{remaining} - xAtZ);Counts",-5,5,200);
              plot2D(c,hit->x()-xAtZ,"L2Selection/LookUpHits/FirstCase/DeltaSearch_Vs_XatZref_All","#Delta x_{remaining} - xAtZ vs x(zRef) True ; x(zRef); #Delta x_{remaining}-x(z)",-3000.,3000.,-5.,5.,300,200);
              if(Assoc4)
              {
                plot(hit->x() - xAtZ, "L2Selection/LookUpHits/FirstCase/DeltaSearch_True","Hit_{X} - xAtZ True;#Delta (Hit_{X} - xAtZ;Counts",-5.,5.,200);
                plot2D(c,hit->x()-xAtZ,"L2Selection/LookUpHits/FirstCase/DeltaSearch_Vs_xatZref_True","#Delta x_{remaining} - xAtZ vs x(zRef) True ; x(zRef); #Delta x_{remaining}-x(z)",-3000.,3000.,-5.,5.,300,200);
              }
            }
            if(iCase == 1)
            {
              plot(hit->x() - xAtZ, "L2Selection/LookUpHits/SecondCase/DeltaSearch_All","Hit_{X} - xAtZ All;#(Delta x_{remaining} - xAtZ);Counts",-5,5,200);
              plot2D(c,hit->x()-xAtZ,"L2Selection/LookUpHits/SecondCase/DeltaSearch_Vs_XatZref_All","#Delta x_{remaining} - xAtZ vs x(zRef) True ; x(zRef); #Delta x_{remaining}-x(z)",-3000.,3000.,-5.,5.,300,200);
              if(Assoc4)
              {
                plot(hit->x() - xAtZ, "L2Selection/LookUpHits/SecondCase/DeltaSearch_True","Hit_{X} - xAtZ True;#Delta (Hit_{X} - xAtZ;Counts",-5.,5.,200);
                plot2D(c,hit->x()-xAtZ,"L2Selection/LookUpHits/SecondCase/DeltaSearch_Vs_xatZref_True","#Delta x_{remaining} - xAtZ vs x(zRef) True ; x(zRef); #Delta x_{remaining}-x(z)",-3000.,3000.,-5.,5.,300,200);
              }
            }
            if(iCase == 2)
            {
              plot(hit->x() - xAtZ, "L2Selection/LookUpHits/ThirdCase/DeltaSearch_All","Hit_{X} - xAtZ All;#(Delta x_{remaining} - xAtZ);Counts",-5,5,200);
              plot2D(c,hit->x()-xAtZ,"L2Selection/LookUpHits/ThirdCase/DeltaSearch_Vs_XatZref_All","#Delta x_{remaining} - xAtZ vs x(zRef) True ; x(zRef); #Delta x_{remaining}-x(z)",-3000.,3000.,-5.,5.,300,200);
              if(Assoc4)
              {
                plot(hit->x() - xAtZ, "L2Selection/LookUpHits/ThirdCase/DeltaSearch_True","Hit_{X} - xAtZ True;#Delta (Hit_{X} - xAtZ;Counts",-5.,5.,200);
                plot2D(c,hit->x()-xAtZ,"L2Selection/LookUpHits/ThirdCase/DeltaSearch_Vs_xatZref_True","#Delta x_{remaining} - xAtZ vs x(zRef) True ; x(zRef); #Delta x_{remaining}-x(z)",-3000.,3000.,-5.,5.,300,200);
              }
            }
#endif
            //Find Hit with Best distance <2.0mm
            if(std::fabs(hit->x() - xAtZ ) < bestDist) 
            {
              bestDist =std::fabs(hit->x() - xAtZ );
              if (msgLevel(MSG::DEBUG)) debug()<<"I found an Hit from projection"<<endmsg;
              bestProj = hit;
            }
          }
          
          //End Loop on the Hits in the remaining layers to pick up Hits in the closer to the parabolic projection 
          if (bestProj != nullptr)
          {
            xHits.push_back(bestProj);
          }
          //in principle i have not
          //pushing back the hit
          //which was used to
          //seed the parabola
          //at this step Hits has the inner layer parabola
          //+ extractrapolated hit &
          // & best hit wrt distance to extrapolated parabola
        }//end loop xZones
        
        if (msgLevel(MSG::DEBUG)) debug()<<"End Loop in between zones to pick up Projection of parabola"<<endmsg;
        //    xHits.push_back( parabolaSeedHits[i]);
        // Add to the xHits Vector the remaining 3 Hits not considered
        xHits.push_back( parabolaSeedHits[i]);
        xHits.push_back( fHit);
        xHits.push_back( lHit);
        
        if(xHits.size()>6)
        { 
          always()<<"Smething goes wrong!!!! in the creation of the xHits list"<<endmsg;
          always()<<"xHits is bigger than 6 : ERROR"<<endmsg;
        }
        //end parabola Seed Hits loop in other Layers
        //Still in the L0 loop (selection last layer)
        //at this step we have 1 Hit in 1st Layer
        //at this step we have 1 Hit in last Layer
        //at this step we have 1 Hit in Middle Layer
        //at this step we have Hit in remaining X layers at the
        //best distance to extrapolated parabola All of them are
        //inside xHits i want to have at least min_HitXSize
        //UNDER STUDY CASE 0 Only Keep Tracks found with 6 Hits (reduce ghost rate if add UV too
        //1st Case keep only 6 Hits on found track
        // if(iCase == 0 && (xHits.size() <m_minXPlanes)) continue;
        // //2nd Case keep tracks with 4/5/6 hits
        // if(iCase == 1 && xHits.size() <m_minXPlanes) continue;
        if( xHits.size() <  m_minXPlanes)  continue; //Require at least 5 X Hits
        std::stable_sort(xHits.begin(), xHits.end(), compX());
        bool isEqual = false;
        //Remove xHits in the xHitsLists whicha are basically the same
        for (PrHits& hits : xHitsLists){
          if(hits == xHits){
            isEqual = true;
            break;
          }
        }
        if(!isEqual)
        { 
          if (msgLevel(MSG::DEBUG)) debug()<<"Pushing Back xHits List"<<endmsg;
          xHitsLists.push_back( xHits);
        }
      }//End loop parabolaSeedHits
      if (msgLevel(MSG::DEBUG)) debug()<<"End Loop For pick up Parabola Hits and build the xHitsLists"<<endmsg;
      //End loop Parabola Seed Hits
      //-------- Remove Duplicates from search in parabolaSeedHits
      if (msgLevel(MSG::DEBUG)) debug()<<"xHitsLists size before removing duplicates: "<<xHitsLists.size()<<endmsg;
      if (xHitsLists.size() == 0) {
        continue;
      }
      if (xHitsLists.size() > 1) {
        //---Remove Duplicates in the HitsList
        std::stable_sort( xHitsLists.begin(), xHitsLists.end() );
        xHitsLists.erase( std::unique(xHitsLists.begin(), xHitsLists.end()), xHitsLists.end());
      }
      if (msgLevel(MSG::DEBUG)) debug()<<"xHitsLists size after removing duplicates: "<<xHitsLists.size()<<endmsg;
      //Now let's fit the track
      for (PrHits& xHits : xHitsLists)
      { 
        if (msgLevel(MSG::DEBUG)) debug()<<"Fit Track"<<endmsg;
        //Create the track 
        PrSeedTrack temp_track( part , m_geoTool->zReference() , xHits); //Create the track
        //Setters for it: usefull later to parametrise
        
         
        temp_track.setdRatio(0.);
        //I load in the track these info which are then plotted
#ifdef TRUTH_MATCH
        if(m_doAssociation){
          temp_track.setDeltaFromTinf(deltaFromInfProje);
          temp_track.setTxPicked(tx_pickedcombination);
          temp_track.setx0Beg(x0);
          temp_track.setTxInf(tx_inf); }  
#endif
        if(m_useCubic){
          //Maybe a dRatio = f(x(zref));//Pierre Parametrisation
          temp_track.setdRatio(m_dRatio0);
        }
        //-----------------------------------------------------
        //----------------O-_The Fit_-O------------------
        //-----------------------------------------------------
        int nIter = 0;
        bool doRefit = false;
        temp_track.setRefitX(0);
        bool OK = fitXProjection(temp_track , 0 , iCase ,doRefit); //Fit the track projection on the XZ plane
        if(!OK && msgLevel(MSG::DEBUG)) 
          debug()<<"Fit Failed"<<endmsg;
        while(!OK)
        {
          nIter++;
          if (msgLevel(MSG::DEBUG)) debug()<<"The Fit failed"<<endmsg;
          if(temp_track.hits().size() <= m_minXPlanes) break;
          temp_track.setRefitX(nIter);
          if (msgLevel(MSG::DEBUG)) debug()<<"Removing Hits and Refit"<<endmsg;
          OK = removeWorstAndRefitX( temp_track, nIter , iCase,doRefit);
          if (msgLevel(MSG::DEBUG)) debug()<<"Remove worse and refit"<<endmsg;
          if (msgLevel(MSG::DEBUG)) debug()<<"***"<<endmsg;
        }
        setChi2X(temp_track);
        
#ifdef TRUTH_MATCH
        
        //Check if the track is Associated or not and fill the ntuples for the Chi2 Study after the refit
        if(m_doAssociation){
          bool assoc = Assoc(temp_track);
          temp_track.setAssoc(assoc);
          if(m_doTuple){
            using namespace Tuples;
            Tuple tupleTrackPostReFit = nTuple("Seeding/AfterFitAndReFit/Track","Eventi");
            tupleTrackPostReFit->column("Case",(int)iCase);
            tupleTrackPostReFit->column("OK", OK);
            tupleTrackPostReFit->column("Step",nIter);
            tupleTrackPostReFit->column("txPickedComb",temp_track.txpicked());
            tupleTrackPostReFit->column("Chi2FirstFit",temp_track.Chi2FirstFit());
            tupleTrackPostReFit->column("MaxChi2OnHit",temp_track.MaxChi2());
            tupleTrackPostReFit->column("Assoc",temp_track.assoc());
            tupleTrackPostReFit->column("TxInf",temp_track.txinf());
            tupleTrackPostReFit->column("Chi2",temp_track.chi2());
            tupleTrackPostReFit->column("Chi2DoF",temp_track.chi2PerDoF());
            tupleTrackPostReFit->column("NDOF",(int)temp_track.hits().size());
            tupleTrackPostReFit->column("MaxDistance",temp_track.MaxDist());
            tupleTrackPostReFit->column("X0",temp_track.X0());
            tupleTrackPostReFit->column("x0Beg",temp_track.x0Beg());
            tupleTrackPostReFit->column("ax",temp_track.ax());
            tupleTrackPostReFit->column("bx",temp_track.bx());
            tupleTrackPostReFit->column("cx",temp_track.cx());
            tupleTrackPostReFit->column("AbsAvgDistance",temp_track.AbsAvgDist());
            tupleTrackPostReFit->column("Chi2FirstFit",temp_track.Chi2FirstFit());
            tupleTrackPostReFit->column("Chi2Removed",temp_track.chi2Removed());
            tupleTrackPostReFit->column("DeltaChi2ReFit",temp_track.deltaRefitXChi2());
            tupleTrackPostReFit->column("Chi2ReFit",temp_track.Chi2ReFit());
            tupleTrackPostReFit->column("Chi2ReRemoved",temp_track.chi2ReRemoved());
            tupleTrackPostReFit->column("DeltaChi2ReReFit",temp_track.deltaRefitRefitXChi2());
            tupleTrackPostReFit->column("Chi2ReReFit",temp_track.Chi2ReReFit());
            tupleTrackPostReFit->column("AvgDistance",temp_track.avgDist());
            tupleTrackPostReFit->column("NAssocSameTrack",temp_track.getN1());
            tupleTrackPostReFit->column("NAssoc",temp_track.getN2());
            tupleTrackPostReFit->column("NHits",temp_track.getN3());
            tupleTrackPostReFit->column("Purity",temp_track.Purity());
            tupleTrackPostReFit->column("Efficiency",temp_track.Efficiency());
            tupleTrackPostReFit->column("P_Associated",temp_track.PAssoc());
            tupleTrackPostReFit->column("fromD",temp_track.fromD());
            tupleTrackPostReFit->column("fromB",temp_track.fromB());
            tupleTrackPostReFit->column("OVTX_X",temp_track.OVTX_X());
            tupleTrackPostReFit->column("OVTX_Y",temp_track.OVTX_Y());
            tupleTrackPostReFit->column("OVTX_Z",temp_track.OVTX_Z());
            tupleTrackPostReFit->column("StrangeDown",temp_track.StrangeDown());
            tupleTrackPostReFit->column("StrangeLong",temp_track.StrangeLong());
            tupleTrackPostReFit->column("Eta25",temp_track.Eta25());
            tupleTrackPostReFit->column("trigger",temp_track.trigger());
            tupleTrackPostReFit->column("isElectron",temp_track.isElectron());
            tupleTrackPostReFit->column("isDown",temp_track.isDown());
            tupleTrackPostReFit->column("isInUT",temp_track.isInUT());
            tupleTrackPostReFit->column("over5",temp_track.over5());
            tupleTrackPostReFit->column("isLong",temp_track.isLong());
            tupleTrackPostReFit->column("ID",temp_track.ID());
            tupleTrackPostReFit->column("Phi",temp_track.Phi());
            tupleTrackPostReFit->column("InVelo",temp_track.isInVelo());
            tupleTrackPostReFit->column("strangeLong",temp_track.StrangeLong());
            tupleTrackPostReFit->column("strangeDown",temp_track.StrangeDown());
            tupleTrackPostReFit->column("deltaFromTinf",temp_track.deltaFromTinf());
            tupleTrackPostReFit->column("slopeAtZRef",temp_track.xSlope(m_geoTool->zReference()));
            tupleTrackPostReFit->write();
          }
          if(m_doHistos)
          { 
            if(iCase ==0){
              if(temp_track.hits().size() == 6){
                plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/FirstCase/Hits_6/Chi2PerDoF_All","#chi2^{2} per DoF All;#Chi^{2} per DoF;Counts",0.,20.,200); 
                plot(temp_track.X0(),"XStep/fitAndReFit/FirstCase/Hits_6/X0_All","X_{0};X_{0};Counts",-1500.,1500.,200);
                plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/FirstCase/Hits_6/Chi2PerDoFVsX0_All","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500,1500.,200,200);
                //--------_TRUTH
                if(temp_track.assoc()){
                  plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/FirstCase/Hits_6/chi2PerDoF_True","#chi^{2} per DoF True;#chi^{2} per DoF;Counts",0.,20.,200);
                  plot(temp_track.X0(),"XStep/fitAndReFit/FirstCase/Hits_6/X0_True","X_{0};X_{0};Counts",-1500.,1500.,200);
                  plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/FirstCase/Hits_6/Chi2PerDoFVsX0_True","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500.,1500.,200,200);
                }
              }
              if(temp_track.hits().size() == 5){
                plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/FirstCase/Hits_5/chi2PerDoF_All","#chi2^{2} per DoF All;#Chi^{2} per DoF;Counts",0.,20.,200); 
                plot(temp_track.X0(),"XStep/fitAndReFit/FirstCase/Hits_5/X0_All","X_{0};X_{0};Counts",-1500.,1500.,200);
                plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/FirstCase/Hits_5/Chi2PerDoFVsX0_All","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500,1500.,200,200);
                //--------_TRUTH
                if(temp_track.assoc()){
                  plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/FirstCase/Hits_5/chi2PerDoF_True","#chi^{2} per DoF True;#chi^{2} per DoF;Counts",0.,20.,200);
                  plot(temp_track.X0(),"XStep/fitAndReFit/FirstCase/Hits_5/X0_True","X_{0};X_{0};Counts",-1500.,1500.,200);
                  plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/FirstCase/Hits_5/Chi2PerDoFVsX0_True","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500.,1500.,200,200);
                }
              }
              if(temp_track.hits().size() == 4)
              {
                plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/FirstCase/Hits_4/chi2PerDoF_All","#chi2^{2} per DoF All;#Chi^{2} per DoF;Counts",0.,20.,200); 
                plot(temp_track.X0(),"XStep/fitAndReFit/FirstCase/Hits_4/X0_All","X_{0};X_{0};Counts",-1500.,1500.,200);
                plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/FirstCase/Hits_4/Chi2PerDoFVsX0_All","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500,1500.,200,200);
                if(temp_track.assoc())
                {
                  plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/FirstCase/Hits_4/chi2PerDoF_True","#chi^{2} per DoF True;#chi^{2} per DoF;Counts",0.,20.,200);
                  plot(temp_track.X0(),"XStep/fitAndReFit/FirstCase/Hits_4/X0_True","X_{0};X_{0};Counts",-1500.,1500.,200);
                  plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/FirstCase/Hits_4/Chi2PerDoFVsX0_True","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500.,1500.,200,200);
                }
              }
            }//end iCase == 0
            //iCase ==1 
            if(iCase ==1){
              if(temp_track.hits().size() == 6){
                plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/SecondCase/Hits_6/chi2PerDoF_All","#chi2^{2} per DoF All;#Chi^{2} per DoF;Counts",0.,20.,200); 
                plot(temp_track.X0(),"XStep/fitAndReFit/SecondCase/Hits_6/X0_All","X_{0};X_{0};Counts",-1500.,1500.,200);
                plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/SecondCase/Hits_6/Chi2PerDoFVsX0_All","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500,1500.,200,200);
                //--------_TRUTH
                if(temp_track.assoc()){
                  plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/SecondCase/Hits_6/chi2PerDoF_true","#chi^{2} per DoF True;#chi^{2} per DoF;Counts",0.,20.,200);
                  plot(temp_track.X0(),"XStep/fitAndReFit/SecondCase/Hits_6/X0_true","X_{0};X_{0};Counts",-1500.,1500.,200);
                  plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/SecondCase/Hits_6/Chi2PerDoFVsX0_True","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500.,1500.,200,200);
                }
              }
              if(temp_track.hits().size() == 5){
                plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/SecondCase/Hits_5/chi2PerDoF_All","#chi2^{2} per DoF All;#Chi^{2} per DoF;Counts",0.,20.,200); 
                plot(temp_track.X0(),"XStep/fitAndReFit/SecondCase/Hits_5/X0_All","X_{0};X_{0};Counts",-1500.,1500.,200);
                plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/SecondCase/SecondCase/Chi2PerDoFVsX0_All","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500,1500.,200,200);
                //--------_TRUTH
                if(temp_track.assoc()){
                  plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/SecondCase/Hits_5/chi2PerDoF_true","#chi^{2} per DoF True;#chi^{2} per DoF;Counts",0.,20.,200);
                  plot(temp_track.X0(),"XStep/fitAndReFit/SecondCase/Hits_5/X0_true","X_{0};X_{0};Counts",-1500.,1500.,200);
                  plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/SecondCase/Hits_5/Chi2PerDoFVsX0_True","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500.,1500.,200,200);
                }
              }
              if(temp_track.hits().size() == 4){
                plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/SecondCase/Hits_4/chi2PerDoF_All","#chi2^{2} per DoF All;#Chi^{2} per DoF;Counts",0.,20.,200); 
                plot(temp_track.X0(),"XStep/fitAndReFit/SecondCase/Hits_4/X0_All","X_{0};X_{0};Counts",-1500.,1500.,200);
                plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/SecondCase/Hits_4/Chi2PerDoFVsX0_All","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500,1500.,200,200);
                if(temp_track.assoc()){
                  plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/SecondCase/Hits_4/chi2PerDoF_true","#chi^{2} per DoF True;#chi^{2} per DoF;Counts",0.,20.,200);
                  plot(temp_track.X0(),"XStep/fitAndReFit/SecondCase/Hits_4/X0_true","X_{0};X_{0};Counts",-1500.,1500.,200);
                  plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/SecondCase/Hits_4/Chi2PerDoFVsX0_true","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500.,1500.,200,200);
                }
              }
            }
            
            if(iCase ==2){
              if(temp_track.hits().size() == 6){
                plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/ThirdCase/Hits_6/chi2PerDoF_All","#chi2^{2} per DoF All;#Chi^{2} per DoF;Counts",0.,20.,200); 
                plot(temp_track.X0(),"XStep/fitAndReFit/ThirdCase/Hits_6/X0_All","X_{0};X_{0};Counts",-1500.,1500.,200);
                plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/ThirdCase/Hits_6/Chi2PerDoFVsX0_All","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500,1500.,200,200);
                //--------_TRUTH
                if(temp_track.assoc()){
                  plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/ThirdCase/Hits_6/chi2PerDoF_true","#chi^{2} per DoF True;#chi^{2} per DoF;Counts",0.,20.,200);
                  plot(temp_track.X0(),"XStep/fitAndReFit/ThirdCase/Hits_6/X0_true","X_{0};X_{0};Counts",-1500.,1500.,200);
                  plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/ThirdCase/Hits_6/Chi2PerDoFVsX0_All","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500.,1500.,200,200);
                }
              }
              if(temp_track.hits().size() == 5){
                plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/ThirdCase/Hits_5/chi2PerDoF_All","#chi2^{2} per DoF All;#Chi^{2} per DoF;Counts",0.,20.,200); 
                plot(temp_track.X0(),"XStep/fitAndReFit/ThirdCase/Hits_5/X0_All","X_{0};X_{0};Counts",-1500.,1500.,200);
                plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/ThirdCase/Hits_5/Chi2PerDoFVsX0_All","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500,1500.,200,200);
                //--------_TRUTH
                if(temp_track.assoc()){
                  plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/ThirdCase/Hits_5/chi2PerDoF_true","#chi^{2} per DoF True;#chi^{2} per DoF;Counts",0.,20.,200);
                  plot(temp_track.X0(),"XStep/fitAndReFit/ThirdCase/Hits_5/X0_true","X_{0};X_{0};Counts",-1500.,1500.,200);
                  plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/ThirdCase/Hits_5/Chi2PerDoFVsX0_True","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500.,1500.,200,200);
                }
              }
              if(temp_track.hits().size() == 4){
                plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/ThirdCase/Hits_4/chi2PerDoF_All","#chi2^{2} per DoF All;#Chi^{2} per DoF;Counts",0.,20.,200); 
                plot(temp_track.X0(),"XStep/fitAndReFit/ThirdCase/Hits_4/X0_All","X_{0};X_{0};Counts",-1500.,1500.,200);
                plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/ThirdCase/Hits_4/Chi2PerDoFVsX0_All","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500,1500.,200,200);
                if(temp_track.assoc()){
                  plot(temp_track.chi2PerDoF(),"XStep/fitAndReFit/ThirdCase/Hits_4/chi2PerDoF_true","#chi^{2} per DoF True;#chi^{2} per DoF;Counts",0.,20.,200);
                  plot(temp_track.X0(),"XStep/fitAndReFit/ThirdCase/Hits_4/X0_true","X_{0};X_{0};Counts",-1500.,1500.,200);
                  plot2D(temp_track.chi2PerDoF(),temp_track.X0(),"XStep/fitAndReFit/ThirdCase/Hits_4/Chi2PerDoFVsX0_All","X_{0} vs #chi^{2} per DoF;#chi^{2} per DoF;X_{0} [mm]",0.,20.,-1500.,1500.,200,200);
                }
              }
            }//end iCase == 2
          }//do Histos
        }//do Association
#endif
        // if(m_Cheat && temp_track.assoc() ){
        //   m_xCandidates.push_back(temp_track);
        // }
        
        double maxChi2PerDoF = m_maxChi2PerDoF;
        if(iCase == 0) maxChi2PerDoF = 6.5;
        if( OK && 
            temp_track.hits().size() >= m_minXPlanes
            && (( temp_track.chi2PerDoF() < maxChi2PerDoF) ||
                (iCase !=0 && temp_track.chi2PerDoF() <m_maxChi2PerDoF +6*tx_pickedcombination*tx_pickedcombination))){
          //PUT THE CUT TO SELECT Tracks with OK HERE!!
          // if((temp_track.hits().size()==6 &&  m_FlagHits &&      //Size ==6
          //     std::fabs(temp_track.cx())<6e-6 &&                      //P Cut
          //     std::fabs(temp_track.AbsAvgDist()<0.125))             //Quality Cut
          //    ||(temp_track.hits().size()==5 && std::fabs(temp_track.cx())<6e-6 && //Size ==5
          //       std::fabs(temp_track.AbsAvgDist()<0.250))
          //    ||(temp_track.hits().size()==4 &&                                            //Size ==4
          //       std::fabs(temp_track.cx())<6e-6&&
          //         std::fabs(temp_track.AbsrooAvgDist()<0.3)))
          // {
          //   for(PrHits::iterator itH = temp_track.hits().begin();temp_track.hits().end()!=itH;++itH){
          //     (*itH)->setUsed(true);
          //   }
          if (msgLevel(MSG::DEBUG)) debug()<<"Will Load track in container"<<endmsg;
          m_xCandidates.push_back(temp_track); //The X Candidate is created
        }
      }//end Loop xHist:xHitsLists
    }//end loop Last Zone given a firsZone selected
  }//end Loop First Zone
   // if(m_doPlots)
   // {    plot(m_xCandidates_highP.size(),"XCandidates/Size","Size vector XCandidates",0.,10000.,400);
   // }
  std::stable_sort(m_xCandidates.begin(),m_xCandidates.end(),PrSeedTrack::GreaterBySize());
  //====================================================================
  // Remove clones at this step???, i.e. share more than 2 hits (why not 3?)
  //====================================================================
  // debug()<<"Removing Clones in X Step"<<endmsg;
  for( PrSeedTracks::iterator itT1 = m_xCandidates.begin() ; m_xCandidates.end()!=itT1;++itT1)
  {
    if( m_xOnly && (*itT1).valid() ){
      if (msgLevel(MSG::DEBUG)) debug()<<"Fillincg Container Of Tracks"<<endmsg;
      m_trackCandidates.push_back( *itT1 );
    }
  }
}


void PrSeedingNew::removeClonesX(unsigned int maxCommon)
{
  
  for ( PrSeedTracks::iterator itT1 = m_xCandidates.begin(); m_xCandidates.end() !=itT1; ++itT1 ){
    if ( !(*itT1).valid() ) continue;
    //if ( (*itT1).hits().size() != 6 ) {
    //   int nUsed = 0;
    //   for ( PrHits::iterator itH = (*itT1).hits().begin(); (*itT1).hits().end() != itH; ++ itH) {
    //     if ( (*itH)->isUsed()) ++nUsed;
    //   }
    //   if ( 1 < nUsed ) 
    //   {
    //     (*itT1).setValid( false );
    //     continue;
    //   }
    // }
    
    for ( PrSeedTracks::iterator itT2 = itT1 + 1; m_xCandidates.end() !=itT2; ++itT2 ) {
      if ( !(*itT2).valid() ) continue;
      int nCommon = 0;
      PrHits::iterator itH1 = (*itT1).hits().begin();
      PrHits::iterator itH2 = (*itT2).hits().begin();
      PrHits::iterator itEnd1 = (*itT1).hits().end();
      PrHits::iterator itEnd2 = (*itT2).hits().end();
      
      while ( itH1 != itEnd1 && itH2 != itEnd2 ) {
        if ( (*itH1)->id() == (*itH2)->id() ) {
          ++nCommon;
          ++itH1;
          ++itH2;
        }else if( (*itH1)->id() < (*itH2)->id() ) {
          ++itH1;
        }else {
          ++itH2;
        }
      }
      if ( nCommon > maxCommon ) {
        if ( (*itT1).hits().size() > (*itT2).hits().size() ) {
          (*itT2).setValid( false );
        } else if ( (*itT1).hits().size() < (*itT2).hits().size() ) {
          (*itT1).setValid( false );
        } else if ( (*itT1).chi2PerDoF() < (*itT2).chi2PerDoF() ) {
          (*itT2).setValid( false );
        } else {
          (*itT1).setValid( false );
        }
      }
    }
  }
}




void PrSeedingNew::removeClones(unsigned int maxCommon)
{
  
  for ( PrSeedTracks::iterator itT1 = m_trackCandidates.begin(); m_trackCandidates.end() !=itT1; ++itT1 )
  {
    if ( !(*itT1).valid() ) continue;
    //if ( (*itT1).hits().size() != 6 ) {
    //   int nUsed = 0;
    //   for ( PrHits::iterator itH = (*itT1).hits().begin(); (*itT1).hits().end() != itH; ++ itH) {
    //     if ( (*itH)->isUsed()) ++nUsed;
    //   }
    //   if ( 1 < nUsed ) 
    //   {
    //     (*itT1).setValid( false );
    //     continue;
    //   }
    // }
    
    for ( PrSeedTracks::iterator itT2 = itT1 + 1; m_trackCandidates.end() !=itT2; ++itT2 ) {
      if ( !(*itT2).valid() ) continue;
      int nCommon = 0;
      PrHits::iterator itH1 = (*itT1).hits().begin();
      PrHits::iterator itH2 = (*itT2).hits().begin();
      PrHits::iterator itEnd1 = (*itT1).hits().end();
      PrHits::iterator itEnd2 = (*itT2).hits().end();
      
      while ( itH1 != itEnd1 && itH2 != itEnd2 ) {
        if ( (*itH1)->id() == (*itH2)->id() ) {
          ++nCommon;
          ++itH1;
          ++itH2;
        }else if( (*itH1)->id() < (*itH2)->id() ) {
          ++itH1;
        }else {
          ++itH2;
        }
      }
      if ( nCommon > maxCommon ) {
        if ( (*itT1).hits().size() > (*itT2).hits().size() ) {
          (*itT2).setValid( false );
        } else if ( (*itT1).hits().size() < (*itT2).hits().size() ) {
          (*itT1).setValid( false );
        } else if ( (*itT1).chi2PerDoF() < (*itT2).chi2PerDoF() ) {
          (*itT2).setValid( false );
        } else {
          (*itT1).setValid( false );
        }
      }
    }
  }
}


void PrSeedingNew::flagHits(unsigned int icase)
{
  /*Standard Flagg*/
  //for( PrSeedTracks::iterator itT1 = m_trackCandidates.begin(); m_trackCandidates.end() !=itT1; ++itT1)
  for (PrSeedTrack& track : m_trackCandidates) {
    if (!track.valid()) continue;
    if(track.hits().size()<m_SizeFlag) continue;
#ifdef TRUTH_MATCH
    if(m_doAssociation) 
    { 
      //PrSeedTrack& track = track;
      //bool Associate = Assoc(track);
      //double purity = track.Purity();
    }
#endif
     
    //for(PrHits::iterator itH = (*itT1).hits().begin(); itH!=(*itT1).hits().end() ; ++itH)
    for (PrHit* hit : track.hits()) {
#ifdef TRUTH_MATCH
      if(m_doTuple)
      {
        using namespace Tuples;
        Tuple tupleHitFlag = nTuple("Seeding/FlagHits/Hits","Flagging");
        tupleHitFlag->column("purity_Track",track.Purity());
        tupleHitFlag->column("efficiency_Track",track.Efficiency());
        tupleHitFlag->column("Chi2Hit",track.chi2(hit));
        tupleHitFlag->column("X0",track.X0());
        tupleHitFlag->column("NHitsTrack",(int)track.hits().size());
        tupleHitFlag->column("Assoc",track.assoc());
        tupleHitFlag->column("Chi2NDoF",track.chi2PerDoF());
        tupleHitFlag->column("Hit_Z",hit->z());
        tupleHitFlag->column("Hit_X",hit->x());
        tupleHitFlag->column("Track_ax",track.ax());
        tupleHitFlag->column("Track_bx",track.bx());
        tupleHitFlag->column("Track_cx",track.cx());
        tupleHitFlag->column("Track_ay",track.ay());
        tupleHitFlag->column("Track_by",track.by());
        tupleHitFlag->write();
      }
#endif
      if(track.chi2(hit) < m_MaxChi2Flag && track.X0() <m_MaxX0Flag)
      {
        hit->setUsed(true);
      }
    }
  }
}
void PrSeedingNew::flagHits2()
{
  //    //loop on track candidates to count how many hits it has in common to other tracks
  for( PrSeedTracks::iterator itT1 = m_trackCandidates.begin(); m_trackCandidates.end() !=itT1; ++itT1)
  {
    int nCommon = 0;
    //Count how many common hits track 1 has with all the other tracks
    for ( PrSeedTracks::iterator itT2 = itT1+1; m_trackCandidates.end() != itT2; itT2 ++)
    {
      PrHits::iterator itH1 = (*itT1).hits().begin();
      PrHits::iterator itH2 = (*itT2).hits().begin();
      PrHits::iterator itEnd1 = (*itT1).hits().end();
      PrHits::iterator itEnd2 = (*itT2).hits().end();
      while (itH1!=itEnd1 && itH2 != itEnd2 )
      {
        if( (*itH1)->id() == (*itH2)->id())
        {++nCommon ;
          ++itH1;
          ++itH2;
        }else if( (*itH1)->id() < (*itH2)->id())
        {
          ++itH1;
        }else
        {
          ++itH2;
        }
      }
      if( nCommon >(int)m_nCommonX) //if number in common is greater than a given value check numbe of hits on both track and flag the hits belonging to the one with greater number and best chi2 if equal
      {
        if( (*itT1).hits().size()>(*itT2).hits().size())
        {
          if((*itT1).hits().size() < m_SizeFlag) continue;
          //Will flag Hits of better track (*itT1)
          PrHits::iterator itH1 = (*itT1).hits().begin();
          PrHits::iterator itEnd1 = (*itT1).hits().end();
          while( itH1!=itEnd1)
          {
            if((*itT1).chi2((*itH1))<0.15 && (*itT1).chi2PerDoF()<1.5)
              (*itH1)->setUsed(true);
            ++itH1;
          }
        }else if((*itT1).hits().size()<(*itT2).hits().size())
        {
          if((*itT2).hits().size()<m_SizeFlag) continue;
          PrHits::iterator itH1 = (*itT2).hits().begin();
          PrHits::iterator itEnd1 = (*itT2).hits().end();
          while( itH1!=itEnd1)
          {
            if((*itT2).chi2((*itH1))<0.15 && (*itT2).chi2PerDoF()<1.5)
              (*itH1)->setUsed(true);
            ++itH1;
          }
        }else if( (*itT1).hits().size()==(*itT2).hits().size())
        {
          if((*itT1).chi2PerDoF()<(*itT2).chi2PerDoF())
          {
            if((*itT1).hits().size()<m_SizeFlag) continue;
            PrHits::iterator itH1 = (*itT1).hits().begin();
            PrHits::iterator itEnd1 = (*itT1).hits().end();
            while( itH1!=itEnd1)
            {
              if((*itT1).chi2((*itH1))<0.15 && (*itT1).chi2PerDoF()<1.5)
                (*itH1)->setUsed(true);
              ++itH1;
            }
          }else{  
            if((*itT2).hits().size()<m_SizeFlag) continue;
            PrHits::iterator itH1 = (*itT2).hits().begin();
            PrHits::iterator itEnd1 = (*itT2).hits().end();
            while( itH1!=itEnd1)
            {
              if((*itT2).chi2((*itH1))<0.15 && (*itT2).chi2PerDoF()<1.5)
                (*itH1)->setUsed(true);
              ++itH1;
            }
          }
        }
      }
    }
  }
}

      

     

    
             
//     if((*itT1).chi2PerDoF() < m_MaxChi2Flag)//Conditions here //flag All
//     {
//       for(PrHits::iterator itH = (*itT1).hits().begin();(*itT1).hits().end() != itH;++itH)
//       {
//         (*itH)->setUsed(true);
//       }
//     }
//   }
// }





//=========================================================================
//  Convert to LHCb tracks
//=========================================================================
void PrSeedingNew::makeLHCbTracks ( LHCb::Tracks* result ) {
  for ( PrSeedTracks::iterator itT = m_trackCandidates.begin();m_trackCandidates.end() != itT; ++itT ) {
    if ( !(*itT).valid() ) continue;
    if ( msgLevel(MSG::DEBUG) ) debug()<<"Creating LHCb Track"<<endmsg;

#ifdef TRUTH_MATCH
    plot((*itT).chi2PerDoF(),"MakeLHCbTrack/Chi2PerDOF","Track Chi2/DOF",-0.5,30.,300);                                                             
    plot((*itT).nDoF(), "MakeLHCbTrack/nDoF", "Track_nDoF",-0.5,40. ,300);
#endif


    //plot(fZone->hits().size(), "NumberOfHitsInFirstZone","NumberOfHitsInFirstZone", 0., 600., 100);        
    //plot(lZone->hits().size(), "NumberOfHitsInLastZone","NumberOfHitsInLastZone", 0., 600., 100);


    LHCb::Track* tmp = new LHCb::Track;
    tmp->setType( LHCb::Track::Ttrack );
    tmp->setHistory( LHCb::Track::PrSeeding );
    double qOverP = m_geoTool->qOverP( *itT );
    LHCb::State tState;
    double z = StateParameters::ZEndT;
    tState.setLocation( LHCb::State::AtT );
    tState.setState( (*itT).x( z ), (*itT).y( z ), z, (*itT).xSlope( z ), (*itT).ySlope( ), qOverP );

    //== overestimated covariance matrix, as input to the Kalman fit

    tState.setCovariance( m_geoTool->covariance( qOverP ) );
    tmp->addToStates( tState );

    //== LHCb ids.

    tmp->setPatRecStatus( LHCb::Track::PatRecIDs );
    for ( PrHits::iterator itH = (*itT).hits().begin(); (*itT).hits().end() != itH; ++itH ) {
      tmp->addToLhcbIDs( (*itH)->id() );
    }
    tmp->setChi2PerDoF( (*itT).chi2PerDoF() );
    tmp->setNDoF(       (*itT).nDoF() );
    result->insert( tmp );
  }
}






void PrSeedingNew::solveParabola(const PrHit* hit1, const PrHit* hit2, const PrHit* hit3, double& a, double& b, double& c){
  const double x3 = hit3->x();

  //New Parabola Method (be adapted to new parametrisation)
  const double z1_PB = hit1->z() - hit3->z();
  const double z2_PB = hit2->z() - hit3->z();
  const double x1_PB = hit1->x() - hit3->x();
  const double x2_PB = hit2->x() - hit3->x();

  const double det_PB = z1_PB*z2_PB*(z1_PB-z2_PB);

  if( std::fabs(det_PB) < 1e-8 ){
    a = 0.0;
    b = 0.0;
    c = 0.0;
    return;

  }

  a=0; b=0;c=0;
  a = (z2_PB*x1_PB-z1_PB*x2_PB)/det_PB;
  b = (-z2_PB*z2_PB*x1_PB+z1_PB*z1_PB*x2_PB)/det_PB;
  const double z3_PB = hit3->z() - m_geoTool->zReference();
  c = x3 + a * z3_PB * z3_PB - b *z3_PB;
  b -= 2 * a * z3_PB;
}

void PrSeedingNew::solveParabola2(const PrHit* hit1,const PrHit* hit2,const PrHit* hit3,double& a1, double& b1,double& c1){
  const double z1 = hit1->z() - m_geoTool->zReference();
  const double z2 = hit2->z() - m_geoTool->zReference();
  const double z3 = hit3->z() - m_geoTool->zReference();
  const double x1 = hit1->x();
  const double x2 = hit2->x();
  const double x3 = hit3->x();
  //const double e = m_dRatio;
  const double corrZ1 = 1+m_dRatio0*z1;
  const double corrZ2 = 1+m_dRatio0*z2;
  const double corrZ3 = 1+m_dRatio0*z3;
  const double det = (z1*z1)*corrZ1*z2 + z1*(z3*z3)*corrZ3 + (z2*z2)*corrZ2*z3 - z2*(z3*z3)*corrZ3 - z1*(z2*z2)*corrZ2 - z3*(z1*z1)*corrZ1;
  if( std::fabs(det) < 1e-8 )
  {
    a1 = 0.0;
    b1 = 0.0;
    c1 = 0.0;
    return;
  }
  const double det1 = (x1)*z2 + z1*(x3) + (x2)*z3 - z2*(x3) - z1*(x2) - z3*(x1);
  const double det2 = (z1*z1)*corrZ1*x2 + x1*(z3*z3)*corrZ3 + (z2*z2)*corrZ2*x3 - x2*(z3*z3)*corrZ3 - x1*(z2*z2)*corrZ2 - x3*(z1*z1)*corrZ1;
  const double det3 = (z1*z1)*corrZ1*z2*x3 + z1*(z3*z3)*corrZ3*x2 + (z2*z2)*corrZ2*z3*x1 - z2*(z3*z3)*corrZ3*x1 - z1*(z2*z2)*corrZ2*x3 - z3*(z1*z1)*corrZ1*x2;
  a1 = det1/det;
  b1 = det2/det;
  c1 = det3/det;
}

//=========================================================================
//  Fit the track, return OK if fit sucecssfull
//=========================================================================
bool PrSeedingNew::fitSimultaneouslyXY( PrSeedTrack& track ,int refit, unsigned int iCase) {
  double mat[15];
  double rhs[5];
  int nHitsX;
  int nHitsStereo;
  double a = track.ax();
  double m_dRatio = m_dRatio0+m_dRatio1*a+m_dRatio2*a*a;
  for ( int loop = 0; 3 > loop ; ++loop ) {
    std::fill(mat,mat+15,0.); 
    std::fill(rhs,rhs+5,0.);
    double z0 = m_geoTool->zReference(); 
    std::fill(mat, mat + 20, 0.);
    nHitsX = 0, nHitsStereo = 0;
    for ( PrHits::iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH ) 
    {
      if( (*itH)->isX()) 
      {
        nHitsX++;
      }else  nHitsStereo++;
      const double w = (*itH)->w();
      const double dxdy = (*itH)->dxDy();
      const double dz = 1e-3 * ((*itH)->z() - z0);
      double deta = 0.;
      if(m_useCubic)
        deta = dz*dz*(1+m_dRatio*dz);
      else deta = dz*dz;
      const double wdz = w * dz;
      const double eta = dz * dz * (1. + dz * m_dRatio);
      const double weta = w * deta;
      const double wdxdy = w * dxdy;
      const double wdxdydz = wdxdy * dz;
      const double dist = track.distance( *itH );
      //Fill Matrix
      mat[0] += w;
      mat[1] += wdz;
      mat[2] += wdz * dz;
      mat[3] += weta;
      mat[4] += weta * dz;
      mat[5] += weta * eta;
      mat[6] -= wdxdy;
      mat[7] -= wdxdydz;
      mat[8] -= wdxdy * deta;
      mat[9] += wdxdy * dxdy;
      mat[10] -= wdxdydz;
      mat[11] -= wdxdydz * dz;
      mat[12] -= wdxdydz * deta;
      mat[13] += wdxdydz * dxdy;
      mat[14] += wdxdydz * dz * dxdy;
      // fill right hand side
      rhs[0] += w * dist;
      rhs[1] += wdz * dist;
      rhs[2] += weta * dist;
      rhs[3] -= wdxdy * dist;
      rhs[4] -= wdxdydz * dist;
    }//Loop over Hits to fill the matrix
     // decompose matrix, protect against numerical trouble
    if (nHitsX < 3 || nHitsStereo < 2) return false;
    ROOT::Math::CholeskyDecomp<double, 5> decomp(mat);
    if (!decomp) return false;
    // solve linear system
    decomp.Solve(rhs);
    // undo dz scaling
    rhs[1] *= 1e-3; rhs[2] *= 1e-6; rhs[4] *= 1e-3;
    // PatSeedTrack y is expanded around z = 0 (what we fit is y(z) = a'+b(z-z0) --> y(z) = a +bz ==> a = a'-bz0 
    double yAtZRef = rhs[3];
    rhs[3] -= rhs[4] * z0;
    // protect against unreasonable track parameter corrections
    if (std::abs(rhs[0]) > 1e4 || std::abs(rhs[1]) > 5. ||
        std::abs(rhs[2]) > 1e-3 || std::abs(rhs[3]) > 1e4 ||
        std::abs(rhs[4]) > 1.) return false;
    track.updateParameters(rhs[0],rhs[1],rhs[2],rhs[3],rhs[4]);
  }
   
  double chi2_track = 0.;
  //double chi2_onHit = 0.;
  double maxChi2 =0;
  double maxDistance = 0.;
  double absdistanceSum = 0.;
  double distanceSum = 0;
  unsigned int NHits = (unsigned int)track.hits().size();
  PrHit *worst = nullptr;
  for ( PrHits::iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH ) 
  {
    double distance = track.distance(*itH);
    distanceSum+= distance/((double)NHits);
    absdistanceSum+= std::fabs(distance)/((double)NHits);
    if(std::fabs(distance)>maxDistance)
      maxDistance = fabs(distance);
    double chi2_onHit = track.chi2( *itH);
    chi2_track+=chi2_onHit;
    if ( chi2_onHit > maxChi2 )
    { 
      maxChi2 = chi2_onHit;
      worst = *itH;
    }
  } //Set Max Chi2DoF
  double X0 = track.ax() - track.bx()*m_geoTool->zReference()+track.cx()*m_ConstC;
  track.setMaxChi2(maxChi2);
#ifdef TRUTH_MATCH
  if(m_doAssociation)
  {
    bool Associate = Assoc(track);
    double chi2PerDoF = chi2_track/(double)track.hits().size();
    track.setAbsAvgDist(absdistanceSum);
    track.setAvgDist(distanceSum);
    track.setMaxDistance(maxDistance);
    //if(m_doTuple)
    //{ 
    if(iCase==2)
    {
      Tuple tupleTrackUV = nTuple("Seeding/UVFit/Track","UVEvents");
      tupleTrackUV->column("step",refit);
      tupleTrackUV->column("AssocX",track.assocX());
      tupleTrackUV->column("iCase" , (int)iCase);
      tupleTrackUV->column("MaxChi2OnHit",maxChi2);
      tupleTrackUV->column("WorstHit_Z", worst->z());
      tupleTrackUV->column("Assoc",track.assoc());
      tupleTrackUV->column("TxInf",track.txinf());
      tupleTrackUV->column("Chi2",chi2_track);
      tupleTrackUV->column("Chi2DoF",chi2PerDoF);
      tupleTrackUV->column("NDOF",NHits);
      tupleTrackUV->column("MaxDistance",maxDistance);
      tupleTrackUV->column("X0",X0);
      tupleTrackUV->column("x0Beg",track.x0Beg());
      tupleTrackUV->column("ax",track.ax());
      tupleTrackUV->column("bx",track.bx());
      tupleTrackUV->column("cx",track.cx());
      tupleTrackUV->column("ay",track.ay());
      tupleTrackUV->column("by",track.by());
      tupleTrackUV->column("SlopeX_xRef",track.xSlope(m_geoTool->zReference()));
      tupleTrackUV->column("AbsAvgDistance",absdistanceSum);
      //Other variables
      tupleTrackUV->column("AvgDistance",distanceSum);
      tupleTrackUV->column("NAssocSameTrack",track.getN1());
      tupleTrackUV->column("NAssoc",track.getN2());
      tupleTrackUV->column("NHits",track.getN3());
      tupleTrackUV->column("Purity",track.Purity());
      tupleTrackUV->column("Efficiency",track.Efficiency());
      tupleTrackUV->column("P_Associated",track.PAssoc());
      tupleTrackUV->column("Phi",track.Phi());
      tupleTrackUV->column("isInVelo",track.isInVelo());
      tupleTrackUV->column("ID",track.ID());
      tupleTrackUV->column("isInUT",track.isInUT());
      tupleTrackUV->column("isDown",track.isDown());
      tupleTrackUV->column("isLong",track.isLong());
      tupleTrackUV->column("isElectron",track.isElectron());
      tupleTrackUV->column("over5",track.over5());
      tupleTrackUV->column("Eta25",track.Eta25());
      tupleTrackUV->column("OVTX_X",track.OVTX_X());
      tupleTrackUV->column("OVTX_Y",track.OVTX_Y());
      tupleTrackUV->column("OVTX_Z",track.OVTX_Z());
      tupleTrackUV->column("fromD",track.fromD());
      tupleTrackUV->column("fromB",track.fromB());
      tupleTrackUV->column("DeltaFormTInf",track.deltaFromTinf());
      tupleTrackUV->column("txPickedComb",track.txpicked());
      tupleTrackUV->column("TxInf",track.txinf());
      tupleTrackUV->column("AvgCoord",track.AvgCoord());
      tupleTrackUV->column("AbsAvgCoord",track.AbsAvgCoord());
      tupleTrackUV->column("Chi2XProje",track.chi2XProj());
      tupleTrackUV->column("nDoFX",track.nDoFX());
      //always()<<"xPredT1" <<track.xPredT1()<<endmsg;
      
      tupleTrackUV->column("xPredT1",track.xPredT1());
      tupleTrackUV->column("xPredT2",track.xPredT2());
      tupleTrackUV->column("xPredT3",track.xPredT3());
      double xHitsT1 = -999999.;
      double xHitsT2 = -999999.;
      double xHitsT3 = -999999.;
      bool hasT1_Pair = false;
      bool hasT2_Pair = false;
      bool hasT3_Pair = false;
      bool hasT1_1 = false;
      bool hasT1_2 = false;
      bool hasT2_1 = false;
      bool hasT2_2 = false;
      bool hasT3_1 = false;
      bool hasT3_2 = false; 
      double tempT1_1 = -99999.;
      double tempT1_2 = -99999.;
      double tempT2_1 =-99999.;
      double tempT2_2 =-99999.;
      double tempT3_1=-99999.;
      double tempT3_2=-99999.;
      
      for(PrHits::iterator it = track.hits().begin();it!=track.hits().end(); ++it)
      {
        if((*it)->isX()) continue;
        if((*it)->planeCode()==1){
          tempT1_1 = (*it)->x(); 
          hasT1_1 = true;
        }
        if((*it)->planeCode()==2){
          tempT1_2 = (*it)->x();
          hasT1_2 = true;
        }
        if((*it)->planeCode()==5){
          tempT2_1 = (*it)->x();
          hasT2_1 = true;
        }
        if((*it)->planeCode()==6){
          tempT2_2 = (*it)->x();
          hasT2_2 = true;
        }
        if((*it)->planeCode()==9){
          tempT3_1 = (*it)->x();
          hasT3_1 = true;
        }
        if((*it)->planeCode()==10){
          tempT3_2 = (*it)->x();
          hasT3_2 = true;
        }
      }
      if(hasT1_1&& hasT1_2){
        hasT1_Pair = true;
        xHitsT1 = (tempT1_1+tempT1_2)/2.;
      }
      if(hasT2_1&&hasT2_2){
        hasT2_Pair = true;
        xHitsT2 = (tempT2_1+tempT2_2)/2.;
      }
      if(hasT3_1&&hasT3_2){
        hasT3_Pair = true;
        xHitsT3 = (tempT3_1+tempT3_2)/2.;
      }
      tupleTrackUV->column("hasT1Pair",hasT1_Pair);
      tupleTrackUV->column("xHitsT1",xHitsT1);
      tupleTrackUV->column("hasT2Pair",hasT2_Pair);
      tupleTrackUV->column("xHitsT2",xHitsT2);
      tupleTrackUV->column("hasT3Pair",hasT3_Pair);
      tupleTrackUV->column("xHitsT3",xHitsT3);
      tupleTrackUV->write();
    }
  }
#endif
  if(iCase ==2 && (track.hits().size()==9 || track.hits().size()==8 )&& std::abs(track.xPredT1())>500. &&chi2_track< (4+std::abs(track.xPredT1()-500.)*20./2000.)) return true;
  if(iCase ==2 && (track.hits().size()==9 || track.hits().size()==8 )&& std::abs(track.xPredT1())<500. ) return false;
  if(iCase == 1 && track.hits().size()==8 && chi2_track<(2.5-std::fabs(track.ay())*2.5/500.)) return true;
  if(iCase == 1 && track.hits().size()==8 && chi2_track>(2.5-std::fabs(track.ay())*2.5/500.)) return false;
  if(iCase ==1 && track.hits().size()==10 && chi2_track< 5-std::fabs(track.ay())*5./2000.) return true;
  if(iCase == 1 && track.hits().size()==10 && chi2_track>5-std::fabs(track.ay())*5./2000.) return false;
  
  if(iCase ==1 && track.hits().size()==9 && chi2_track<(2.5-std::fabs(track.ay())*2.5/500.)) return true;
  if(iCase ==1 && track.hits().size()==9 && chi2_track>(2.5-std::fabs(track.ay())*2.5/500.)) return false;
  
  if(iCase ==0 && chi2_track< 7-std::fabs(track.ay())*7./500. && std::fabs(track.X0())<500.) return true;
  if ( iCase ==0 && track.hits().size()==10 && (refit == 1 || refit ==2 || refit==3 || refit==4 ) && chi2_track<4 && chi2_track < 6-std::fabs(track.ay())*6./1700.) return true;
  if(iCase ==0) return false;
  if ( m_maxChi2HitsX > maxChi2 ){ // && (track.hits().size()!=10 && (refit !=1 || refit !=2 || refit !=3 || refit !=4)))
    return true;  
  }
  return false;
}
//=======================================
//Fit Only X Projection
//=======================================

bool PrSeedingNew::fitXProjection(PrSeedTrack& track , int Refit, unsigned int iCase , bool& dorefit)
{
  if (msgLevel(MSG::DEBUG)) debug()<<"Fitting"<<endmsg;
  //if(Refit==0 && track.hits().size()< m_minXPlanes) return false;
  //if(iCase == 0 && Refit ==0 && track.hits().size()==4) return false;
  if(track.hits().size()<m_minXPlanes) return false;
  double mat[6]; 
  double rhs[3];
  for(int loop = 0;3>loop;++loop)
  {
    std::fill(mat,mat+6,0.); 
    std::fill(rhs,rhs+3,0.);
    for( PrHits::iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH ){
      const double w = (*itH)->w();//squared
      const double dz= ((*itH)->z() - m_geoTool->zReference());
      double deta = 0;
      if(m_useCubic)
      {
        deta = dz*dz*(1+track.dRatio()*dz);
      }else{
        deta = dz*dz;
      }
      double dist = track.distance( *itH );
      if (msgLevel(MSG::DEBUG)) debug()<<"Loop \t"<<loop<<"\n Distance From Hit \t"<<dist<<endmsg;
      mat[0]+= w;   
      mat[1]+= w * dz;   mat[2]+= w * dz * dz;
      mat[3]+= w * deta; mat[4]+= w * dz * deta;  mat[5]+= w * deta * deta;
      //right hand side
      rhs[0]+= w * dist;
      rhs[1]+= w * dist * dz;
      rhs[2]+= w * dist * deta;
    }

    ROOT::Math::CholeskyDecomp<double,3> decomp(mat);
    if(!decomp)
    {
      return false;
    }
    //Solve linear system
    decomp.Solve(rhs);
    if (msgLevel(MSG::DEBUG)) debug()<<"Loop \t"<<loop<<"\n a = \t"<<rhs[0]<<"\n b = \t"<<rhs[1]<<"\n c = \t"<<rhs[2]<<endmsg;
    // protect against unreasonable track parameter corrections
    // (check that out)
    if(std::abs(rhs[0]) > 1e4 || std::abs(rhs[1]) > 5. ||
       std::abs(rhs[2]) > 1e-3 ) return false;
    //Small corrections
    track.updateParameters(rhs[0],rhs[1],rhs[2],0.,0.);
    double m_dRatio = m_dRatio0+rhs[0]*m_dRatio1+m_dRatio2*rhs[0]*rhs[0];
    track.setdRatio(m_dRatio);
    //Put back later faster maybe
    if (loop >0 && std::abs(rhs[0]) < 5e-3 && std::abs(rhs[1]) < 5e-6 &&
        std::abs(rhs[2]) < 5e-9) {
      break;
    }
  }

  //Fit is done
  //track.assoc is set here

  //Compute some values on the track
  double chi2_track = 0.;
  double maxChi2 = 0.;
  double maxDistance = 0.;
  double absdistanceSum = 0.;
  double distanceSum =0.;
  unsigned int NHits = (unsigned int)track.hits().size();
  PrHit *worst= nullptr;
  for ( PrHits::iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH )  //Loop over all hits in PrSeedTrack
  {
    double distance = track.distance( *itH );
    absdistanceSum += std::fabs(distance)/((double)NHits);
    distanceSum += distance/((double)NHits);
    if(std::fabs(distance)>maxDistance){
      maxDistance = fabs(distance);
    }
    double chi2_onHit = track.chi2( *itH );
    chi2_track += track.chi2( *itH );
    if ( chi2_onHit > maxChi2 ){
      maxChi2 = chi2_onHit;
      worst = *itH;
    }
    //Debug Hits?? add in the tuple *itH
    // if(m_doPlots && m_doAssociation)
    // {
    //   // Tuple tupleHit = nTuple("Seeding/XFit/Fit/Hits","Events");
    //   // tupleHit->column("Refit",Refit);
    //   // tupleHit->column("Distance",distance);
    //   // tupleHit->column("Chi2OnHit",chi2_onHit);
    //   // tupleHit->column("Assoc",track.assocX());
    //   // tupleHit->write();
    // }
  }//End Loop on Hits
  double X0 = track.ax() - track.bx()*m_geoTool->zReference()+track.cx()*m_ConstC;
  track.setX0(X0);
  //iCase == 0 *best selected 6_0_Case0
  //Fist Fit Conditions 6 Hits
  bool selectedOld = maxChi2<5.5;
  bool selected6_0 = (
                      std::fabs(X0)< (850.-maxChi2*(850./6.5)) && 
                      Refit ==0 &&
                      track.hits().size()==6
                      );

  //--------------------------------------Case 0 
  //--------------------------------------1st Fit
  bool selected6_0_Case0 = (
                            iCase ==0 &&
                            Refit==0 &&
                            track.hits().size()==6 &&
                            std::fabs(X0) < 650 &&
                            maxChi2<8.5 &&
                            (maxChi2-0.0)*(std::fabs(X0)-30)<800);//selection
                             
  bool selected5_0_Case0 = (
                            iCase == 0 &&
                            Refit == 0 &&
                            track.hits().size()==5 &&
                            std::fabs(X0)< (300.-maxChi2*(300./1.5))); //selection
  bool selected4_0_Case0 = (
                            iCase==0 &&
                            Refit == 0 && 
                            track.hits().size()==4
                            && false); //remove all of them
  //---------------------------------------Refit
  //Case0 ReFit
  bool selected5_1_Case0 =  (
                             iCase ==0 &&
                             Refit==1 &&
                             track.hits().size()==5 &&
                             maxChi2<2.5-std::fabs(X0)*2.5/400.);//selection
   
  bool selected4_1_Case0 = (
                            iCase==0 &&
                            Refit==1 &&
                            track.hits().size()==4 && 
                            maxChi2<(0.15-std::fabs(X0)*0.15/40.)); //selection
  //---------------------------------------ReReFit
  //Case0 ReRefit
  bool selected4_2_Case0 = (
                            iCase == 0 &&
                            Refit==2 &&
                            (track.hits().size()==4) &&
                            maxChi2<(0.15-std::fabs(X0)*0.15/40.)); //selection
   

  //------------------------------------Case 1
  //Case 1 1stFit
  bool selected6_0_Case1 =  (
                             /*iCase ==1 &&*/
                             Refit==0 &&
                             track.hits().size()==6 &&
                             maxChi2 < 8.5 && 
                             (maxChi2-2.)*(std::fabs(X0)-110.)<300.);//selection
   
  bool selected5_0_Case1 = (
                            /*iCase ==1 &&*/
                            Refit == 0 &&
                            track.hits().size()==5 &&
                            maxChi2<5.5-std::fabs(X0)*5.5/500. ); //selection
   
  bool selected4_0_Case1 = (
                            /*iCase ==1 &&*/
                            Refit==0 &&
                            track.hits().size()==4 &&
                            maxChi2<0.5-std::fabs(X0)*0.5/225); //selection
  //Case1 ReFit
  bool selected5_1_Case1 = (
                            /*iCase ==1 && */
                            Refit ==1 &&
                            track.hits().size()==5 &&
                            maxChi2<2.5); //selection
   
  bool selected4_1_Case1 = ( //remove all of them?
                            iCase == 1 &&
                            Refit ==1 &&
                            false &&
                            track.hits().size()==4);
  bool selected4_2_Case1 = (
                            /*iCase==1 &&*/
                            Refit==2 &&
                            track.hits().size()==4 &&
                            std::fabs(X0) > 900 && maxChi2<5.5 ); //sele
  
  //Case 2 == case 1
  track.setX0(X0);
  track.setRefitX(Refit);
  track.setMaxChi2(maxChi2);
    
#ifdef TRUTH_MATCH
  if(m_doAssociation)
  {
    Assoc(track);
    double chi2PerDoF = chi2_track/(double)(track.hits().size()-3);
    //track.setMaxChi2(maxChi2);
    track.setAbsAvgDist(absdistanceSum);
    track.setAvgDist(distanceSum);
    track.setMaxDistance(maxDistance);
    if( Refit ==0)
      track.setChi2FirstFit(chi2_track);
    if(Refit ==1)
    { 
      track.setChi2ReFit(chi2_track);
      track.setdeltaRefitXChi2(track.Chi2FirstFit()-chi2_track);
    }
    if(Refit==2)
    {
      track.setChi2ReReFit(chi2_track);
      track.setdeltaRefitRefitXChi2(track.Chi2ReFit()-chi2_track);
    }
    if(m_doTuple)
    {
      Tuple tupleTrack = nTuple("Seeding/XFit/FirstFit/Track","Events");
      tupleTrack->column("iCase",iCase);
      tupleTrack->column("step",Refit);
      tupleTrack->column("WorstHit_Z",worst->z());
      tupleTrack->column("stepOld",selectedOld);
      //Case 0
      //1St Fit
      tupleTrack->column("step0_Selected6_Case0",selected6_0_Case0);
      tupleTrack->column("step0_Selected5_Case0",selected5_0_Case0);
      tupleTrack->column("step1_Selected5_Case0",selected5_1_Case0);
      tupleTrack->column("step1_Selected4_Case0",selected4_1_Case0);
      tupleTrack->column("step2_Selected4_Case0",selected4_2_Case0);
      tupleTrack->column("step0_Selected6_Case1",selected6_0_Case1);
      tupleTrack->column("step0_Selected5_Case1",selected5_0_Case1);
      tupleTrack->column("step0_Selected4_Case1",selected4_0_Case1);
      tupleTrack->column("step1_Selected5_Case1",selected5_1_Case1);
      tupleTrack->column("step1_Selected4_Case1",selected4_1_Case1);
      tupleTrack->column("step2_Selected4_Case1",selected4_2_Case1);
      // tupleTrack->column("step0_Selected6_Case2",selected6_0_Case2);
      // tupleTrack->column("step0_Selected5_Case2",selected5_0_Case2);
      // tupleTrack->column("step0_Selected4_Case2",selected4_0_Case2);
      // tupleTrack->column("step1_Selected5_Case2",selected5_1_Case2);
      // tupleTrack->column("step1_Selected4_Case2",selected4_1_Case2);
      // tupleTrack->column("step2_Selected4_Case2",selected4_2_Case2);
      tupleTrack->column("MaxChi2OnHit",track.MaxChi2());
      tupleTrack->column("Chi2Removed",track.chi2Removed());
      tupleTrack->column("Chi2ReRemoved",track.chi2ReRemoved());
      tupleTrack->column("MaxDistance",maxDistance);
      tupleTrack->column("AbsAvgDistance",absdistanceSum);
      tupleTrack->column("AvgDistance",distanceSum);
      tupleTrack->column("DeltaChi2ReFit",track.deltaRefitXChi2());
      tupleTrack->column("DeltaChi2ReReFit",track.deltaRefitRefitXChi2());
      tupleTrack->column("Chi2FirstFit",track.Chi2FirstFit());
      tupleTrack->column("Chi2ReFit",track.Chi2ReFit());
      tupleTrack->column("Chi2ReReFit",track.Chi2ReReFit());
      tupleTrack->column("x0Beg",track.x0Beg());
      tupleTrack->column("txInf",track.txinf());
      tupleTrack->column("txpicked",track.txpicked());
      tupleTrack->column("DeltaFromTInf",track.deltaFromTinf());
      tupleTrack->column("fromB",track.fromB());
      tupleTrack->column("fromD",track.fromD());
      tupleTrack->column("OVTX_X",track.OVTX_X());
      tupleTrack->column("OVTX_Y",track.OVTX_Y());
      tupleTrack->column("OVTX_Z",track.OVTX_Z());
      tupleTrack->column("StrangeDown",track.StrangeDown());
      tupleTrack->column("StrangeLong",track.StrangeLong());
      tupleTrack->column("Eta25",track.Eta25());
      tupleTrack->column("Trigger",track.trigger());
      tupleTrack->column("Over5",track.over5());
      tupleTrack->column("IsElectron",track.isElectron());
      tupleTrack->column("IsLong",track.isLong());
      tupleTrack->column("IsDown",track.isDown());
      tupleTrack->column("isInUT",track.isInUT());
      tupleTrack->column("ID",track.ID());
      tupleTrack->column("Phi",track.Phi());
      tupleTrack->column("isInVelo",track.isInVelo());
      tupleTrack->column("Assoc",track.assoc());
      tupleTrack->column("P_Assoc",track.PAssoc());
      tupleTrack->column("N1",track.getN1());
      tupleTrack->column("N2",track.getN2());
      tupleTrack->column("N3",track.getN3());
      tupleTrack->column("Efficiency",track.Efficiency());
      tupleTrack->column("Purity",track.getN1());
      tupleTrack->column("Chi2",chi2_track);
      tupleTrack->column("Chi2DoF",chi2PerDoF);
      tupleTrack->column("X0",X0);
      tupleTrack->column("ax",track.ax());
      tupleTrack->column("bx",track.bx());
      tupleTrack->column("cx",track.cx());
      tupleTrack->column("NDOF",track.hits().size());
      tupleTrack->column("slopeAtZRef",track.xSlope(m_geoTool->zReference()));
      tupleTrack->write();
      // if(m_Cheat && track.assoc()) return true;
      // if(m_Cheat && !track.assoc()) return false;
    } 
  }
#endif
  // if(std::fabs(track.cx())>0.017e-3 ||
  //    track.MaxDist()>1.4 ||
  //    track.MaxChi2()>110 || 
  //    track.AbsAvgDist()>0.82 ||
  //    track.Chi2FirstFit()/(double)track.hits().size() > 42.5 ||
  //    std::fabs(track.X0())>2700){
  //   return false;
  // }
  //1st Fit conditions for 6 / 5 / 4 Hits on a track (tighter ?)
  //As the Standard PrSeedingXLayers
  //===============================
  //============6 HITS===============
  if(track.hits().size()==6)
  {
    if(Refit==0){
      if(iCase == 0 && selected6_0_Case0) return true; //Case 0
      if(iCase == 1 && selected6_0_Case1) return true; //Case 1
      if(iCase == 2 && selected6_0_Case1) return true;//same as case 1 = Case 2
      return false;
    }
  }
  //================================
  //===========5 HITS=================
  if(track.hits().size() == 5){
    if(Refit==0){
      if(iCase == 0 && selected5_0_Case0) return true; //Case0
      if(iCase == 1 && selected5_0_Case1) return true; //Case1
      if(iCase == 2 && selected5_0_Case1) return true; //same as case 1 //Case2==Case1
      return false;
    }
    if(Refit==1){
      if(iCase == 0 && selected5_1_Case0) return true; //Case0
      if(iCase == 1 && selected5_1_Case1) return true; //Case1
      if(iCase == 2 && selected5_1_Case1) return true; //same as case 1 == Case2
    }
  }
  //=================================
  //==============4 HITS===============
  if(track.hits().size()==4)
  {
    if(Refit==0){
      if(iCase ==0) return false; //Case0
      if(iCase ==1 && selected4_0_Case1) return true;//Case1
      if(iCase ==2 && selected4_0_Case1) return true;//Case1 = Case2
      return false;
    }
    if(Refit==1){
      if(iCase==0 && selected4_1_Case0) return true; //Case0
      if(iCase==1 && selected4_1_Case1) return true; //Case1
      if(iCase==2 && selected4_1_Case1) return true; //Case2 = Case1 
      return false;
    }
    if(Refit==2){
      if(iCase==0 && selected4_2_Case0) return true; //Case 
      if(iCase==1 && selected4_2_Case1) return true;
      if(iCase==2 && selected4_2_Case1) return true;
      return false;
    }
  }
  return false;
}

  
  
bool PrSeedingNew::removeWorstAndRefit(PrSeedTrack& track, int step, unsigned int iCase)
{
  float maxChi2 = 0.;
  PrHits::iterator worst = track.hits().begin();
  for ( PrHits::iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH ) {
    float chi2 = track.chi2( *itH );
    if ( chi2 > maxChi2 ) {
      maxChi2 = chi2;
      worst = itH;
    }
  }
  track.hits().erase( worst );
  return fitSimultaneouslyXY( track ,step, iCase);
}


//=========================================================================
//  Remove the worst hit and refit.
//=========================================================================
bool PrSeedingNew::removeWorstAndRefitX ( PrSeedTrack& track,int step , unsigned int iCase , bool& doRefit) 
{
  
  if(track.hits().size()>=m_minXPlanes)
  {
    if (msgLevel(MSG::DEBUG)) debug()<<"Removing Worst and Refitting"<<endmsg;
    //Find maxChi2 contribution of an Hit
    double maxChi2 = 0.;
    PrHits::iterator worst = track.hits().begin(); 
    for ( PrHits::iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH ) 
    {
      double chi2 = track.chi2( *itH );
      if ( chi2 > maxChi2 ) {
        maxChi2 = chi2;
        worst = itH;
      }
    }
#ifdef TRUTH_MATCH
    if(step ==1)
      track.setChi2Removed(maxChi2);
    if(step ==2)
      track.setChi2ReRemoved(maxChi2);
#endif
    track.hits().erase( worst );
    bool doRefit = true;
    bool Ok =  fitXProjection(track , step, iCase ,doRefit);
    if(Ok) return true;
    return false;
  }
  return false;
}

void PrSeedingNew::setChi2 ( PrSeedTrack& track ) 
{
  float chi2 = 0.;
  int   nDoF = -3;
  // Fitted a parabola
  bool hasStereo = false;
  //for ( PrHits::iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH ) 
  for (PrHit* hit : track.hits()) {
    float d = track.distance( hit );
    if ( hit->dxDy() != 0 ) hasStereo = true;
    //hasStereo = hasStereo || (hit->dxDy() != 0);
    float w = hit->w();
    chi2 += w * d * d;
    nDoF += 1;
  }                                                                          
  if (hasStereo) {
    nDoF -= 2;
  }
  track.setChi2( chi2, nDoF );
}
               


//=========================================================================
//  Set the chi2 of the track
//=========================================================================
void PrSeedingNew::setChi2X ( PrSeedTrack& track ) {
  double chi2 = 0.;
  int   nDoF = -3;  // Fitted a parabola
  //bool hasStereo = false;
  //for ( PrHits::iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH ) {
  for (PrHit* hit : track.hits()) {
    if(hit->dxDy() !=0 && msgLevel(MSG::DEBUG)) debug()<<"You were picking up Stereo Layers!!!"<<endmsg;
    double d = track.distance( hit );
    double w = hit->w();
    chi2 += w * d * d;
    nDoF += 1;
  }
  if (msgLevel(MSG::DEBUG)) debug()<<"Chi2 Set for track = \t"<<chi2<<endmsg;
  track.setChi2( chi2, nDoF );
  #ifdef TRUTH_MATCH
  track.setChi2XProj(chi2,nDoF);
  #endif
}


//===============================================
//  Print the whole track
//=========================================================================
void PrSeedingNew::printTrack ( PrSeedTrack& track ) {
  for ( PrHits::iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH ) {
    info() << format( "dist %7.3f dy %7.2f chi2 %7.2f ", track.distance( *itH ), track.deltaY( *itH ), track.chi2( *itH ) );
    printHit( *itH );
  }
}


//=========================================================================
//  Print the information of the selected hit
//=========================================================================
void PrSeedingNew::printHit ( const PrHit* hit, std::string title ) {
  info() << "  " << title << " "
         << format( " Plane%3d zone%2d z0 %8.2f x0 %8.2f  size%2d charge%3d coord %8.3f used%2d ",
                    hit->planeCode(), hit->zone(), hit->z(), hit->x(),
                    hit->size(), hit->charge(), hit->coord(), hit->isUsed() );
  if ( m_debugTool ) m_debugTool->printKey( info(), hit->id() );
  if ( matchKey( hit ) ) info() << " ***";
  info() << endmsg;
}

void PrSeedingNew::createMap()
{ 
#ifdef TRUTH_MATCH
  LinkedTo<LHCb::MCParticle, LHCb::FTCluster> myClusterLink ( evtSvc(), msgSvc(), LHCb::FTClusterLocation::Default ); 
  LHCb::MCParticles* mcParts= getIfExists<LHCb::MCParticles>(LHCb::MCParticleLocation::Default);
  MCTrackInfo trackInfo( evtSvc(), msgSvc());

  //Loop over all the MCParticle
  for(LHCb::MCParticles::const_iterator iPart =  mcParts->begin(); iPart != mcParts->end(); ++iPart)
  { 
    //Get the MC particle
    const LHCb::MCParticle* mcPart = *iPart;
    // bool isLong = trackInfo.hasVeloAndT( mcPart);
    // bool isDown = trackInfo.hasT( mcPart) && trackInfo.hasTT(mcPart); 
    bool isSeed = trackInfo.hasT( mcPart); 
    // bool isUp = trackInfo.hasVelo(mcPart) && trackInfo.hasTT(mcPart);
    
    if(!isSeed && !m_UseSeed) continue; //Select only MCParticles which are Seed tracks
    //if(!isLong && !m_UseLong) continue;
    //if(!isDown && !m_UseDown) continue;
    //bool isElectron = std::fabs(mcPart->particleID().pid())==11;
    //if(isElectron && m_NoElectrons) continue;
    //bool PCut = std::fabs(mcPart->p())<= m_PCut;
    //if(PCut) continue; //skip track to truth match if momentum not good
    for(int i = 0; i < 24; ++i){
      PrHitZone* zone = m_hitManager->zone( i );
      
      //If Only X step do not match for Hits in UV
      //Matching for X and matching for UV
      if(m_xOnly && !zone->isX()) continue;
      for( PrHits::iterator itH = zone->hits().begin(); zone->hits().end() != itH; ++itH){
        //get the mcParticle associated to the Cluster LHCbID
        LHCb::MCParticle* mcPart1 = myClusterLink.first( (*itH)->id().ftID() ); 
        //Vector of MCParticles which end up in a LHCbID
        std::vector< LHCb::MCParticle* > parts;
        bool found = false;
        while(mcPart1 !=nullptr)
        {
          if(mcPart1==mcPart)
          {
            found = true;
            parts.push_back(mcPart1);
          }
          mcPart1=myClusterLink.next(); //next MCParticle contributing to the LHCbID
        }
        if(parts.size()>1) always()<<"More Than one MCParticle through the channel"<<endmsg; 
        if(found && parts.size()!=0)
        {debug()<<"Map Filled"<<endmsg;
          m_map[(*itH)->id()] = parts;} //instead of vector of Key
        //parts.clear(); //delete the vector for a single Hit association
      }//end loop Zone Hits
    }//end Loop All Parts
  }//end Loop MCParticles
#endif
}//end createMap m_map

bool PrSeedingNew::Assoc(PrSeedTrack& track)
{
#ifdef TRUTH_MATCH
  std::map<LHCb::MCParticle*, int> Counter;
  Counter.clear();
  //Number if Hits with at least 1 MCParticle associated
  int NHits =0;
  int HitWithAssoc=0;
  //Loop over track Hits
  for(PrHits::const_iterator itH = track.hits().begin();track.hits().end()!=itH;++itH)
  {
    NHits++; //Number of hits on track to associate
    //Get The vector of MCParticle Associated to the LHCBiD
    std::vector<LHCb::MCParticle*> parts = m_map[(*itH)->id()]; 
    if(parts.size() == 0 )
    {
      continue; //next Hit 
    }
    for(std::vector<LHCb::MCParticle*>::iterator itP = parts.begin(); itP != parts.end(); ++itP)
    {
      if((*itP)==nullptr) break;
      Counter[(*itP)]++;
      debug()<<"Counter[part]\t"<<Counter[(*itP)]<<endmsg;
    }
    HitWithAssoc++;
  }//end loop on all Hits on the track
  MCTrackInfo trackInfo( evtSvc(), msgSvc() );
  while(!Counter.empty())
  {
    std::vector<std::pair<LHCb::MCParticle*,int> > v(std::begin(Counter),std::end(Counter));
    std::sort(std::begin(v),std::end(v),[](const std::pair<LHCb::MCParticle*,int>& a,const std::pair<LHCb::MCParticle*,int>& b) {return a.second < b.second;});
    std::pair<LHCb::MCParticle*,int> a = v.back();
    LHCb::MCParticle* part = a.first;
    //FROM PRCHECKER FLAGS TO IMPLEMNT IN PRSEEDTRACK PRIVATE VARIABLE: When associate we can access to truth matching info
    if(0 == trackInfo.fullInfo(part)) return false;
    //from Checker
    bool isElectron = abs(part->particleID().pid()) ==11;
    bool isLong = trackInfo.hasVeloAndT(part);
    bool isDown = trackInfo.hasT(part) && trackInfo.hasTT(part);
    bool over5 = 5000. < std::fabs(part->p());
    bool trigger = 3000. < std::fabs(part->p()) && 500. <std::fabs(part->pt());
    bool isInVelo = trackInfo.hasVelo(part);
    bool isInUT = trackInfo.hasTT(part);
    bool strangeLong = false;
    bool strangeDown = false;
    bool fromB = false;
    bool fromD = false;
    bool eta25 = part->momentum().Eta() >2 && part->momentum().Eta()<5;
    const LHCb::MCVertex* vertex =0;
    vertex = part->originVertex();
    double OVTX_Z = vertex->position().z();
    double OVTX_X = vertex->position().x();
    double OVTX_Y = vertex->position().y();
    if(0!=part->originVertex()){
      const LHCb::MCParticle*mother = part->originVertex()->mother();
      if(0!=mother){
        if(0!=mother->originVertex())
        {
          double rOrigin = mother->originVertex()->position().rho();
          if(fabs(rOrigin)<5.){
            int pid = abs(mother->particleID().pid());
            if ( 130 == pid ||    // K0L
                 310 == pid ||    // K0S                                                                      
                 3122 == pid ||   // Lambda
                 3222 == pid ||   // Sigma+
                 3212 == pid ||   // Sigma0
                 3112 == pid ||   // Sigma-
                 3322 == pid ||   // Xsi0
                 3312 == pid ||   // Xsi
                 3334 == pid      // Omega-
                 ) 
            {
              strangeDown = trackInfo.hasT(part) && trackInfo.hasTT(part);
              strangeLong = trackInfo.hasVeloAndT(part);
            }
          }
        }
      }
      while (0!=mother){
        if ( mother->particleID().hasBottom() &&
             ( mother->particleID().isMeson() ||  mother->particleID().isBaryon() ) ) fromB = true;
        if ( mother->particleID().hasCharm() &&
             ( mother->particleID().isMeson() ||  mother->particleID().isBaryon() ) ) fromD = true;
        mother = mother->originVertex()->mother();
      }
    }
    int n_maxAssoc = a.second; 
    //Setter Properties of found track on Hit
    track.setfromB(fromB);
    track.setfromD(fromD);
    track.setOVTX(OVTX_X,OVTX_Y,OVTX_Z);
    track.setStrangeLong(strangeLong);
    track.setStrangeDown(strangeDown);
    track.setEta25(eta25);
    track.setTrigger(trigger);
    track.setover5(over5);
    track.SetIsElectron(isElectron);
    track.SetIsLong(isLong);
    track.SetIsDown(isDown);
    track.SetIsInUT(isInUT);
    track.SetIsInVelo(isInVelo);
    track.setPAssoc( part->p());
    track.setPhi(part->momentum().phi());
    track.setN1N2N3(n_maxAssoc,HitWithAssoc,NHits);
    double purity = ((double)n_maxAssoc)/((double)HitWithAssoc);
    double Efficiency = ((double)n_maxAssoc)/((double)NHits);
    track.setPurity(purity);
    track.setEfficiency(Efficiency);

    if( Efficiency>m_Efficiency &&  //4 if m_nMinXPlanes == 5
        purity>m_Purity){ //70 % and at least 3 hit associated to same MCParticle!will give inf chi2
      debug()<<"# of Hits associated to the same MCParticle \t"
             <<n_maxAssoc<<"\n Hit with associaton \t"
             <<HitWithAssoc<<"\n of Hits on track \t"
             <<track.hits().size()<<endmsg;
      Counter.clear();
      //Setter for Associator Plot
      track.setAssoc(true);
      track.setKey(part->key());
      return true;
    }
    Counter.clear();
    track.setAssoc(false);
    return false; 
  }
  debug()<<"Will return False due to condition_2"<<endmsg;
  Counter.clear();
  return false;
#endif 
  return false;
}
