#ifndef PRSEEDTRACK_H 
#define PRSEEDTRACK_H 1

// Include files
#include "Event/Track.h"
#include "PrKernel/PrHit.h"

/** @class PrSeedTrack PrSeedTrack.h
 *  This is the working class inside the T station pattern
 *
 *  @author Olivier Callot
 *  @date   2012-03-22
 */
class UVPair {
public:
  UVPair(float xPredicted, PrHits& hitsU, PrHits& hitsV , float xTol, float yMax)
    : m_xPred( xPredicted, m_hitsU(hitsU), m_hitsV(hitsV))
  {
    init();
  }
  virtual ~UVPair ()  {  }; //<Destructor
  
  private:
  void init() 
  {
    
  }
  
  
  
#endif // PRSEEDTRACK_H
