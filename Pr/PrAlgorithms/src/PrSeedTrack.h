#ifndef PRSEEDTRACK_H 
#define PRSEEDTRACK_H 1

// Include files
#include "Event/Track.h"
#include "PrKernel/PrHit.h"
#include "Event/MCParticle.h"
/** @class PrSeedTrack PrSeedTrack.h
 *  This is the working class inside the T station pattern
 *
 *  @author Olivier Callot
 *  @date   2012-03-22
 */
//Comment it if you don't want to do truth matching
//#define TRUTH_MATCH
class PrSeedTrack {
public: 
  /// Constructor with the z reference
  PrSeedTrack( unsigned int zone, double zRef ) 
    : m_zone( zone ), m_zRef( zRef )
  {
#ifdef TRUTH_MATCH
    init2();
#else
    init();
#endif
  }; 
  
  /// Constructor with list of hits //Initialize track
  PrSeedTrack( unsigned int zone, double zRef, PrHits& hits )
    : m_zone( zone ), m_zRef( zRef ), m_hits( hits ) 
  {
#ifdef TRUTH_MATCH
    init2();
#else
    init();
#endif
  }; 
  
  virtual ~PrSeedTrack( ) {}; ///< Destructor
  //Handling the hits on the track
  PrHits& hits()                   { return m_hits; }
  const PrHits& hits()       const { return m_hits; }
  //Add hit
  void addHit( PrHit* hit )        { m_hits.push_back( hit );}
  void addHits( PrHits& hits ) {
    m_hits.reserve( m_hits.size() + hits.size() );
    m_hits.insert( m_hits.end(), hits.begin(), hits.end() );
  }
  //Zone (0,1) Up Down
  unsigned int zone() const { return m_zone; }
  //====================================
  //SETTERS & Getters  Genearl One
  //====================================
  
  //***********Track Parameters to compute distances
  void setParameters( double ax, double bx, double cx, double ay, double by ) {
    m_ax = ax;
    m_bx = bx;
    m_cx = cx;
    m_ay = ay;
    m_by = by;
  }
  double ax()                      const { return m_ax;}
  double bx()                      const { return m_bx;}
  double cx()                      const { return m_cx;}
  double ay()                      const { return m_ay;}
  double by()                      const { return m_by;}
  
  //**********Update the parameters of a track iteratively in the Fit
  void updateParameters( double dax, double dbx, double dcx,
                         double day=0., double dby= 0.  ) {
    m_ax += dax;
    m_bx += dbx;
    m_cx += dcx;
    m_ay += day;
    m_by += dby;
  }

  //validity
  void setValid( bool v )               { m_valid = v; }
  bool   valid()                    const { return m_valid; }
  //dRatio
  void setdRatio( double dRatio )       { m_dRatio = dRatio;}
  double dRatio()                 const{return m_dRatio;}
  //Chi2 & DoF
  
  void setChi2(double chi2, int nDoF) { m_chi2 = chi2;m_nDoF = nDoF; }
  double chi2()                   const { return m_chi2; }
  double chi2PerDoF()         const { return m_chi2 / m_nDoF;}
  int nDoF()                        const { return m_nDoF; }
  //MeanDy
  double meanDy()             const { return m_meanDy; }
  void setMeanDy( double meanDy )        { m_meanDy = meanDy;}  
  //MaxChi2OnHit
  void setMaxChi2(double val1)                { m_maxChi2 = val1;}
  double MaxChi2()              const { return m_maxChi2;}
 
  //BackProjection
  void setX0(double X0)                           { m_X0=X0;}
  double X0()                      const { return m_X0;}

  //RefitX status
  int RefitX()                        const { return m_RefitX;}
  void setRefitX(int Refit)                         { m_RefitX = Refit;}
  
  //dXCoord
  void setDXCoord( double dxCoord )        { m_dXCoord = dxCoord;}  
  double dXCoord()             const { return m_dXCoord; }
  //y(at a given z)
  double y( double z )          const { return m_ay + (z-m_zRef) *  m_by; }
  //Slope by
  double ySlope( )               const { return m_by; }
  
  //  X at a given Z
  double x( double z )const{
    double dz = z-m_zRef;
    return m_ax + dz * ( m_bx + dz* m_cx * (1+m_dRatio*dz) );}
  
  //  Slope X-Z plane track at a given z
  double xSlope( double z )const{ 
    double dz = z-m_zRef;
    return m_bx + 2. * dz * m_cx; 
  }
  
  //y positon on Track
  double yOnTrack( PrHit* hit )    const{ return hit->yOnTrack( m_ay - m_zRef * m_by, m_by ); }
  
  //distance from Hit of the track
  double distance( PrHit* hit )    const{ 
    double yTra = yOnTrack( hit );
    return hit->x( yTra ) - x( hit->z(yTra) );
  }
  
  //Chi2 contribution from a single Hit
  double chi2( PrHit* hit )           const{ 
    double d = distance( hit );
    return d * d * hit->w();}
  
  //DeltaY
  double deltaY( PrHit* hit )        const{
    if ( hit->isX() ) return 0.;
    return distance( hit ) / hit->dxDy();
  }  
  
  struct GreaterBySize {
    bool operator() (const PrSeedTrack& lhs, const PrSeedTrack rhs ) const { return lhs.hits().size() > rhs.hits().size(); }
  };
  void setxPredT1Mid( double xPred)
  {
    m_xPredT1 = xPred;
  }                                                                                                                                
  double xPredT1()  const
  {
    return m_xPredT1;
  }                                                                                                                                           
  void setxPredT2Mid( double xPred)
  {
    m_xPredT2 = xPred;
  }                                                                                                                                
  double xPredT2()  const
  {
    return m_xPredT2;
  }                                                                                                                                           
  void setxPredT3Mid( double xPred)
  {
    m_xPredT3 = xPred;
  }                                                                                                                                
  double xPredT3()  const
  {
    return m_xPredT3;
  }
  
protected:
  
private:

  void init() {
    m_valid = true;
    m_hits.reserve( 32 );
    //track params
    m_ax = 0.;
    m_bx = 0.;
    m_cx = 0.;
    m_by = 0.;
    m_ay = 0.;
    //tracl chi2 settings
    m_chi2 = 0.;
    m_nDoF = -1;
    m_dXCoord = 0.;
    m_meanDy  = 0.;
    m_dRatio = 0.;
    m_X0 = -99999.;
    m_maxChi2 = -99999.;
    m_xPredT1 = 0.;
    m_xPredT2 = 0.;
    m_xPredT3 = 0.;
  }
  
  
  //Global Private variables
private:
  unsigned int m_zone;
  double         m_zRef;
  PrHits           m_hits;
  bool             m_valid;
  int               m_nDoF;
  double         m_chi2;
  double         m_ax;
  double         m_bx;
  double         m_cx;
  double         m_ay;
  double         m_by;
  double         m_dRatio;
  double         m_meanDy;
  double         m_maxChi2;
  double         m_X0;
  int               m_RefitX;
  double         m_dXCoord;
  double        m_xPredT1;
  double         m_xPredT2;
  double       m_xPredT3;
  
//=========================================================
//--------------ONLINE ASSOCIATION-------------------------
//=========================================================
#ifdef TRUTH_MATCH
public:
  void setKey(int Key) 
  {  m_key = Key;}
  int key() const
  {return m_key;}
  //Avg Coord of UV Hits Added
  void setNUVAdded( int n)
  {
    m_nUVAdd = n;
  }
  int nUVAdded() const 
  {
    return m_nUVAdd;
  }
  
  // void setxPredT1Mid( double xPred){m_xPredT1 = xPred;}
  // double xPredT1()  const{return m_xPredT1;}
  // void setxPredT2Mid( double xPred){m_xPredT2 = xPred;}
  // double xPredT2()  const{return m_xPredT2;}
  // void setxPredT3Mid( double xPred){m_xPredT3 = xPred;}
  // double xPredT3()  const{return m_xPredT3;}
  
  void setChi2XProj(double chi2,int nDoF)
  {
    m_chi2X = chi2;
    m_nDoFX = nDoF;
  }
  double chi2XProj() const{return m_chi2X;}
  double nDoFX() const{ return m_nDoFX;}
  void setAvgCoord(double avgCoord){m_avgCoord = avgCoord;}
  double AvgCoord() const{ return m_avgCoord;}
  void setAbsAvgCoord(double absAvgCoord){m_absAvgCoord = absAvgCoord;}
  double AbsAvgCoord() const{return m_absAvgCoord;}
  
  //Chi2 contribution removed
  void setChi2Removed(double Val){m_chi2Removed = Val;}
  double chi2Removed()             const { return  m_chi2Removed;}
  
  //Chi2 contribution removed at step2
  void setChi2ReRemoved(double Val)      {m_Chi2ReRemoved = Val;}
  double chi2ReRemoved()           const { return m_Chi2ReRemoved;}
  
  //MaxDistance from track
  void setMaxDistance(double maxDist)    {m_maxDistance = maxDist;}
  double MaxDist()                 const { return m_maxDistance;}

  //AvgDistance
  void setAvgDist(double avg)                  {m_avgDistance = avg;}
  double avgDist()                 const { return m_avgDistance;}
  
  //AbsAvgDistance
  void setAbsAvgDist(double val)              {m_AbsavgDistance = val;}
  double AbsAvgDist()              const { return m_AbsavgDistance;}
  
  //RefitXChi2 value
  void setdeltaRefitXChi2(double deltaChi2){m_deltaRefitXChi2 = deltaChi2;}
  double deltaRefitXChi2()         const { return m_deltaRefitXChi2;}

  //DeltaRefitRefitX
  void setdeltaRefitRefitXChi2(double Chi2){m_deltaRefitRefitXChi2 = Chi2;}
  double deltaRefitRefitXChi2()     const { return m_deltaRefitRefitXChi2;}

  //First Fit Chi2
  void setChi2FirstFit(double chi2)              {m_Chi2FirstFit = chi2;}
  double Chi2FirstFit()            const { return m_Chi2FirstFit;}

  //Chi2 From Refit
  void setChi2ReFit(double chi2)                {m_Chi2ReFit = chi2;}
  double Chi2ReFit()               const { return m_Chi2ReFit;}

  //Chi2 From ReReFit
  void setChi2ReReFit(double chi2)            {m_Chi2ReReFit = chi2;}
  double Chi2ReReFit()             const { return m_Chi2ReReFit;}
  
  //x0 proj from beginning
  void setx0Beg(double x0Beg)                {m_x0 =x0Beg;}
  double x0Beg()                   const { return m_x0;}

  //TxInfinite (1st hit position picked)
  void setTxInf(double txInf)                     {m_txInf = txInf;}
  double txinf()                   const { return m_txInf;}
  
  //Tx Picked combination
  void setTxPicked(double tx)                   {m_txpicked = tx;}
  double txpicked()const{return m_txpicked;}

  //Delta picked from Tinfinite
  void setDeltaFromTinf(double Val)          {m_deltafromTinf = Val;}
  double deltaFromTinf() const{return m_deltafromTinf;}  
 
  //Flag Particle from B mesons
  void setfromB(bool fromB){m_fromB = fromB;}
  bool fromB() const{return m_fromB;}
  
  //Flag Particle from D mesons
  void setfromD(bool fromD){m_fromD = fromD;}
  bool fromD() const {return m_fromD;}
  
  //Setters and Getters for the Origin Vertex
  void setOVTX(double OVTX_X, double OVTX_Y, double OVTX_Z){
    m_ovtx_x = OVTX_X;
    m_ovtx_y = OVTX_Y;
    m_ovtx_z = OVTX_Z;}
  double OVTX_X() const {return m_ovtx_x;}
  double OVTX_Y() const {return m_ovtx_y;}
  double OVTX_Z() const {return m_ovtx_z;}
  
  //Strange Downstream
  void setStrangeDown(bool strangeDown){m_strangeDown = strangeDown;}
  bool StrangeDown() const {return m_strangeDown;}
 
  //Strange Long
  void setStrangeLong(bool strangeLong){m_strangeLong = strangeLong;}
  bool StrangeLong() const {return m_strangeLong;}
 
  //In 2< Eta <5
  void setEta25(bool eta25){m_eta25 = eta25;}
  bool Eta25() const {return m_eta25;}
  
  //Trigger Cut
  void setTrigger(bool trigger){m_trigger = trigger;}
  bool trigger() const {return m_trigger;}
  
  //P>5 GeV?
  void setover5(bool tover5){m_over5 = tover5;}
  bool over5() const {return m_over5;}
  
  //Is Electron
  void SetIsElectron(bool ise){m_isElectron = ise;}
  bool isElectron() const {return m_isElectron;}
 
  //Is Long?
  void SetIsLong(bool isLong){m_isLong = isLong;}
  bool isLong() const {return m_isLong;}
  //isDownstream?
  void SetIsDown(bool isDown){m_isDown = isDown;}
  bool isDown() const{return m_isDown;}
  //Is in Ut?
  void SetIsInUT(bool isInUT){m_isInUT = isInUT;}
  bool isInUT() const {return m_isInUT;}
  
  void SetID( int ID){
    m_ID = ID;
  }
  int ID() const{
    return m_ID;
  }
  
  //Phi of associated track
  void setPhi(double Phi){m_Phi = Phi;}
  double Phi() const {return m_Phi;}
  
  //Is in Velo?
  void SetIsInVelo(bool isInVelo){m_isInVelo = isInVelo;}
  bool isInVelo() const{return m_isInVelo;}
  
  //Associated?
  void setAssoc(bool Ok)    { m_assoc = Ok;}
  bool assoc()        const { return m_assoc;}
  
  //Momentum Associated
  void setPAssoc( double P) { m_P = P; } //Momentum of Associated Track
  double PAssoc()     const { return m_P;}
  
  //Number of associated to same/to a particle/ number of Hits on track
  void setN1N2N3(int nAssocSame,int nAssoc,int nHit){
    m_n1 = nAssocSame;
    m_n2 = nAssoc;
    m_n3 = nHit;}  
  int getN1()                      const { return m_n1;}
  int getN2()                      const { return m_n2;}
  int getN3()                      const { return m_n3;}
  
  //Purity of association
  void setPurity( double purity)  {m_Purity = purity;}
  double Purity()                 const  { return m_Purity;}
  
  //Efficiency of association
  void setEfficiency( double Efficiency) {m_Efficiency = Efficiency;}
  double Efficiency()              const { return m_Efficiency;}
  
  // //Association on X
  void setAssocX(bool Ok){m_assocX = Ok;}
  bool assocX() const{return m_assocX;}
  
  //Private Variables for the associator
private:
  void init2()
  { 
    m_nUVAdd = 0;
    m_absAvgCoord = -999.;
    m_avgCoord = -999.;
    // m_xPredT3 = 0.;
    // m_xPredT2 = 0.;
    // m_xPredT1 = 0.;
    m_chi2X = -999.;
    m_nDoFX = -999.;
    m_valid = true;
    m_hits.reserve( 32 );
    m_ax = 0.;
    m_bx = 0.;
    m_cx = 0.;
    m_by = 0.;
    m_ay = 0.; 
    m_chi2 = 0.;
    m_nDoF = -1;
    m_dXCoord = 0.;
    m_meanDy  = 0.;
    m_dRatio = 0.;
    m_X0 = -99999.;
    m_maxChi2 = -99999.; 
    //as normale
    //All necessary variables for truth matching
    m_chi2Removed = -999.;
    m_Chi2ReRemoved = -999.;
    m_maxDistance = -999.;
    m_avgDistance = -999.;
    m_AbsavgDistance = -999.;
    m_deltaRefitXChi2 = -999.;
    m_deltaRefitRefitXChi2 =  -999.;
    m_Chi2FirstFit = -999.;
    m_Chi2ReReFit = -999.;
    m_x0 = -9999.;
    m_txInf = -999.;
    m_txpicked = -999.;
    m_deltafromTinf = -9999.;
    m_fromB = false;
    m_fromD = false;
    m_ovtx_x = -9999.;
    m_ovtx_y = -9999.;
    m_ovtx_z = -9999.;
    m_strangeDown = false;
    m_strangeLong = false;
    m_eta25 = false;
    m_trigger = false;
    m_over5 = false;
    m_isElectron = false;
    m_isLong = false;
    m_isDown = false;
    m_isInUT = false;
    m_ID = -9999;
    
    m_Phi = -999.;
    m_key = -999;
    m_isInVelo = false;
    m_P = -9999.;
    m_n1 = 0;
    m_n2 = 0;
    m_n3 = 0;
    m_Purity = -1.;
    m_Efficiency = -1.;
    m_assocX = false;//for the PrSeedingXLayers
    m_assoc = false;
    m_zMidT1= -99999.;
    m_zMidT2= -99999.;
    m_zMidT3= -99999.;
    
  }
  bool m_assocX; //for the PrSeedignXLayers
  int m_key;
  double m_chi2Removed;
  double m_Chi2ReRemoved;
  double m_maxDistance;
  double m_AbsavgDistance;
  double m_avgDistance;
  double m_deltaRefitXChi2;
  double m_deltaRefitRefitXChi2;
  double m_Chi2FirstFit;
  double m_Chi2ReFit;
  double m_Chi2ReReFit;
  double m_x0;
  double m_txInf;
  double m_txpicked;
  double m_deltafromTinf;
  bool    m_fromB;
  bool    m_fromD;
  double m_ovtx_z;
  double m_ovtx_y;
  double m_ovtx_x;
  bool m_strangeLong;
  bool m_strangeDown;
  bool m_eta25;
  bool m_trigger;
  bool m_over5;
  bool m_isLong;
  bool m_isDown;
  bool m_isElectron;
  bool m_isInUT;
  double m_Phi;
  int m_ID;
  bool m_isInVelo;
  bool m_assoc;
  double m_P;
  int m_n1;
  int m_n2;
  int m_n3;
  double m_Purity;
  double m_Efficiency;
  int    m_nUVAdd;
  double m_absAvgCoord;
  double m_avgCoord;
  // double m_xPredT3;
  // double m_xPredT2;
  // double m_xPredT1;
  double m_nDoFX;
  double m_chi2X;
  double m_zMidT3;
  double m_zMidT2;
  double m_zMidT1;
  //LHCb::MCParticle m_mcPart;
  // double m_deltaRefitXChi2;
  // double m_deltaRefitRefitXChi2;
#endif  

};

typedef std::vector<PrSeedTrack> PrSeedTracks;

#endif // PRSEEDTRACK_H
