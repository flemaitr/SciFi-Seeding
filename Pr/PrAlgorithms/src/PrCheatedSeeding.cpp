// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h"
#include "Event/Track.h"
#include "Event/StateParameters.h"
// local
#include "PrCheatedSeeding.h"
#include "PrPlaneCounter.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Linker/LinkedTo.h"
#include "Linker/LinkedFrom.h"
#include "Linker/AllLinks.h"
#include "Event/FTCluster.h"
#include "Event/FTLiteCluster.h"
#include "Event/MCTrackInfo.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrCheatedSeeding
//
// 2013-02-14 : Olivier Callot
// 2013-03-21 : Yasmine Amhis Modification 
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( PrCheatedSeeding )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrCheatedSeeding::PrCheatedSeeding( const std::string& name,
                                        ISvcLocator* pSvcLocator)
: 
  GaudiHistoAlg ( name, pSvcLocator),
  m_hitManager(nullptr),
  m_geoTool(nullptr),
  m_debugTool(nullptr),
  m_timerTool(nullptr)
{
  declareProperty( "InputName",           m_inputName            = LHCb::TrackLocation::Forward );
  declareProperty( "OutputName",          m_outputName           = LHCb::TrackLocation::Seed    );
  declareProperty( "HitManagerName",      m_hitManagerName       = "PrFTHitManager"             );
  declareProperty( "DecodeData",          m_decodeData           = false                        );
  declareProperty( "XOnly",               m_xOnly                = false                        );
  
  declareProperty( "MaxChi2InTrack",      m_maxChi2InTrack       = 5.5                          );
  declareProperty( "TolXInf",             m_tolXInf              = 0.5 * Gaudi::Units::mm       );
  declareProperty( "TolXSup",             m_tolXSup              = 8.0 * Gaudi::Units::mm       );
  declareProperty( "MinXPlanes",          m_minXPlanes           = 5                            );
  declareProperty( "MaxChi2PerDoF",       m_maxChi2PerDoF        = 4.0                          );
  declareProperty( "MaxParabolaSeedHits", m_maxParabolaSeedHits  = 4                            );
  declareProperty( "TolTyOffset",         m_tolTyOffset          = 0.002                        );
  declareProperty( "TolTySlope",          m_tolTySlope           = 0.015                        );
  declareProperty( "MaxIpAtZero",         m_maxIpAtZero          = 5000.                        );
  
  // Parameters for debugging
  declareProperty( "DebugToolName",       m_debugToolName         = ""                          );
  declareProperty( "WantedKey",           m_wantedKey             = -100                        );
  declareProperty( "TimingMeasurement",   m_doTiming              = false                       );
  declareProperty( "PrintSettings",       m_printSettings         = false                       );
  
}
//=============================================================================
// Destructor
//=============================================================================
PrCheatedSeeding::~PrCheatedSeeding() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrCheatedSeeding::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  m_hitManager = tool<PrHitManager>( m_hitManagerName );
  m_hitManager->buildGeometry();
  m_geoTool = tool<PrGeometryTool>("PrGeometryTool");

  m_debugTool   = 0;
  if ( "" != m_debugToolName ) {
    m_debugTool = tool<IPrDebugTool>( m_debugToolName );
  } else {
    m_wantedKey = -100;  // no debug
  }

  if ( m_doTiming) {
    m_timerTool = tool<ISequencerTimerTool>( "SequencerTimerTool/Timer", this );
    m_timeTotal   = m_timerTool->addTimer( "PrSeeding total" );
    m_timerTool->increaseIndent();
    m_timeFromForward = m_timerTool->addTimer( "Convert Forward" );
    m_timeXProjection = m_timerTool->addTimer( "X Projection" );
    m_timeStereo      = m_timerTool->addTimer( "Add stereo" );
    m_timeFinal       = m_timerTool->addTimer( "Convert tracks" );
    m_timerTool->decreaseIndent();
  }

  if( m_decodeData ) info() << "Will decode the FT clusters!" << endmsg;

  // -- Print the settings of this algorithm in a readable way
  if( m_printSettings){
    
    info() << "========================================"             << endmsg
           << " InputName            = " <<  m_inputName             << endmsg
           << " OutputName           = " <<  m_outputName            << endmsg
           << " HitManagerName       = " <<  m_hitManagerName        << endmsg
           << " DecodeData           = " <<  m_decodeData            << endmsg
           << " XOnly                = " <<  m_xOnly                 << endmsg
           << " MaxChi2InTrack       = " <<  m_maxChi2InTrack        << endmsg
           << " TolXInf              = " <<  m_tolXInf               << endmsg
           << " TolXSup              = " <<  m_tolXSup               << endmsg
           << " MinXPlanes           = " <<  m_minXPlanes            << endmsg
           << " MaxChi2PerDoF        = " <<  m_maxChi2PerDoF         << endmsg
           << " MaxParabolaSeedHits  = " <<  m_maxParabolaSeedHits   << endmsg
           << " TolTyOffset          = " <<  m_tolTyOffset           << endmsg
           << " TolTySlope           = " <<  m_tolTySlope            << endmsg
           << " MaxIpAtZero          = " <<  m_maxIpAtZero           << endmsg
           << " DebugToolName        = " <<  m_debugToolName         << endmsg
           << " WantedKey            = " <<  m_wantedKey             << endmsg
           << " TimingMeasurement    = " <<  m_doTiming              << endmsg
           << "========================================"             << endmsg;
  }
  
  static const std::string histoDir = "Track/" ;
  if ( "" == histoTopDir() ) setHistoTopDir(histoDir);

  IHistoTool* htool = tool<IHistoTool>( "HistoTool","PrPlotFTHitsHistos",this ) ;
  m_ghtool = dynamic_cast<GaudiHistoTool*>(htool) ;
  
  // -- catch the possible failure of the dynamic cast
  if( m_ghtool == NULL){
    error() << "Dynamic cast of Gaudi Histogramming Tool failed!" << endmsg;
    return StatusCode::FAILURE;
  }
  
  m_histoTool = htool;

  m_diffNumIDs = 0;
  

#ifdef DEBUG_HISTO 
  setHistoTopDir("FT/");
#endif 

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PrCheatedSeeding::execute() {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  

  LHCb::Tracks* result = new LHCb::Tracks();
  put( result, m_outputName );

  //m_hitManager->decodeData();
  

 

  makeLHCbTracks( result );

  

  
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode PrCheatedSeeding::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return GaudiHistoAlg::finalize();  // must be called after all other actions
}


//=========================================================================
//  Convert to LHCb tracks
//=========================================================================
void PrCheatedSeeding::makeLHCbTracks ( LHCb::Tracks* result ) {

  /*
  LinkedTo<LHCb::MCParticle, LHCb::FTCluster> myClusterLink ( evtSvc(), msgSvc(), LHCb::FTClusterLocation::Default );
  LinkedTo<LHCb::MCHit, LHCb::FTCluster> myFTCluster2MCHitLink ( evtSvc(),msgSvc(), LHCb::FTClusterLocation::Default + "2MCHits");
  
  typedef FastClusterContainer<LHCb::FTLiteCluster,int> FTLiteClusters;
  FTLiteClusters* clus = get<FTLiteClusters>( LHCb::FTLiteClusterLocation::Default );
  LHCb::MCParticles* mcParts = getIfExists<LHCb::MCParticles> ( LHCb::MCParticleLocation::Default );

  MCTrackInfo trackInfo( evtSvc(), msgSvc() );

  for( LHCb::MCParticles::const_iterator iPart = mcParts->begin(); iPart != mcParts->end(); ++iPart){
    
    const LHCb::MCParticle* mcPart = *iPart;

    bool isLong = trackInfo.hasVeloAndT( mcPart );
    bool isDown = trackInfo.hasT( mcPart ) && trackInfo.hasTT( mcPart );
    bool isSeed = trackInfo.hasT( mcPart );
    

    //if( !isLong && !isDown ) continue;
    if( !isSeed ) continue;
    
    LHCb::Track* tmp = new LHCb::Track;
    tmp->setType( LHCb::Track::Ttrack );
    tmp->setHistory( LHCb::Track::PrSeeding );
    tmp->setPatRecStatus( LHCb::Track::PatRecIDs );
    
    std::array<int, 24> firedXLayers;
    std::array<int, 24> firedStereoLayers;
    firedXLayers.fill(0);
    firedStereoLayers.fill(0);
    
    int totHits = 0;
    
    for(int i = 0; i < 24; ++i){
      
      PrHitZone* zone = m_hitManager->zone( i );

      for ( PrHits::iterator itH = zone->hits().begin(); zone->hits().end() != itH; ++itH){
        
        PrHit* hit = *itH;
        
        LHCb::MCParticle* mcPart1 = myClusterLink.first( hit->id().ftID() ); 
        bool found = false;
        
        while( mcPart1 != nullptr){
          if( mcPart1 == mcPart){
            found = true;
          }
          mcPart1 = myClusterLink.next();
        }
      
  
        if( hit->isX() && found ){
          if( firedXLayers[i] == 0){
            firedXLayers[i]++;
          }
        }
        if( !(hit->isX()) && found ){
          if( firedStereoLayers[i] == 0){
            firedStereoLayers[i]++;
          }
        }
        if( found ){
          totHits++;
          tmp->addToLhcbIDs( hit->id() );
        }
        

      }
      
      

    }
    
        
    int sumLowerX = 0;
    int sumUpperX = 0;
    int sumStereo = 0;
    

    for(int i = 0; i < 24; i = i+2){
      sumLowerX += firedXLayers[i];
    }
    
    for(int i = 1; i < 24; i = i+2){
      sumUpperX += firedXLayers[i];
    }

    for(int i = 0; i < 24; i++){
      sumStereo += firedStereoLayers[i];
    }
    

    //std::cout << "sumLowerX: " << sumLowerX << " sumUpperX " << sumUpperX << " sumStereo " << sumStereo << std::endl;
    
    if( (sumLowerX < 5 && sumUpperX < 5) || sumStereo < 4 || totHits < 9){
      delete tmp;
      continue;
    }
    
    



    // -- these are obviously useless numbers, but it should just make the checkers happy
    double qOverP = 0.01;

    LHCb::State tState;
    double z = StateParameters::ZEndT;
    tState.setLocation( LHCb::State::AtT );
    tState.setState( 100, 50, z, 0.1, 0.1, qOverP );
    
    //== overestimated covariance matrix, as input to the Kalman fit

    tState.setCovariance( m_geoTool->covariance( qOverP ) );
    tmp->addToStates( tState );

    result->insert( tmp );
    
    
  }
  
  
  
  
  

 */

}



