#ifndef PRSEEDINGNEW_H 
#define PRSEEDINGNEW_H 1

// Include files 
// from Gaudi
#include "PrSeedTrack.h" //The define TRUTH_MATCH is there

//uncomment this line if you want to do truth matching and do plots
#ifdef TRUTH_MATCH
#include "GaudiAlg/GaudiTupleAlg.h"
#else
#include "GaudiAlg/GaudiAlgorithm.h"
#endif


#include "GaudiAlg/ISequencerTimerTool.h"
#include "PrKernel/IPrDebugTool.h"
#include "PrKernel/PrHitManager.h"
#include "PrGeometryTool.h"
#include "TfKernel/RecoFuncs.h"
#include "Event/MCParticle.h"

// timing
#include "rdtsc.h"
#include <ctime>
#include <time.h>
#include <sys/time.h>
#include "realtime.h"

/** @class PrSeedingNew PrSeedingNew.h
 *  
 *
 *  @author renato quagliani
 *  @date   2015-03-11
 */
#ifdef TRUTH_MATCH
class PrSeedingNew : public GaudiTupleAlg{ 
#else
  class PrSeedingNew : public GaudiAlgorithm {
#endif
  public: 
    /// Standard constructor
    PrSeedingNew( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~PrSeedingNew( ); ///< Destructor
    virtual StatusCode initialize();    ///< Algorithm initialization
    virtual StatusCode execute   ();    ///< Algorithm execution
    virtual StatusCode finalize  ();    ///< Algorithm finalization
    
  protected:
    bool matchKey( const PrHit* hit ) {
      if ( m_debugTool ) return m_debugTool->matchKey( hit->id(), m_wantedKey );
      return false;
    };
    
    
    bool matchKey( const PrSeedTrack& track ) {
      if ( !m_debugTool ) return false;
    for ( PrHits::const_iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH ) {
      if ( m_debugTool->matchKey( (*itH)->id(), m_wantedKey ) ) return true;
    }
    return false;
    };
    
    void setKey( int key ) {
      m_wantedKey = key;
      return;
    };
    
    
    /** @brief Collect Hits in X layers for High momentum tracks
     *  @param part lower (1, y>0) or upper(0, y<0) half of the detector 
     *  @param icase 1(T1-1x, T3-2x), 2(T1-2x,T3/2x),3(T3-2x,T3.1x)
     */
    void findXProjectionsHighMomentum( unsigned int part ,unsigned int icase);
      /** @brief Collect Hits in X layers for Low momentum tracks
       *  @param part lower (1, y>0) or upper(0, y<0) half of the detector 
       *  @param icase 1(T1-1x, T3-2x), 2(T1-2x,T3/2x),3(T3-2x,T3.1x)
     */
    void findXProjectionsLowMomentum( unsigned int part, unsigned int icase);
    /** @brief Collect hits in the stereo-layers. 
     *   @param part lower (1) or upper (0) half
     */
    void addStereo2( unsigned int part , unsigned int iCase);
   
    /** @brief Remove Clones of produced tracks
     */
    void removeClonesX(unsigned int maxCommon);
    void removeClones(unsigned int maxCommon);
    /** @brief Set the Chi2 value and Chi2DoF for the track after the X+U-V search
     */
    void setChi2(PrSeedTrack& track);
   
    /** @brief Flag Hits under conditions at the end of the iCase loop 
     */
    void flagHits(unsigned int icase);
    /** @bried Flag Hits under conditions (similar to removal of clones ) sort of hybrid
     */
    void flagHits2();
    /** @brief Fit the track combining the XZ and YZ projections
     *  @param track The track to fit
     *  @param Refit Iteration in the Refitting after removal worst hit
     *  @return bool Success of the XY+XZ Fit
     **/
    bool fitSimultaneouslyXY( PrSeedTrack& track ,int Refit , unsigned int iCase);
    
    /** @brief Fit the track combining the only in the XZ plane
     *  @param track The track to fit
     *  @param Refit Iteration in the Refitting after removal worst hit
     *  @return bool Success of the XZ Fit
     **/
    bool fitXProjection( PrSeedTrack & track , int Refit , unsigned int iCase ,bool& doreFit);
    /** @brief Remove the hit which gives the largest contribution to the chi2 and refit XZ
     *  @param track The track to fit
     *  @return bool Success of the fit
     */
    bool removeWorstAndRefitX( PrSeedTrack& track ,int step , unsigned int iCase ,bool& doRefit);
    
     /** @brief Remove the hit which gives the largest contribution to the chi2 and refit XZ + YZ
      *  @param track The track to fit
     *  @return bool Success of the fit
     */
    bool removeWorstAndRefit( PrSeedTrack& track , int step, unsigned int iCase );
    
     /** @brief Set the chi2 of the track
      *  @param track The track to set the chi2 of 
      */
    void setChi2X( PrSeedTrack& track );
    /** @brief Transform the tracks from the internal representation into LHCb::Tracks
     *  @param tracks The tracks to transform
     */
    void makeLHCbTracks( LHCb::Tracks* result );
    
    /** @brief Print some information of the hit in question
     *  @param hit The hit whose information should be printed
     *  @param title Some additional information to be printed
     */
    void printHit( const PrHit* hit, std::string title="" );
    
    /** @brief Print some information of the track in question
     *  @param hit The track whose information should be printed
     */
    void printTrack( PrSeedTrack& track );
    
    /** @brief Internal method to construct parabolic parametrisation out of three hits, using Cramer's rule.
   *  @param hit1 First hit
   *  @param hit2 Second hit
   *  @param hit3 Third hit
   *  @param a quadratic coefficient
   *  @param b linear coefficient
   *  @param c offset
   */
    void solveParabola(const PrHit* hit1, const PrHit* hit2, const PrHit* hit3, double& a, double& b, double& c);
    /** @brief Internal method to construct parabolic parametrisation + cubic correction included out of three hits, using Cramer's rule.
   *  @param hit1 First hit
   *  @param hit2 Second hit
   *  @param hit3 Third hit
   *  @param a quadratic coefficient
   *  @param b linear coefficient
   *  @param c offset
   */
    void solveParabola2(const PrHit* hit1, const PrHit* hit2, const PrHit* hit3, double& a1, double& b1, double& c1);
    
    /** @brief Internal method to invert the association relation: it created the map(LHCbID,vector<MCParticle>) which can be used to do truth matching.
     */
    void createMap();
     /** @brief Internal method to check if the internal representation of the track is associated or not to an MCParticle. Done using LHCbID informations. 
         @param track The track to check the association with MCParticle
         @return bool Association status of the track based on the m_purity and m_efficiency check
     */
    bool Assoc(PrSeedTrack& track);
    LHCb::MCParticle AssocP(PrSeedTrack & track);

    /// Classe to find lower bound of x of PrHits
    class lowerBoundX {
    public:
      bool operator() (const PrHit* lhs, const double testval ) const { return lhs->x() < testval; }
    };
    /// Classe to find upper bound of x of PrHits
    class upperBoundX {
    public:
      bool operator() (const double testval, const PrHit* rhs) const { return testval < rhs->x(); }
    };
    /// Class to compare x positions of PrHits
    class compX {
    public:
      bool operator() (const PrHit* lhs, const PrHit* rhs ) const { return lhs->x() < rhs->x(); }
    };
    ///Class to find lower bound of LHCbIDs of PrHits
    class lowerBoundLHCbID {
    public:
      bool operator() (const PrHit* lhs, const LHCb::LHCbID id ) const { return lhs->id() < id; }
    };
    ///Class to compare LHCbIDs of PrHits
    class compLHCbID {
    public:
      bool operator() (const PrHit* lhs, const PrHit* rhs ) const { return lhs->id() < rhs->id(); }
    };
    
  private:
    //-------------Names for input container(if forward as input), output container name, HitManager name
    std::string        m_inputName;
    std::string        m_outputName;
    std::string        m_hitManagerName;
    //-------------Global configuration of the algorithm
    bool               m_decodeData;
    bool               m_xOnly;
    unsigned int   m_minXPlanes;
    unsigned int   m_nCases;
    bool               m_doTiming;
    bool               m_printSettings;
    bool               m_useCubic;
    bool               m_removeClones;
    bool               m_removeClonesX;
    bool               m_removeClones_InLoop;
    
    bool               m_FlagHits;
    bool               m_removeFlagged;
    //------------X-search parametrisation
    //1st / Last Layer search windows
    double         m_alphaCorrection_Tight;
    double         m_TolHp_Tight;
    double          m_alphaCorrection;
    double          m_TolHp;
    //Add of third hit in T2
    double           m_x0Corr;
    double           m_m1; //not used now
    double           m_m2; //not used now
    double           m_x0Offset; //not used now
    double           m_yOff1; //Used
    double           m_yOff1_Loose;
    double           m_yOff2;
    double           m_yOff2_Loose;
    unsigned int m_maxParabolaSeedHits;
    //Look up in remaining X layers
    double           m_maxDeltaPar;
    double            m_maxDeltaPar_Loose;
    //Look up in remaining X layers and Track Fitting parameters
    double           m_dRatio0;
    double           m_dRatio1;
    double           m_dRatio2;
    double           m_ConstC;
    //--------_Fit X parametrisation
    double            m_maxChi2_X;
    //Add the parameters for the Chi2 Cut
    unsigned int     m_nSigmaOffX;
    unsigned int     m_X0Max;
    

     //-----------------UV search parametrisation
    //Added
    bool            m_force12;
    double          m_alphaUV;
    double          m_doAsymmUV;
    //
    double          m_coord;
    double          m_tolTyOffset;
    double          m_tolTySlope;
 
    double          m_tolXStereo;
    //Triangle Fix
    bool            m_useFix; 
    bool            m_removeHole;
    bool            m_useFix2ndOrder;
    double          m_tolTriangle;
    double          m_tolYOffset;
    //X+Y fit configure
    double          m_maxChi2HitsX;
    double          m_maxChi2PerDoF;
    
    //Clone removal setting
    unsigned int              m_nCommonX;
    unsigned int              m_nCommonUV;
    unsigned int              m_nUsed;
    
    //Flag Hits Settings
    double         m_MaxChi2Flag;
    double         m_MaxX0Flag;
    unsigned int  m_SizeFlag;
    
    

    //------------------Truth Matching 
    bool            m_doHistos;
    bool             m_doTuple; //Histograms are auto-created with the #define TRUTH_MATCH
    bool             m_Cheat; //Cheat : pass only tracks which are associated to figure out the absolute loss in efficiency
    bool             m_doAssociation;  //It create the association map and it sets for the track the association flag
    bool              m_UseSeed;
    bool              m_UseLong;
    bool              m_UseDown;
    bool              m_NoElectrons;
    double          m_PCut;
    double          m_Purity;
    double          m_Efficiency;
    
    std::map<LHCb::LHCbID , std::vector< LHCb::MCParticle*>> m_map;
    

    //--------------------Global things

    PrHitManager*   m_hitManager;
    PrGeometryTool* m_geoTool;
    
    //== Debugging controls
    std::string     m_debugToolName;
    int             m_wantedKey;
    IPrDebugTool*   m_debugTool;

    //-------------------Internal timer
    double m_timer;
    double m_ctimer;
    int m_counter;
    
    
    //-------------------Containers
    std::vector<PrSeedTrack>       m_trackCandidates;
    std::vector<PrSeedTrack>       m_xCandidates;
    std::vector<PrHitZone*>        m_zones;
    ISequencerTimerTool* m_timerTool;
    int            m_timeTotal;
    int            m_timeFromForward;
    int            m_timeXProjection;
    int            m_timeStereo;
    int            m_timeFinal;

    //-------------------Algorithms Constants

    // Stations
    static const int s_T1X1 = 0;
    static const int s_T1U  = 2;
    static const int s_T1V  = 4;
    static const int s_T1X2 = 6;
    static const int s_T2X1 = 8;
    static const int s_T2U  = 10;
    static const int s_T2V  = 12;
    static const int s_T2X2 = 14;
    static const int s_T3X1 = 16;
    static const int s_T3U  = 18;
    static const int s_T3V  = 20;
    static const int s_T3X2 = 22;

    static const int s_down = 0;
    static const int s_up   = 1;
  };
  
#endif // PRSEEDINGNEW_H
