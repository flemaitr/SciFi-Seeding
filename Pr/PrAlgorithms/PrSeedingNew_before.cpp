
// Include files
// from Gaudi
#include "GaudiKernel/AlgFactory.h" 
#include "Event/Track.h"
#include "Event/StateParameters.h"
#include "Math/CholeskyDecomp.h"
// local
#include "PrSeedingNew.h"
#include "PrPlaneCounter.h"
#include "Event/MCTrackInfo.h"
#include "PrPlaneCounter.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Linker/LinkedTo.h"
#include "Linker/LinkedFrom.h"
#include "Linker/AllLinks.h"
#include "Event/FTCluster.h"
#include "Event/FTLiteCluster.h"
#include "Event/MCParticle.h"
//-----------------------------------------------------------------------------
// Implementation file for class : PrSeedingNew
//
// 2015-03-11 : renato quagliani
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( PrSeedingNew )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================


PrSeedingNew::PrSeedingNew( const std::string& name,
                            ISvcLocator* pSvcLocator)
: GaudiTupleAlg( name , pSvcLocator )
  ,m_hitManager(nullptr)
  ,m_geoTool(nullptr)
  ,m_debugTool(nullptr)
  ,m_timerTool(nullptr)
{
  declareProperty( "InputName" ,         m_inputName="");
  declareProperty( "OutputName",         m_outputName=LHCb::TrackLocation::Seed);
  declareProperty( "HitManagerName",     m_hitManagerName= "PrFTHitManager");
  declareProperty( "DecodeData",         m_decodeData= false);
  declareProperty( "XOnly",              m_xOnly= true);
  declareProperty( "MinXPlanes",         m_minXPlanes= 5);
  declareProperty( "TimingMeasurement",  m_doTiming= false);
  declareProperty( "PrintSettings",      m_printSettings= true);
  declareProperty( "DoPlots",            m_doPlots= true);
  declareProperty( "HighPAlpha",         m_alphaCorrection = 120.64 *Gaudi::Units::mm); //L0 selection
  //  delcareProperty( "HighPAlphaTight" ,   m_
  declareProperty( "TolHp",              m_TolHp = 250.0 *Gaudi::Units::mm); //L0 selection
  declareProperty( "x0Corr",             m_x0Corr = 0.0023); //L1 selection
  declareProperty( "m_m1",               m_m1 = 0.); //L1 selection Bending 1
  declareProperty( "m_m2",               m_m2 = 0.); //L1 selection Bending 2
  declareProperty( "m_x0Offset",         m_x0Offset =0.); //L1 selection x0Offset
  declareProperty( "m_yOff1",            m_yOff1 = +3.0); //L1 selection tollerance
  declareProperty( "m_yOff2",            m_yOff2 = -3.0); //L1 selection  tollerance
  declareProperty( "maxDelta_Par",       m_maxDeltaPar = 2.0 *Gaudi::Units::mm); //L2 selection
  declareProperty( "maxParabolaSeedHits",m_maxParabolaSeedHits = 8); //L2 selection
  declareProperty( "dRatio",              m_dRatio= -0.000244); // global 
  declareProperty( "minNCommon",          m_nCommon = 3); //for clone removal
  declareProperty( "useCubic",            m_useCubic = true);
  declareProperty( "m_maxChi2HitsX",      m_maxChi2HitsX = 5.5);
  declareProperty( "m_maxOffset",         m_maxOffset = 0.7 *Gaudi::Units::mm);
  declareProperty( "RemoveFlagged",       m_removeFlagged = false);
  declareProperty( "m_doAssociation",     m_doAssociation = false);
  declareProperty( "m_UseSeedTrack" ,     m_UseSeed = true);
  declareProperty( "m_UseLong" ,          m_UseLong = true);
  declareProperty( "m_UseDown" ,          m_UseDown = true);
  declareProperty( "m_NoElectrons" ,      m_NoElectrons= true);
  declareProperty( "m_doPCut",            m_doPCut=true);
  declareProperty( "m_PCut" ,             m_PCut = 5000. *Gaudi::Units::MeV);
  declareProperty( "m_removeCloneX" ,     m_removeCloneX = false);  
  declareProperty( "ConstC",              m_ConstC = 2.452e8 );
  declareProperty( "FlagHits",            m_FlagHits);
  declareProperty( "Cheat" ,              m_Cheat = false);
  declareProperty( "Purity" ,             m_Purity = 0.79);
  declareProperty( "Efficiency",          m_Efficiency = 0.79);
  declareProperty( "nUsed" ,              m_nUsed = 1);
  declareProperty( "nCases" ,             m_nCases = 3);
}
//=============================================================================
// Destructor
//=============================================================================
PrSeedingNew::~PrSeedingNew() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrSeedingNew::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  
  m_hitManager= tool<PrHitManager>( m_hitManagerName );
  m_geoTool = tool<PrGeometryTool>("PrGeometryTool");
  
  m_hitManager->buildGeometry();
  m_debugTool = 0;
  if ( "" != m_debugToolName ) {
    m_debugTool = tool<IPrDebugTool>( m_debugToolName );
    info()<<"Debug tool "<<m_debugToolName<<" loaded."<<endmsg;
  } else {
    m_wantedKey = -100;  //no debug
  }  
  if ( m_doTiming) {
    m_timerTool = tool<ISequencerTimerTool>( "SequencerTimerTool/Timer", this );
    m_timeTotal   = m_timerTool->addTimer( "PrSeeding total" );
    m_timerTool->increaseIndent();
    m_timeFromForward = m_timerTool->addTimer( "Convert Forward" );
    m_timeXProjection = m_timerTool->addTimer( "X Projection" );
    m_timeStereo      = m_timerTool->addTimer( "Add stereo" );
    m_timeFinal       = m_timerTool->addTimer( "Convert tracks" );
    m_timerTool->decreaseIndent();
  }
  if( m_decodeData ) info() << "Will decode the FT clusters!" << endmsg;
  if( m_printSettings){
    info() <<"======================================="<<endmsg
           <<" InputName               = "    <<  m_inputName             << endmsg
           << " OutputName            = "    <<  m_outputName           << endmsg
           << " HitManagerName     = "    <<  m_hitManagerName     << endmsg
           << " DecodeData            = "    <<  m_decodeData          << endmsg
           << " XOnly                     = "    <<  m_xOnly                    << endmsg
           << " MinXPlanes              = "    <<  m_minXPlanes            << endmsg
           << " UseCubicCorrection  = "    <<  m_useCubic               << endmsg
      //<< " UseFix               = "    <<  m_useFix                << endmsg
           << " TimingMeasurement    = "    <<  m_doTiming              << endmsg
           << " doPlots              =  "   <<  m_doPlots               <<endmsg
           << " doAssociation     =  "   << m_doAssociation      <<endmsg
           << " L0_AlphaRotation = "   << m_alphaCorrection    <<endmsg   //1st -last layer combination
           << " L0_Tollerance      =  "  << m_TolHp                  <<endmsg   //1st -last layer combination
           << " L1_x0Correction   =  "  << m_x0Corr                 <<endmsg   //T2 search 3rd Hit 
           << " L1_yOff1            =   "  << m_yOff1                   <<endmsg   //T2 search 3rd Hit
           << " L1_yOff2           =    "  << m_yOff2                  <<endmsg    //T2 search 3rd Hit
           << " L1_maxParabolaSeedHits = "<<m_maxParabolaSeedHits <<endmsg
           << " L1_MaxDeltaXatZ = " <<m_maxDeltaPar          <<endmsg    //Other T-station search distance from parabola
           << " L2_MaxOffset     = "   <<m_maxOffset             <<endmsg   //In the Fit max Offset from Fit of a single Hit(Optional --> try to implement it & see the Chi2 distribution"
           << " L2_mDratioForFit = " <<m_dRatio                   <<endmsg  //Fitting And Clone Removal Parameters
           << " L3_RemoveClone = "<<m_removeCloneX <<endmsg
           << " L3_CloneCommonHitXStep  = " << m_nCommon << endmsg
           << " L2_maxChi2ContributionHit = "<<m_maxChi2HitsX<<endmsg
           << " nCommon" <<m_nCommon <<endmsg
           << " removeClones" <<m_removeCloneX<<endmsg
           << " constC "<<m_ConstC<<endmsg
           << " flagHits "<<m_FlagHits<<endmsg
           << " nUsed" <<m_nUsed<<endmsg
           << " \t Debugging"<<endmsg                                                // Debugging
           << " UseSeedTracks "<<m_UseSeed<<endmsg
           << " UseLongTracks " <<m_UseLong<<endmsg
           << " UseDownstreamTracks"<<m_UseDown<<endmsg
           << " RemoveElectrons "<<m_NoElectrons<<endmsg
           << " Momentum P Cut"<<m_PCut <<endmsg
           << " Cheat " <<m_Cheat <<endmsg
           << "========================================"                << endmsg;
  }
  if(m_doPlots)
    setHistoTopDir("FT/");
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PrSeedingNew::execute() {
  using namespace Tuples ;
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  if ( m_doTiming ) {
    m_timerTool->start( m_timeTotal );
    m_timerTool->start( m_timeFromForward );
  }
  LHCb::Tracks* result = new LHCb::Tracks();
  put( result, m_outputName );
  if( m_decodeData ){
    debug()<<"Decoding Data"<<endmsg;
    m_hitManager->decodeData();
    debug()<<"Decoding Done"<<endmsg;
  }
  int multiplicity[24];
  int multiplicityTot = 0;
  char zoneName[100];
  
  for ( unsigned int zone = 0; m_hitManager->nbZones() > zone; ++zone ) {
    multiplicity[zone]=0;
    sprintf (zoneName, "Multiplicity_InZone_%i",zone);
    for ( PrHits::const_iterator itH = m_hitManager->hits( zone ).begin();
          m_hitManager->hits( zone ).end() != itH; ++itH ) {
      (*itH)->setUsed( false );
      multiplicity[zone]++;
      multiplicityTot++;
    }  
   }
  
  //========================================================
  // Remove Seed segment if we pass the  Forward as Input
  //========================================================
  if ( "" != m_inputName ) {
    debug()<<"Removing Seed Segment from Forward Tracking"<<endmsg;
    for(int i = 0; i < 24; i++){
      PrHitZone* zone = m_hitManager->zone(i);
      std::stable_sort( zone->hits().begin(),  zone->hits().end(), compLHCbID());
    }
    LHCb::Tracks* forward = get<LHCb::Tracks>( m_inputName );
    //Loop over forward tracks container
    for ( LHCb::Tracks::iterator itT = forward->begin(); forward->end() != itT; ++itT ) {
      //Vector of LHCbID
      std::vector<LHCb::LHCbID> ids;
      ids.reserve(20);
      // Loop over LHCbIDs of the forward track
      for ( std::vector<LHCb::LHCbID>::const_iterator itId = (*itT)->lhcbIDs().begin();
            (*itT)->lhcbIDs().end() != itId; ++itId ) {
        if ( (*itId).isFT() && (*itId).ftID().layer() < 12 ) {
          LHCb::FTChannelID ftId =(*itId).ftID();
          int zoneNb = 2 * ftId.layer() + ftId.mat(); //zones top are even (0, 2, 4, ....,22)  and zones bottom are odd
          //Load the PrHitZone
          PrHitZone* zone = m_hitManager->zone(zoneNb);
          
          // -- The hits are sorted according to LHCbID, we can therefore use a lower bound to speed up the search
          
          PrHits::iterator itH = std::lower_bound(  zone->hits().begin(),  zone->hits().begin(), *itId, lowerBoundLHCbID() );
                    
          for ( ; zone->hits().end() != itH; ++itH ) {
            if( *itId < (*itH)->id() ) break;
            if ( (*itH)->id() == *itId ) (*itH)->setUsed( true );
          }
          ids.push_back( *itId );
        }
      }
      
      
      // Forward output loaded to TTrack container with History flag
      LHCb::Track* seed = new LHCb::Track;
      seed->setLhcbIDs( ids );
      seed->setType( LHCb::Track::Ttrack );
      seed->setHistory( LHCb::Track::PrSeeding );
      seed->setPatRecStatus( LHCb::Track::PatRecIDs );
      seed->addToStates( (*itT)->closestState( 9000. ) );
      result->insert( seed );
    }
    
    
    //=======================================
    // Sort hits according to x for each zone
    //=======================================
    for(int i = 0; i < 24; i++){
      PrHitZone* zone = m_hitManager->zone(i);
      std::stable_sort( zone->hits().begin(),  zone->hits().end(), compX());
    }
  }
  
  if(m_doAssociation)
  {
    createMap();
  }
  
  
  
  //=========================================
  // Hits are ready to be processed
  //=========================================
  
  m_trackCandidates_highP.clear();
  if ( m_doTiming ) {
    m_timerTool->stop( m_timeFromForward );
  }
  // -- Loop through lower and upper half
  for ( unsigned int part= 0; 2 > part; ++part ){
    //Start timing
    if ( m_doTiming ) {
      m_timerTool->start( m_timeXProjection);
    }
    //1st -- X layers hits & projections
    debug()<<"FindXProjection for High Momentum"<<endmsg;
    findXProjectionsHighMomentum( part );
    debug()<<"FindXProjection for High Momentum Done"<<endmsg;
    if ( m_doTiming ) {
      m_timerTool->stop( m_timeXProjection);
      m_timerTool->start( m_timeStereo);
    }
      
    //Add Stereo (deactivated for the moment)
    //if ( ! m_xOnly ) addStereo2( part ); //to be written (method to addStereoLayers)
    if ( m_doTiming ) {
      m_timerTool->stop( m_timeStereo);
    }
  }//end part loop 
  if ( m_doTiming ) {
    m_timerTool->start( m_timeFinal);
  }
  // Convert to LHCb Tracks
  debug()<<"Making LHCb Tracks"<<endmsg;
  makeLHCbTracks( result );
  debug()<<"Making LHCb Tracks Done"<<endmsg;
  if ( m_doTiming ) {
    m_timerTool->stop( m_timeFinal);
    double tot = m_timerTool->stop( m_timeTotal );
    info() << format( "                                            Time %8.3f ms", tot )
           << endmsg;
    if(m_doPlots)
      plot2D(multiplicityTot,tot,"Multiplicity/timing","timing",0.,10000.,0.,1000.,100,100);
  }
  m_map.clear();
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode PrSeedingNew::finalize() {
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;
  
  return GaudiTupleAlg::finalize();  // must be called after all other actions
}
//====================================================================
//Find XZ plane projections of tracks
//====================================================================

void PrSeedingNew::findXProjectionsLowMomentum(unsigned int part)
{
  //Will be a copy paste of the LowMomentum once we run the cases
  //I am doing anything in this step
  debug()<<"Entering the Case Loop"<<endmsg;
  for ( unsigned int iCase = 0 ; 3 > iCase ; ++iCase ) {
    
    //just do the 1st one here //1st layer and last one
    //int firstZone = part;
    //int lastZone  = 22 + part; 
    //Case 1
    //if ( 1 == iCase ) firstZone = part + 6; // firstZone = 2nd X-Layer in T1
    //Case 3
    //if ( 2 == iCase ) lastZone  = 16 + part;
  }
}

void PrSeedingNew::findXProjectionsHighMomentum(unsigned int part)
{
  //The Main idea of this step is to look for high momentum tracks with relatively small search windows:
  //Provide at the end of the 3 cases a container for X projections: between one case and the next one we flag hits 
  //On tracks we think are good: this is a delicate step since if we flag a track which is a ghost we are privating the next case to use a good Hit
  //Goal is to reduce as much as possible the ghost rate at this step in order to be able to flag only good tracks.
  //In addition the way it's implemented now it applies very tight cuts requiring 4 hits out of 6 (default) and the goal is to obtain a sample with ~100% purity and low ghost rate
  //Loop on 3 
  m_xCandidates_highP.clear();
  //3 cases 
  debug()<<"Entering the Case Loop"<<endmsg;
  for ( unsigned int iCase = 0 ; m_nCases > iCase ; ++iCase ) {
    
    //just do the 1st one here //1st layer and last one
    int firstZone = part;
    int lastZone  = 22 + part; 
    //Case 1 
    if ( 1 == iCase ) firstZone = part + 6; // firstZone = 2nd X-Layer in T1
    //Case 3
    if ( 2 == iCase ) lastZone  = 16 + part; //lastZone = 1st X-layer in T3
    debug()<<"\t Loading Case Hit in first and last Zone"<<endmsg;
    PrHitZone* fZone = m_hitManager->zone( firstZone );
    PrHitZone* lZone = m_hitManager->zone( lastZone );
    debug()<<"\t Hits in last and first Zone Loaded"<<endmsg;
    //load hits in first zone and last one
    PrHits& fHits = fZone->hits();
    PrHits& lHits = lZone->hits();
    double zFirst     = fZone->z(0.); //First  Zone Z value
    double zLast      = lZone->z(0.); //First  Zone Z value
    double DeltaZ = zLast-zFirst;     //Delta First-Last
    
    
    std::vector< PrHits::iterator > iterators;  //iterator for in-between layers
    iterators.reserve(24);
    debug()<<"\t\t Case \t"<<iCase
           <<"\t\t First Zone Index =\t"<<firstZone
           <<"\t\t Last Zone Index =\t"<<lastZone
           <<endmsg;
    
    std::vector<PrHitZone*> xZones; 
    xZones.reserve(12);
    for ( int kk = firstZone+2; lastZone > kk ; kk += 2 ) {
      if ( m_hitManager->zone( kk )->isX() ){
        xZones.push_back( m_hitManager->zone(kk) ); //pushback in xZones all the xZones hits
        debug()<<"\t\t\t x Zones Selected"<<kk<<endmsg;
      }   
    }
    PrHits::iterator itLBeg = lHits.begin();
    
    debug()<<"\t\t Hits in the InBetween Zones Loaded"<<endmsg;
    debug()<<"\t\t Will Loop over Hits in first Zone"<<endmsg;
    for ( PrHits::iterator itF = fHits.begin(); fHits.end() != itF; ++itF ) {  
      if ( 0 != iCase && (*itF)->isUsed() ) continue;
      //define search window as a function of the x in the first layer
      double tx_inf =  (*itF)->x()/zFirst; 
      double xProjeInf = tx_inf*zLast;
      double minXl =  xProjeInf + tx_inf*m_alphaCorrection  -m_TolHp;
      double maxXl =  xProjeInf + tx_inf*m_alphaCorrection  +m_TolHp;
      debug()<<"\t\t\t Min X last \t "<<minXl<<"\n\t\t\t Max X last \t "<<maxXl<<endmsg;
      if(m_doPlots)
      {
        plot2D(minXl,tx_inf,"L0Selection/minXl_high_P_vs_tinf","minXl vs t_{x}^{inf};minXl;t_{x}^{inf}",-4000.,4000.,-0.5,0.5,200,200);
        plot2D(minXl,tx_inf,"L0Selection/maxXl_high_P_vs_tinf","maxXl vs t_{x}^{inf};maxXl;t_{x}^{inf}",-4000.,4000.,-0.5,0.5,200,200);
        plot2D(minXl,(*itF)->x(),"L0Selection/minXl_highP_vs_X_first" ,"minXl vs X first (high P)",-4000.,4000.,-3000,3000.,200,200);
        plot2D(maxXl,(*itF)->x(),"L0Selection/minXl_high_P_vs_X_first" ,"minXl vs X first (high P)",-4000.,4000.,-3000,3000.,200,200); 
      }
      
      if(minXl>maxXl) debug()<<"\t\t\t You are stupid"<<endmsg;
      itLBeg = std::lower_bound( lHits.begin(), lHits.end(), minXl, lowerBoundX() ); 
      PrHits::iterator itL = itLBeg;
      debug()<<"\t\t\t WIll Loop over Last Layer"<<endmsg;
      while ( itL != lHits.end() && (*itL)->x() < maxXl ) 
      {
        if( (*itL)->x()<minXl || (*itL)->x()>maxXl ){ 
          break;
          debug()<<"\t\t\t\t Not Picking Hits in Last layer in the good search Window"<<endmsg;
        }
        
        if ( 0 != iCase && (*itL)->isUsed()) {
          ++itL;
          continue; 
        }
        double  tx_pickedcombination = ((*itL)->x()-(*itF)->x())/DeltaZ; 
        PrHits parabolaSeedHits;
        parabolaSeedHits.clear();
        parabolaSeedHits.reserve(8);
        
        double x0 = (*itF)->x()-tx_pickedcombination*zFirst;
        double CorrX0 = m_x0Corr*x0;
        double x0new = x0*(1.+m_x0Corr);
        //Loop over Zones in T2
        unsigned int counter = 0;
        bool skipZone = true;
        if( iCase !=0 ) skipZone = false;
        for ( std::vector<PrHitZone*>::iterator itZ = xZones.begin(); xZones.end() != itZ; ++itZ )
        {
          ++counter;
          if(skipZone) {
            skipZone = false;
            continue;
          }
          if( iCase == 0 ){
            if(counter>3) break;
          }
          else{
            if(counter>2) break;
          }
          double xProjected = x0 + (*itZ)->z(0.)*tx_pickedcombination;
          double xProjectedCorrected = xProjected+CorrX0;          
          double xMax =  xProjectedCorrected + m_m1*(x0+m_x0Offset)+m_yOff1;
          double xMin =  xProjectedCorrected + m_m2*(x0+m_x0Offset)+m_yOff2;
          // if(m_doPlots)
          // {
          //   //Add here Plots xMax,xMin vs tx_{picked}  X0<0
          // }
          
          //debug()<<"\t\t\t\t\t Looking for Hits in In -between layer"<<endmsg;
          
          if( x0>0. )
          {
            xMax =  xProjectedCorrected  + m_m1*(x0-m_x0Offset)-m_yOff2;
            xMin  =  xProjectedCorrected  + m_m2*(x0-m_x0Offset)-m_yOff1;
          }
          if(m_doPlots)
          {
            plot2D(x0,tx_inf,"L1Selection/x0vsTx_inf","x_{0} Vs t_{x}^{inf} ;x_{0};t_{x}^{inf}" , -3000. , 3000., -0.2,0.2,300,200);
          }
          
          if( xMax<xMin) debug()<<"\t\t\t\t\t Wrong xMax/xMin"<<endmsg;
          PrHits::iterator itH = std::lower_bound((*itZ)->hits().begin(), (*itZ)->hits().end(), xMin, lowerBoundX()  );
          for ( ; (*itZ)->hits().end() != itH; ++itH ) 
          {
            if ( (*itH)->x() < xMin ) break; //or break? in principle we have the lower_bound before
            if ( (*itH)->x() > xMax ) break;
            debug()<<"\t\t\t\t\t\t Filling Parabola Seed Hits"<<endmsg;
            parabolaSeedHits.push_back(*itH);
          }
          // if (parabolaSeedHits.size() > 0) debug()<<"ParabolaSeedHits Size\t"<<parabolaSeedHits.size()<<endmsg;
           //Look for another Hit in last layer
          //end loop to pick up Hits in the 2 inner Layer (was only
          //1 inner Layer!!!!)
        }
        if (parabolaSeedHits.size()==0){++itL; continue;}//if we don't fine any parabola Seed Hits in the middle 2 Layers then search for another XLast Hit
        // sort the parabola seed hits wrt to distance to the linear projection
        // merged parabolaSeedHits T2-1 & T2-2
        //=======================================================
        // We have 1 Hit in 1st 1 Hit in last and a
        // vector of Hits for in-between
        //=======================================================
        std::vector<PrHits> xHitsLists; //vector of list of Hits
        xHitsLists.clear();
        
        //=======================================================
        //Sort the ParabolaSeedHits for in-between layers in increasing distance from the Projected Corrected position only when we have more than 1 ParabolaSeedHit
        //=======================================================
        if(parabolaSeedHits.size()>1)
        {
          if(m_doPlots)
          {
            plot(parabolaSeedHits.size(),"L1Selection/ParabolaSeedHits_Size","ParabolaSeedHits Size;Size;Counts",.5,20.5,20);
          }
          //Principle of the Lambda funtion, Hits sorted wrt distance from linear Projection 1st-3rd layer
          std::stable_sort( parabolaSeedHits.begin(),parabolaSeedHits.end(),
                            [x0new,tx_pickedcombination](const PrHit* lhs, const PrHit* rhs)
                            ->bool{return std::fabs(lhs->x() - (x0new + lhs->z()*tx_pickedcombination)) < std::fabs(rhs->x() - (x0new + rhs->z(0)*tx_pickedcombination));} );
        }
        debug()<<"The Lambda Function Sorting end"<<endmsg;
        
        unsigned int maxParabolaSeedHits = m_maxParabolaSeedHits;
        if(parabolaSeedHits.size()<m_maxParabolaSeedHits)
        {
          maxParabolaSeedHits = parabolaSeedHits.size();
        }
        for (unsigned int i = 0; i<maxParabolaSeedHits;++i) //build a parabola for each 3 hit combination
        {
          if (maxParabolaSeedHits==0) break; //maybe a repetition
          double a = 0;
          double b = 0;
          double c = 0; 
          PrHits xHits;
          xHits.clear();
          if (m_useCubic)
          {
            solveParabola2(*itF,parabolaSeedHits[i],*itL,a,b,c); //Extrapolation with dRatio
          }else solveParabola(*itF,parabolaSeedHits[i],*itL,a,b,c); //Extrapolation without dRatio
          
          debug()<<"Parabola Par"
                 <<"\n a \t"<<a
                 <<"\n b \t"<<b
                 <<"\n c \t"<<c<<endmsg;
          
          //===================================================
          // Look in all the other layers except the
          // 1st/last/zone except the parabolaSeedHit
          //===================================================
          //Loop on all the xZones
          for ( std::vector<PrHitZone*>::iterator itZ = xZones.begin(); xZones.end() != itZ; ++itZ ) {
            //skip xZone if planeCode is equal to the Last/First and parabolaSeedHits
            if( ((int)(*itZ)->planeCode() == (int)(*itF)->planeCode()) || 
                ((int)(*itZ)->planeCode()==(int)(*itL)->planeCode() ))
              continue; 
            if((int)(*itZ)->planeCode() == (int)parabolaSeedHits[i]->planeCode())continue;
            debug()<<"Selecting ParSeedHits"<<endmsg;
            
            double dz   = (*itZ)->z() - m_geoTool->zReference();
            double xAtZ = a * dz * dz + b * dz + c; //Parabolic computation
            if(m_useCubic)
              xAtZ= a * dz * dz * (1. + m_dRatio * dz) + b * dz + c; //with Cubic Correction
            double xMaxAtZ = xAtZ + m_maxDeltaPar;   //std::fabs(tx_pickedcombination)+0.5;
            double xMinAtZ = xAtZ - m_maxDeltaPar;     //std::fabs(tx_pickedcombination)-0.5;
            // if(m_doPlots)
            // {
            //   //Add Plot xMaxAtZ - xMinAtZ
            // }
            
            //Best projected Hit
            PrHit* bestProj = nullptr;
            double  bestDist = m_maxDeltaPar; //2.0 mm at the moment (Larger)? tighter? (see offline Seeding)
            //Loop in all the Hits in a given X Zones( here still in the loop in xZones
            //Sort Hits from xMinAtZ to greater X
            PrHits::iterator itH = std::lower_bound( (*itZ)->hits().begin() ,(*itZ)->hits().end(),xMinAtZ,lowerBoundX());
            for (; (*itZ)->hits().end() != itH; ++itH ) 
            {
              if(xMinAtZ > xMaxAtZ)
              {
                debug()<<"Bad Settings!!!!!!"<<endmsg;
              }
              
              if ( (*itH)->x() < xMinAtZ ) break;
              if ( (*itH)->x() > xMaxAtZ ) break;
              if(std::fabs((*itH)->x() - xAtZ ) < bestDist) 
              {
                bestDist =std::fabs((*itH)->x() - xAtZ );
                debug()<<"I found an Hit from projection"<<endmsg;
                bestProj = *itH;
              }
            }
            //End Loop on the Hits in the remaining layers to pick up Hits in the closer to the parabolic projection 
            if (bestProj != nullptr)
            {
              xHits.push_back(bestProj);
            }
            //in principle i have not
            //pushing back the hit
            //which was used to
            //seed the parabola
            //at this step Hits has the inner layer parabola
            //+ extractrapolated hit &
            // & best hit wrt distance to extrapolated parabola
          }//end loop xZones
          
          debug()<<"End Loop in between zones to pick up Projection of parabola"<<endmsg;
          //    xHits.push_back( parabolaSeedHits[i]);
          // Add to the xHits Vector the remaining 3 Hits not considered
          xHits.push_back( parabolaSeedHits[i]);
          xHits.push_back( *itF);
          xHits.push_back( *itL);
          
          if(xHits.size()>6)
          {
            
            always()<<"Smething goes wrong!!!! in the creation of the xHits list"<<endmsg;
            always()<<"xHits is bigger than 6 : ERROR"<<endmsg;
          }
          
          //end parabola Seed Hits loop in other Layers
          //Still in the L0 loop (selection last layer)
          //at this step we have 1 Hit in 1st Layer
          //at this step we have 1 Hit in last Layer
          //at this step we have 1 Hit in Middle Layer
          //at this step we have Hit in remaining X layers at the
          //best distance to extrapolated parabola All of them are
          //inside xHits i want to have at least min_HitXSize
          
          if( xHits.size() <  m_minXPlanes)  continue; //Require at least 5 X Hits
          std::stable_sort(xHits.begin(), xHits.end(), compX());
          bool isEqual = false;
          //Remove xHits in the xHitsLists whicha are basically the same
          for (PrHits hits : xHitsLists){
            if(hits == xHits){
              isEqual = true;
              break;
            }
          }
          if(!isEqual)
          { 
            debug()<<"Pushing Back xHits List"<<endmsg;
            xHitsLists.push_back( xHits);
          }
        }//End loop parabolaSeedHits
        debug()<<"End Loop For pick up Parabola Hits and build the xHitsLists"<<endmsg;
        //End loop Parabola Seed Hits
        //-------- Remove Duplicates
        debug()<<"xHitsLists size before removing duplicates: "<<xHitsLists.size()<<endmsg;
        if( xHitsLists.size() == 0) 
        {
          ++itL;
          continue;
        }
        if( xHitsLists.size() > 2){
          //---Remove Duplicates in the HitsList
          std::stable_sort( xHitsLists.begin(), xHitsLists.end() );
          xHitsLists.erase( std::unique(xHitsLists.begin(), xHitsLists.end()), xHitsLists.end());
        }
        debug()<<"xHitsLists size after removing duplicates: "<<xHitsLists.size()<<endmsg;
        //Now let's fit the track
        //Tuple tupleTrackPostReFit = nTuple("Seeding/AfterFitAndReFit/Track","Events");
        for (PrHits xHits : xHitsLists)
        { 
          debug()<<"Fit Track"<<endmsg;
          //PrSeedTrack(zone,zRef,Hits) constructor
          PrSeedTrack temp_track( part , m_geoTool->zReference() , xHits); //Create the track
          temp_track.setdRatio(0.);
          temp_track.setx0Beg(x0);
          temp_track.setTxInf(tx_inf);
          if(m_useCubic)
            temp_track.setdRatio(m_dRatio);
          //-----------------------------------------------------
          //----------------O-_The Fit_-O------------------
          //-----------------------------------------------------
          int nIter = 0;
          bool OK = fitXProjection(temp_track , 0); //Fit the track projection on the XZ Plane
          if(!OK) debug()<<"Fit Failed"<<endmsg;
          while(!OK)
          {
            nIter++;
            debug()<<"The Fit failed"<<endmsg;
            if(temp_track.hits().size() <= m_minXPlanes) break;
            debug()<<"Removing Hits and Refit"<<endmsg;
            OK = removeWorstAndRefitX( temp_track, nIter );
            debug()<<"Remove worse and refit"<<endmsg;
            debug()<<"***"<<endmsg;
          }
          bool Selected = false;
          if(OK)
            setChi2X(temp_track); 
         
          if(m_doPlots)
          {
            
            Tuple tupleTrackPostReFit = nTuple("Seeding/AfterFitAndReFit/Track","Events");
            tupleTrackPostReFit->column("Selected",Selected);
            tupleTrackPostReFit->column("Ok",OK);
            //tupleTrackPostReFit->column("",track.
            tupleTrackPostReFit->column("txInf",temp_track.txinf());
            tupleTrackPostReFit->column("Chi2",temp_track.chi2());
            tupleTrackPostReFit->column("Chi2DoF",temp_track.chi2PerDoF());
            tupleTrackPostReFit->column("X0",temp_track.X0());
            tupleTrackPostReFit->column("x0_beg",temp_track.x0Beg());
            tupleTrackPostReFit->column("Assoc",temp_track.assocX());
            tupleTrackPostReFit->column("maxDist",temp_track.MaxDist());
            tupleTrackPostReFit->column("avgDist",temp_track.avgDist());
            tupleTrackPostReFit->column("AbsAvgDist",temp_track.AbsAvgDist());
            tupleTrackPostReFit->column("ax",temp_track.ax());
            tupleTrackPostReFit->column("bx",temp_track.bx());
            tupleTrackPostReFit->column("cx",temp_track.cx());
            tupleTrackPostReFit->column("NDOF",temp_track.nDoF());
            tupleTrackPostReFit->write();
          }
          double maxChi2 =  12.; //??(6*3dof = 18 on chi2)??tighter?
          //Flag Conditions
          if(m_Cheat && temp_track.assocX()){
            m_xCandidates_highP.push_back(temp_track);
          }
          if( OK && 
              temp_track.hits().size() >= m_minXPlanes && !m_Cheat )
          {
            
            if((temp_track.hits().size()==6 &&  m_FlagHits &&      //Size ==6
                std::fabs(temp_track.cx())<6e-6 &&                      //P Cut
                std::fabs(temp_track.AbsAvgDist()<0.125))             //Quality Cut
               ||(temp_track.hits().size()==5 && std::fabs(temp_track.cx())<6e-6 && //Size ==5
                  std::fabs(temp_track.AbsAvgDist()<0.250))
               ||(temp_track.hits().size()==4 &&                                            //Size ==4
                  std::fabs(temp_track.cx())<6e-6&&
                  std::fabs(temp_track.AbsAvgDist()<0.3)))
            {
              for(PrHits::iterator itH = temp_track.hits().begin();temp_track.hits().end()!=itH;++itH){
                (*itH)->setUsed(true);
              }
            }
            
            debug()<<"Will Load track in container"<<endmsg;
            m_xCandidates_highP.push_back(temp_track); //The X Candidate is created
          } 
        }//end Loop xHist:xHitsLists
        ++itL;
      }//end loop Last Zone given a firsZone selected
    }//end Loop First Zone
  } //end Case
  if(m_doPlots)
  {    plot(m_xCandidates_highP.size(),"XCandidates/Size","Size vector XCandidates",0.,10000.,400);
  }
  
  std::stable_sort(m_xCandidates_highP.begin(),m_xCandidates_highP.end(),PrSeedTrack::GreaterBySize());
  //====================================================================
  // Remove clones, i.e. share more than 2 hits (why not 3?)
  //====================================================================
  debug()<<"Removing Clones in X Step"<<endmsg;
  for( PrSeedTracks::iterator itT1 = m_xCandidates_highP.begin() ; m_xCandidates_highP.end()!=itT1;++itT1)
  {
    if( !(*itT1).valid())continue; //should be always true because validity is set at the construction
    if( (*itT1).hits().size() !=6) //size == 5
    { 
      //only case when Size =5 are we
      //sure of it? Ghost rate? 
      int nUsed = 0;
      for (PrHits::iterator itH = (*itT1).hits().begin() ; (*itT1).hits().end()!=itH ; ++itH)
      {
        if((*itH)->isUsed()) ++nUsed; 
        //count the number of
        //hits which end up in a
        //track of size 5 or 4 that
        //are also used by tracks with
        //6 hits
      }
      if(  nUsed > m_nUsed)
      {
        if(m_removeFlagged)
          (*itT1).setValid(false);
        continue;
      }
    }
    //Look to all the other tracks inside this one
    for(PrSeedTracks::iterator itT2 = itT1+1 ; m_xCandidates_highP.end() != itT2 ; ++itT2)
    {
      if( !(*itT2).valid() ) continue;
      int nCommon = 0;
      PrHits::iterator itH1 = (*itT1).hits().begin(); //Hit in Track1
      PrHits::iterator itEnd1 = (*itT1).hits().end();
      PrHits::iterator itH2 = (*itT2).hits().begin();//Hits in Track2
      PrHits::iterator itEnd2 = (*itT2).hits().end();
      while( itH1 != itEnd1 && itH2 != itEnd2 )
      {
        if( (*itH1)->id() == (*itH2)->id() )
        {
          ++nCommon; //Number of Hit in Common
          ++itH1;    //Increment The Hit Pointer for track 1
          ++itH2;    //Increment The Hit Pointer for track 2
        } 
        else if( (*itH1)->id() < (*itH2)->id() )
        {
          ++itH1;
        }
        else
        {
          ++itH2;
        }
      }
      if( nCommon >= m_nCommon ) {
        if( (*itT1).hits().size() > (*itT2).hits().size() )
        {
          if(m_removeCloneX)
            (*itT2).setValid( false );
        }
        else if( (*itT1).hits().size() < (*itT2).hits().size() )
        {
          if(m_removeCloneX)
            (*itT1).setValid( false );
        }
        else if( (*itT1).chi2PerDoF() < (*itT2).chi2PerDoF() )//same size
        {//same size pick the one with best chi2
          if(m_removeCloneX)
            (*itT2).setValid( false );
        } else
        {//same size pick the one with best chi2
          if(m_removeCloneX)
            (*itT1).setValid( false );
        }
      }
    }
    if( m_xOnly && (*itT1).valid() ){debug()<<"Fillincg Container Of Tracks"<<endmsg;
      m_trackCandidates_highP.push_back( *itT1 );
    } 
  }  
}








//=========================================================================
//  Convert to LHCb tracks
//=========================================================================
void PrSeedingNew::makeLHCbTracks ( LHCb::Tracks* result ) {
  for ( PrSeedTracks::iterator itT = m_trackCandidates_highP.begin();m_trackCandidates_highP.end() != itT; ++itT ) {
    if ( !(*itT).valid() ) continue;
    debug()<<"Creating LHCb Track"<<endmsg;
    if(m_doPlots)
    {
      plot((*itT).chi2PerDoF(),"MakeLHCbTrack/Chi2PerDOF","Track Chi2/DOF",-0.5,30.,300);                                                             
      plot((*itT).nDoF(), "MakeLHCbTrack/nDoF", "Track_nDoF",-0.5,40. ,300);
    }
    
    //plot(fZone->hits().size(), "NumberOfHitsInFirstZone","NumberOfHitsInFirstZone", 0., 600., 100);        
    //plot(lZone->hits().size(), "NumberOfHitsInLastZone","NumberOfHitsInLastZone", 0., 600., 100);
    
    
    LHCb::Track* tmp = new LHCb::Track;
    tmp->setType( LHCb::Track::Ttrack );
    tmp->setHistory( LHCb::Track::PrSeeding );
    double qOverP = m_geoTool->qOverP( *itT );
    LHCb::State tState;
    double z = StateParameters::ZEndT;
    tState.setLocation( LHCb::State::AtT );
    tState.setState( (*itT).x( z ), (*itT).y( z ), z, (*itT).xSlope( z ), (*itT).ySlope( ), qOverP );
    
    //== overestimated covariance matrix, as input to the Kalman fit
    
    tState.setCovariance( m_geoTool->covariance( qOverP ) );
    tmp->addToStates( tState );
    
    //== LHCb ids.
    
    tmp->setPatRecStatus( LHCb::Track::PatRecIDs );
    for ( PrHits::iterator itH = (*itT).hits().begin(); (*itT).hits().end() != itH; ++itH ) {
      tmp->addToLhcbIDs( (*itH)->id() );
    }
    tmp->setChi2PerDoF( (*itT).chi2PerDoF() );
    tmp->setNDoF(       (*itT).nDoF() );
    result->insert( tmp );
  }
}


 



void PrSeedingNew::solveParabola(const PrHit* hit1, const PrHit* hit2, const PrHit* hit3, double& a, double& b, double& c){
  const double x3 = hit3->x();
  
  //New Parabola Method (be adapted to new parametrisation)
  const double z1_PB = hit1->z() - hit3->z();
  const double z2_PB = hit2->z() - hit3->z();
  const double x1_PB = hit1->x() - hit3->x();
  const double x2_PB = hit2->x() - hit3->x();
  
  const double det_PB = z1_PB*z2_PB*(z1_PB-z2_PB);
  
  if( std::fabs(det_PB) < 1e-8 ){
    a = 0.0;
    b = 0.0;
    c = 0.0;
    return;
        
  }
    
  a=0; b=0;c=0;
  a = (z2_PB*x1_PB-z1_PB*x2_PB)/det_PB;
  b = (-z2_PB*z2_PB*x1_PB+z1_PB*z1_PB*x2_PB)/det_PB;
  const double z3_PB = hit3->z() - m_geoTool->zReference();
  c = x3 + a * z3_PB * z3_PB - b *z3_PB;
  b -= 2 * a * z3_PB;
}

void PrSeedingNew::solveParabola2(const PrHit* hit1,const PrHit* hit2,const PrHit* hit3,double& a1, double& b1,double& c1){
  const double z1 = hit1->z() - m_geoTool->zReference();
  const double z2 = hit2->z() - m_geoTool->zReference();
  const double z3 = hit3->z() - m_geoTool->zReference();
  const double x1 = hit1->x();
  const double x2 = hit2->x();
  const double x3 = hit3->x();
  //const double e = m_dRatio;
  const double corrZ1 = 1+m_dRatio*z1;
  const double corrZ2 = 1+m_dRatio*z2;
  const double corrZ3 = 1+m_dRatio*z3;
  const double det = (z1*z1)*corrZ1*z2 + z1*(z3*z3)*corrZ3 + (z2*z2)*corrZ2*z3 - z2*(z3*z3)*corrZ3 - z1*(z2*z2)*corrZ2 - z3*(z1*z1)*corrZ1;
  if( std::fabs(det) < 1e-8 )
  {
    a1 = 0.0;
    b1 = 0.0;
    c1 = 0.0;
    return;
  }
  const double det1 = (x1)*z2 + z1*(x3) + (x2)*z3 - z2*(x3) - z1*(x2) - z3*(x1);
  const double det2 = (z1*z1)*corrZ1*x2 + x1*(z3*z3)*corrZ3 + (z2*z2)*corrZ2*x3 - x2*(z3*z3)*corrZ3 - x1*(z2*z2)*corrZ2 - x3*(z1*z1)*corrZ1;
  const double det3 = (z1*z1)*corrZ1*z2*x3 + z1*(z3*z3)*corrZ3*x2 + (z2*z2)*corrZ2*z3*x1 - z2*(z3*z3)*corrZ3*x1 - z1*(z2*z2)*corrZ2*x3 - z3*(z1*z1)*corrZ1*x2;
  a1 = det1/det;
  b1 = det2/det;
  c1 = det3/det;

}


bool PrSeedingNew::fitXProjection(PrSeedTrack& track , int Refit){
  using namespace Tuples;
   //x_{i} = x(z_{i}) = a + b(z_{i}-z_{ref})+c*(z_{i}-z_{ref})^{2}
  // LinParFit<double> fit(3);
  debug()<<"Fitting"<<endmsg;
  if(Refit==0 && track.hits().size()< m_minXPlanes) return false;
  if(track.hits().size()<m_minXPlanes) return false;
  debug()<<"Track has 5 hits and XOnly is False"<<endmsg;
  double mat[6]; 
  double rhs[3];
  for(int loop = 0;3>loop;++loop)
  {
    std::fill(mat,mat+6,0.); 
    std::fill(rhs,rhs+3,0.);
    for( PrHits::iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH ){
      const double w = (*itH)->w();//squared
      const double dz= ((*itH)->z() - m_geoTool->zReference());
      double deta = 0;
      if(m_useCubic)
      {
        deta = dz*dz*(1+m_dRatio*dz);
      }else deta = dz*dz;
      double dist = track.distance( *itH );
      debug()<<"Loop \t"<<loop<<"\n Distance From Hit \t"<<dist<<endmsg;
      mat[0]+= w;
      mat[1]+= w * dz;
      mat[2]+= w * dz * dz;
      mat[3]+= w * deta;
      mat[4]+= w * dz * deta;
      mat[5]+= w * deta * deta;
      //right hand side
      rhs[0]+= w * dist;
      rhs[1]+= w * dist * dz;
      rhs[2]+= w * dist * deta;
    }
    
    ROOT::Math::CholeskyDecomp<double,3> decomp(mat);
    if(!decomp)
    {
      return false;
    }
    //Solve linear system
    decomp.Solve(rhs);
    debug()<<"Loop \t"<<loop<<"\n a = \t"<<rhs[0]<<"\n b = \t"<<rhs[1]<<"\n c = \t"<<rhs[2]<<endmsg;
    // protect against unreasonable track parameter corrections
    // (check that out)
    // if (std::abs(rhs[0]) > 1e4 || std::abs(rhs[1]) > 5. ||
    //     std::abs(rhs[2]) > 1e-3 ) return false;
    //Small corrections
    track.updateParameters(rhs[0],rhs[1],rhs[2],0.,0.);
    if (loop >0 && std::abs(rhs[0]) < 5e-3 && std::abs(rhs[1]) < 5e-6 &&
        std::abs(rhs[2]) < 5e-9) {
      break;
    }
  }
  //Fit is done
  
  if(m_doAssociation)
  {
    
    bool AssociateX = Assoc(track);
  }
  
  double chi2_track = 0.;
  double maxChi2 = 0.;
  double maxDistance = 0.;
  double absdistanceSum = 0.;
  double distanceSum =0.;
  unsigned int NHits = (unsigned int)track.hits().size();
  for ( PrHits::iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH )  //Loop over all hits in PrSeedTrack
  {
    double distance = track.distance( *itH );
    absdistanceSum += std::fabs(distance)/((double)NHits);
    distanceSum += distance/((double)NHits);
    if(std::fabs(distance)>maxDistance){
      maxDistance = fabs(distance);
    }
    double chi2_onHit = track.chi2( *itH );
    chi2_track += track.chi2( *itH );
    if ( chi2_onHit > maxChi2 ){
      maxChi2 = chi2_onHit;
    }
    if(m_doPlots && m_doAssociation)
    {
      
      Tuple tupleHit = nTuple("Seeding/XFit/Fit/Hits","Events");
      tupleHit->column("Refit",Refit);
      tupleHit->column("Distance",distance);
      tupleHit->column("Chi2OnHit",chi2_onHit);
      tupleHit->column("Assoc",track.assocX());
      tupleHit->write();
    }
    
  }//End Loop on Hits 
  double chi2PerDoF = chi2_track/(double)track.hits().size();
  double X0 = track.ax() - track.bx()*m_geoTool->zReference()+track.cx()*m_ConstC; //Back Projection
  track.setX0(X0);
  track.setMaxChi2(maxChi2);
  track.setAbsAvgDist(absdistanceSum);
  track.setAvgDist(distanceSum);
  track.setMaxDistance(maxDistance);
  track.setRefitX(Refit);
  if( Refit ==0)
    track.setChi2FirstFit(chi2_track);
  if(Refit ==1)
  {track.setChi2ReFit(chi2_track);
    track.setdeltaRefitXChi2(track.Chi2FirstFit()-chi2_track);
  }
  if(Refit==2)
  {
    track.setChi2ReReFit(chi2_track);
    track.setdeltaRefitRefitXChi2(track.Chi2ReFit()-chi2_track);
  }
  if(m_doPlots && m_doAssociation)
  {
    Tuple tupleTraack = nTuple("Seeding/XFit/FirstFit/Track","Events");
    tupleTrack->column("step",Refit);
    tupleTrack->column("Chi2Removed",track.Chi2Removed());
    tupleTrack->column("MaxChi2OnHit",maxChi2);
    tupleTrack->column("Assoc",track.assocX());
    tupleTrack->column("TxInf",track.txinf());
    tupleTrack->column("Chi2",chi2_track);
    tupleTrack->column("Chi2DoF",chi2PerDoF);
    tupleTrack->column("NDOF",NHits);
    tupleTrack->column("MaxDistance",maxDistance);
    tupleTrack->column("NAssoc",track.getNAssocX()); 
    tupleTrack->column("X0",X0);
    tupleTrack->column("x0Beg",track.x0Beg());
    tupleTrack->column("ax",track.ax());
    tupleTrack->column("bx",track.bx());
    tupleTrack->column("cx",track.cx());
    tupleTrack->column("AbsAvgDistance",absdistanceSum);
    tupleTrack->column("Chi2ReFit",track.Chi2ReFit());
    tupleTrack->column("Chi2ReReFit",track.Chi2ReReFit());
    tupleTrack->column("AvgDistance",distanceSum);
    tupleTrack->column("N1",track.getN1());
    tupleTrack->column("N2",track.getN2());
    tupleTrack->column("N3",track.getN3());
    tupleTrack->column("NHits",track.nHits());
    tupleTrack->column("Purity",track.Purity());
    tupleTrack->column("Efficiency",track.Efficiency());
    tupleTrack->column("P_Associated",track.PAssoc());
    tupleTrack->write();
    
  
  
    debug()<<"Track is Associated"<<track.assocX();
  }
  
  // if(!Refit){
  //   track.setChi2FirstFit(chi2_track);
  // }else{
  //   track.setChi2ReFit(chi2_track);
  //   track.setdeltaRefitXChi2(track.Chi2FirstFit()-chi2_track);
  // }
  //Conditions for First Fit (keep Good Tracks) reject one bad & to be refitted
  //Cheating
  if(m_Cheat && m_doAssociation){ //Meaning that 100 %Hit Purity
    debug()<<"Cheat and truth Matching In Refit/t"<<Refit<<endmsg;
    if(AssociateX) return true;
    return false;
  }
  //Global Cut: Momentum and quality of the Fit
  if(std::fabs(track.cx())>0.017e-3 ||
     track.MaxDist()>1.4 ||
     track.MaxChi2()>110 || 
     track.AbsAvgDist()>0.82 ||
     track.Chi2FirstFit()/(double)track.hits().size() > 42.5 ||
     std::fabs(track.X0())>2700){
    return false;
  }
  //6 Hits Combination
  if(track.hits().size()==6 && Refit==0)
  {//From Truth Matching studies
    if(std::fabs(track.cx())<6e-6 &&
       std::fabs(track.AbsAvgDist()<0.125)&&
       std::fabs(X0)<1200 &&track.Chi2FirstFit()/6.<0.65)
      //track.Chi2FirstFit()/6. <0.8 &&//also 0.4?
      //std::fabs(X0)<1200 &&
      //track.maxDist()<0.4)
      return true;
    return false; //remove the hit
  }
  if(track.hits().size()==5 && (Refit ==0 || (Refit ==1 && track.deltaRefitXChi2()/track.chi2Removed()>1.34)))
  {
    if(std::fabs(track.cx())<6e-06&&
       std::fabs(track.AbsAvgDist()<0.250))
      return true;
    return false;
  }
  if(track.hits().size()==4 && (Refit ==0 || Refit==1 || Refit ==2)) //to be done
  {
    if(std::fabs(track.cx())<6e-06&&
       std::fabs(track.AbsAvgDist()<0.3)){  return true;
    }
    return false;
  }
  
  
  if(Refit==1)
  {
    if (track.deltaRefitXChi2()/track.Chi2Removed() > 1.3 || (5.5 >track.MaxChi2()))
      return true;
    return false
  }
  
  
  
  //Refitting Conditions
  // if(ReFit && track.hits().size()==4)
  // {
  //   if(std::fabs(cx)<6e-06&&
  //      std::fabs(track.AbsAvgDist()<0.3))
  //     return true;
  //   return false;
  // }
  
  // if(!Refit){
  //   if(std::fabs(maxDistance) < 1.75 
  //      && maxChi2 < 25. 
  //      && std::fabs(track.cx())< 13.0e-6 
  //      && std::fabs(X0)<1500.)
  //   {
  //     if(track.hits().size()==6 && 
  //        std::fabs(maxDistance) <0.250 && 
  //        maxChi2<2.5){
  //       return true; 
  //     }else{return false;}
  //     debug()<<"Fit Done"<<endmsg; 
  //     return true;
  //   }
  // }
  //   if(std::fabs(maxDistance) < 1.2 && maxChi2 < 15. && std::fabs(X0)<800.)
  //   {
  //     return true;
  //   }
  // }
  return false;
}


//=========================================================================
//  Remove the worst hit and refit.
//=========================================================================
bool PrSeedingNew::removeWorstAndRefitX ( PrSeedTrack& track,int step ) 
{
  using namespace Tuples;
  
  if(track.hits().size()>m_minXPlanes)
  {
    debug()<<"Removing Worst and Refitting"<<endmsg;
    double maxChi2 = 0.;
    PrHits::iterator worst = track.hits().begin(); 
    for ( PrHits::iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH ) 
    {
      double chi2 = track.chi2( *itH );
      if ( chi2 > maxChi2 ) {
        maxChi2 = chi2;
        worst = itH;
      }
    }
    track.setChi2Removed(maxChi2);
    track.hits().erase( worst );
    int refit = step;
    bool Ok =  fitXProjection(track , refit);
    // if(step==1)
    // {
    //   Tuple tupleTrackPostReFit = nTuple("Seeding/FirstReFit/Track","Events");
    //   tupleTrackPostReFit->column("OK",Ok);
    //   tupleTrackPostReFit->column("Chi2RemovedHit",maxChi2);
    //   tupleTrackPostReFit->column("TxInf",track.txinf());
    //   tupleTrackPostReFit->column("Chi2",track.chi2());
    //   tupleTrackPostReFit->column("Chi2DoF",track.chi2PerDoF());
    //   tupleTrackPostReFit->column("X0",track.X0());
    //   tupleTrackPostReFit->column("x0_beg",track.x0Beg());
    //   tupleTrackPostReFit->column("Assoc",track.assocX());
    //   tupleTrackPostReFit->column("maxDist",track.MaxDist());
    //   tupleTrackPostReFit->column("avgDist",track.avgDist());
    //   tupleTrackPostReFit->column("AbsAvgDist",track.AbsAvgDist());
    //   tupleTrackPostReFit->column("ax",track.ax());
    //   tupleTrackPostReFit->column("bx",track.bx());
    //   tupleTrackPostReFit->column("cx",track.cx());
    //   tupleTrackPostReFit->column("NDOF",track.nDoF());
    //   tupleTrackPostReFit->column("step",step);
    // }
    // if(step==2)
    // {
    //   Tuple tupleTrackReReFit = nTuple("Seeding/ReReFit/Track","Events");
    //   tupleTrackReReFit->column("OK",Ok);
    //   tupleTrackReReFit->column("TxInf",track.txinf());
    //   tupleTrackReReFit->column("Chi2",track.chi2());
    //   tupleTrackReReFit->column("Chi2DoF",track.chi2PerDoF());
    //   tupleTrackReReFit->column("X0",track.X0());
    //   tupleTrackReReFit->column("x0_beg",track.x0Beg());
    //   tupleTrackReReFit->column("Assoc",track.assocX());
    //   tupleTrackReReFit->column("maxDist",track.MaxDist());
    //   tupleTrackReReFit->column("avgDist",track.avgDist());
    //   tupleTrackReReFit->column("AbsAvgDist",track.AbsAvgDist());
    //   tupleTrackReReFit->column("ax",track.ax());
    //   tupleTrackReReFit->column("bx",track.bx());
    //   tupleTrackReReFit->column("cx",track.cx());
    //   tupleTrackReReFit->column("NDOF",track.nDoF());
    //   tupleTrackReReFit->column("step",step);
    // }
    if(!Ok) return false;
    return true;
  }
  return false;
}

//=========================================================================
//  Set the chi2 of the track
//=========================================================================
void PrSeedingNew::setChi2X ( PrSeedTrack& track ) {
  double chi2 = 0.;
  int   nDoF = -3;  // Fitted a parabola
  //bool hasStereo = false;
  for ( PrHits::iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH ) {
    if((*itH)->dxDy() !=0) debug()<<"You were picking up Stereo Layers!!!"<<endmsg;
    double d = track.distance( *itH );
    double w = (*itH)->w();
    chi2 += w * d * d;
    nDoF += 1;
  }
  debug()<<"Chi2 Set for track = \t"<<chi2<<endmsg;
  track.setChi2( chi2, nDoF );
}


//===============================================
//  Print the whole track
//=========================================================================
void PrSeedingNew::printTrack ( PrSeedTrack& track ) {
  for ( PrHits::iterator itH = track.hits().begin(); track.hits().end() != itH; ++itH ) {
    info() << format( "dist %7.3f dy %7.2f chi2 %7.2f ", track.distance( *itH ), track.deltaY( *itH ), track.chi2( *itH ) );
    printHit( *itH );
  }
}


//=========================================================================
//  Print the information of the selected hit
//=========================================================================
void PrSeedingNew::printHit ( const PrHit* hit, std::string title ) {
  info() << "  " << title << " "
         << format( " Plane%3d zone%2d z0 %8.2f x0 %8.2f  size%2d charge%3d coord %8.3f used%2d ",
                    hit->planeCode(), hit->zone(), hit->z(), hit->x(),
                    hit->size(), hit->charge(), hit->coord(), hit->isUsed() );
  if ( m_debugTool ) m_debugTool->printKey( info(), hit->id() );
  if ( matchKey( hit ) ) info() << " ***";
  info() << endmsg;
}

void PrSeedingNew::createMap()
{ 
  LinkedTo<LHCb::MCParticle, LHCb::FTCluster> myClusterLink ( evtSvc(), msgSvc(), LHCb::FTClusterLocation::Default );
  //Container of the MCParticles
  LHCb::MCParticles* mcParts= getIfExists<LHCb::MCParticles>(LHCb::MCParticleLocation::Default);
  MCTrackInfo trackInfo( evtSvc(), msgSvc() );

  //Loop over all the MCParticle
  for(LHCb::MCParticles::const_iterator iPart =  mcParts->begin(); iPart != mcParts->end(); ++iPart)
  { 
    //Get the MC particle
    const LHCb::MCParticle* mcPart = *iPart;
    //bool isLong = trackInfo.hasVeloAndT( mcPart);
    //bool isDown = trackInfo.hasT( mcPart) && trackInfo.hasTT(mcPart);
    bool isSeed = trackInfo.hasT( mcPart);
    //bool isUp = trackInfo.hasVelo(mcPart) && trackInfo.hasTT(mcPart);
    
    if(!isSeed && !m_UseSeed) continue; //Select only MCParticles which are Seed tracks
    //hasT --> acceptance criteria to be track 
    
    bool isElectron = std::fabs(mcPart->particleID().pid())==11;
    if(isElectron && m_NoElectrons) continue;
    bool PCut = std::fabs(mcPart->p())<= m_PCut;
    if(PCut && m_doPCut) continue; //skip track to truth match if momentum not good
    int totHits = 0;
    for(int i = 0; i < 24; ++i){
      PrHitZone* zone = m_hitManager->zone( i );
      if(!zone->isX()) continue;
      for( PrHits::iterator itH = zone->hits().begin(); zone->hits().end() != itH; ++itH){
        LHCb::MCParticle* mcPart1 = myClusterLink.first( (*itH)->id().ftID() ); //get the mcParticle associated to the Cluster LHCbID
        std::vector< LHCb::MCParticle* > parts;
        bool found = false;
        while(mcPart1 !=nullptr && mcPart1->p()>m_PCut && std::fabs(mcPart1->particleID().pid())!=11)
        {
          if(mcPart1==mcPart)
          {
            // if(mcPart1->p()>m_PCut && std::fabs(mcPart1->particleID().pid())!=11)
            found = true;
            parts.push_back(mcPart1);
          }
          mcPart1=myClusterLink.next(); //next MCParticle contributing to the LHCbID
        }
        if(found && parts.size()!=0)
        {debug()<<"Map Filled"<<endmsg;
          m_map[(*itH)->id()] = parts;} //instead of vector of Key
        //parts.clear(); //delete the vector for a single Hit association
      }//end loop Zone Hits
    }//end Loop All Parts
  }//end Loop MCParticles
}//end createMap m_map

bool PrSeedingNew::Assoc(PrSeedTrack& track)
{
  //for each MCParticle i have a Counter
  std::map<LHCb::MCParticle*, int> Counter;
  Counter.clear();
  //Number if Hits with at least 1 MCParticle associated
  int NHits =0;
  int HitWithAssoc=0;
  //Loop over track Hits
  for(PrHits::const_iterator itH = track.hits().begin();track.hits().end()!=itH;++itH)
  {
    NHits++; //Number of hits on track to associate
    //Get The vector of MCParticle Associated to the LHCBiD
    std::vector<LHCb::MCParticle*> parts = m_map[(*itH)->id()]; 
    //if no MCParticles associated then continue
    if(parts.size() == 0 )
    {
      continue;
    }
    for(std::vector<LHCb::MCParticle*>::iterator itP = parts.begin(); itP != parts.end(); ++itP)
    {
      if((*itP)==nullptr) break;
      Counter[(*itP)]++;
      debug()<<"Counter[part]\t"<<Counter[(*itP)]<<endmsg;
    }
    HitWithAssoc++;
  }//end loop on all Hits on the track
  while(!Counter.empty())
  {//Lambda function C++11
    std::vector<std::pair<LHCb::MCParticle*,int> > v(std::begin(Counter),std::end(Counter));
    std::sort(std::begin(v),std::end(v),[](const std::pair<LHCb::MCParticle*,int>& a,const std::pair<LHCb::MCParticle*,int>& b) {return a.second < b.second;});
    std::pair<LHCb::MCParticle*,int> a = v.back();
    LHCb::MCParticle* Part = a.first;
    int n_maxAssoc = a.second;
    track.setPAssoc( Part->p());
    double purity = (double)n_maxAssoc/(double)HitWithAssoc;
    double Efficiency = (double)n_maxAssoc/(double)NHits;
    if( Efficiency>m_Efficiency &&  //4 if m_nMinXPlanes == 5
        purity>m_Purity){ //70 % and at least 3 hit associated to same MCParticle!will give inf chi2
      debug()<<"# of Hits associated to the same MCParticle \t"<<n_maxAssoc<<"\n Hit with associaton\t"<<HitWithAssoc<<"\n  of Hits on track \t"<<track.hits().size()<<endmsg;
      Counter.clear();
      //Setter for Associator Plot
      track.setNAssocX(n_maxAssoc);
      track.setAssocX(true);
      return true;
    }
    Counter.clear();
    return false; 
  }
  debug()<<"Will return False due to condition_2"<<endmsg;
  Counter.clear();
  return false;
  //Want to know track properties?
  //LHCb::MCParticle mcPart* = (*found)s.first
  //double P = mcPart->p();
}
