#ifndef PRSEEDTRACK_H 
#define PRSEEDTRACK_H 1

// Include files
#include "Event/Track.h"
#include "PrKernel/PrHit.h"
#include "Event/MCParticle.h"
/** @class PrSeedTrack PrSeedTrack.h
 *  This is the working class inside the T station pattern
 *
 *  @author Olivier Callot
 *  @date   2012-03-22
 */
class PrSeedTrack {
public: 
  /// Constructor with the z reference
  PrSeedTrack( unsigned int zone, double zRef ) 
    : m_zone( zone ), m_zRef( zRef )
  {
    init();
  }; 

  /// Constructor with list of hits
  PrSeedTrack( unsigned int zone, double zRef, PrHits& hits )
    : m_zone( zone ), m_zRef( zRef ), m_hits( hits ) 
  {
    init();
  }; 

  virtual ~PrSeedTrack( ) {}; ///< Destructor

  /// Handling of hits: acceess, insertion
  PrHits& hits()                   { return m_hits; }
  const PrHits& hits()       const { return m_hits; }
  void addHit( PrHit* hit )        { m_hits.push_back( hit );}
  void addHits( PrHits& hits ) {
    m_hits.reserve( m_hits.size() + hits.size() );
    m_hits.insert( m_hits.end(), hits.begin(), hits.end() );
  }
  unsigned int zone() const { return m_zone; }
     
  /// Parameters of the trajectory in the T stations
  void setParameters( double ax, double bx, double cx, double ay, double by ) {
    m_ax = ax;
    m_bx = bx;
    m_cx = cx;
    m_ay = ay;
    m_by = by;
  }
  
  void updateParameters( double dax, double dbx, double dcx,
                         double day=0., double dby= 0.  ) {
    m_ax += dax;
    m_bx += dbx;
    m_cx += dcx;
    m_ay += day;
    m_by += dby;
  }
  double ax(){return m_ax;}
  double bx(){return m_bx;}
  double cx(){return m_cx;}
  double x( double z )         const { double dz = z-m_zRef; return m_ax + dz * ( m_bx + dz* m_cx * (1+m_dRatio*dz) ); }
  double xSlope( double z )    const { double dz = z-m_zRef; return m_bx + 2. * dz * m_cx; }
  double y( double z )         const { return m_ay + (z-m_zRef) *  m_by; }
  double ySlope( )            const { return m_by; }  

  double yOnTrack( PrHit* hit ) const { return hit->yOnTrack( m_ay -m_zRef * m_by, m_by ); }

  double distance( PrHit* hit ) const { 
    double yTra = yOnTrack( hit );
    return hit->x( yTra ) - x( hit->z(yTra) );
  }

  double chi2( PrHit* hit )     const { double d = distance( hit ); return d * d * hit->w(); }

  double deltaY( PrHit* hit )   const {
    if ( hit->isX() ) return 0.;
    return distance( hit ) / hit->dxDy();
  }

  bool valid()                  const { return m_valid; }
  void setValid( bool v )             { m_valid = v; }
  void setdRatio( double dRatio ) {m_dRatio = dRatio;}
  void setChi2( double chi2, int nDoF ) { m_chi2 = chi2; m_nDoF = nDoF; }
  double chi2()                  const { return m_chi2; }
  double chi2PerDoF()            const { return m_chi2 / m_nDoF; }
  int   nDoF()                  const { return m_nDoF; }
  void setChi2Removed(double Val)  {    m_Chi2Removed = Val; }
  double Chi2Removed() const{return  m_Chi2Removed;}
  void setDXCoord( double dxCoord )    { m_dXCoord = dxCoord; }
  double dXCoord()               const { return m_dXCoord; }
  void setMeanDy( double meanDy )      { m_meanDy = meanDy; }
  double meanDy()                 const { return m_meanDy; }
  
  //Fll Fit Association
  
  bool assoc()                       const { return m_assoc;  }
  void setAssoc(bool Ok) {m_assoc = Ok;}
  void setNAssocX(int n){ m_nAssocX = n;}
  int getNAssocX() const{  return m_nAssocX;}
  
  //Only X Step Association
  
  bool assocX()                  const { return m_assocX;  }
  void setAssocX(bool Ok1) { m_assocX = Ok1; }
  
  //Max Chi2 contribution of Hit
  
  void setMaxChi2(double val1)         { m_maxChi2 = val1;}
  double MaxChi2() const{return m_maxChi2;}
  
  //X0 backward propagation
  
  void setX0(double X0)                {m_X0=X0;}
  double X0()                   const {return m_X0;}
  
  
  //Max Distance of a single Hit in track
  
  void setMaxDistance(double maxDist)  {m_maxDistance = maxDist;}
  double MaxDist()              const {return m_maxDistance;}
  
  //Average Distance of Hits in track
  
  void setAvgDist(double avg)  {m_avgDistance = avg;}
  double avgDist()              const {return m_avgDistance;}
  
  //Average absolute distance of Hits in track
  
  void setAbsAvgDist(double val){m_AbsavgDistance = val;}
  double AbsAvgDist()           const {return m_AbsavgDistance;}
  
  //Flag to tell if track comes from a refitX
  int  RefitX()            const {return m_RefitX;}
  void setRefitX(int Refit) { m_RefitX = Refit;}
  
 
  
  //Number Of Hits
  void setNHits(int num){m_nHits = num;}
  int nHits()               const {return m_nHits;}
  

  //Delta of Chi2 due to refit
  double  deltaRefitXChi2()    const{return m_deltaRefitXChi2;}
  void setdeltaRefitXChi2(double deltaChi2){m_deltaRefitXChi2 = deltaChi2;}
  double deltaRefitRefitXChi2() const{return m_deltaRefitRefitXChi2;}
  void setdeltaRefitRefitXChi2(double Chi2){m_deltaRefitRefitXChi2 = Chi2;}
  //Chi2 value of First FitX
  void setChi2FirstFit(double chi2){m_Chi2FirstFit = chi2;}
  double Chi2FirstFit() const{return m_Chi2FirstFit;}
  //Chi2 value of First ReFitX
  void setChi2ReFit(double chi2){m_Chi2ReFit = chi2;}
  double Chi2ReFit()const{return m_Chi2ReFit;}
  //Chi2 value of Second RefitX
  void setChi2ReReFit(double chi2) {m_Chi2ReReFit = chi2;  }
  double Chi2ReReFit() const{return m_Chi2ReReFit; }
  void setPurity( double purity)  {m_Purity = purity;}
  void setEfficiency( double Efficiency){m_Efficiency = Efficiency; }
  double Purity() const  {    return m_Purity;  }
  double Efficiency() const{    return m_Efficiency;  }
  
  void setx0Beg(double x0Beg){ m_x0 =x0Beg;}
  double x0Beg() const{return m_x0;}
  double txinf() const {return m_txInf;}
  void setTxInf(double txInf){m_txInf = txInf;  }
  void setPAssoc( double P){m_P = P;}
  double PAssoc() const {return m_P;}
  // void setMCPart(LHCb::MCParticle mcPart)
  // {  m_mcPart = mcPart;}
  //LHCb::MCParticle getMCPart() const{return m_mcPart;}
  void setN1N2N3(int n1,int n2,int n3){
    m_n1 = n1;
    m_n2 = n2;
    m_n3 = n3;
  }
  int getN1() const {return m_n1;}
  int getN2() const {return m_n2;}
  int getN3() const {return m_n3;  }
  
  
  
  struct GreaterBySize {
    bool operator() (const PrSeedTrack& lhs, const PrSeedTrack rhs ) const { return lhs.hits().size() > rhs.hits().size(); }
  };


protected:

private:
  void init() {
    m_valid = true;
    m_hits.reserve( 32 );
    m_ax = 0.;
    m_bx = 0.;
    m_cx = 0.;
    m_by = 0.;
    m_ay = 0.;
    m_chi2 = 0.;
    m_nDoF = -1;
    m_Chi2Removed = -1;
    m_dXCoord = 0.;
    m_meanDy  = 0.;
    m_dRatio = 0.;
    m_assoc = false;
    m_assocX = false;
    m_Purity = 0.;
    m_Efficiency = 0.;
    m_X0 = -3000;
    m_x0 = 0.;
    m_Chi2FirstFit = -1;
    m_Chi2ReFit = -2;
    m_Chi2ReReFit  = -3;
    m_deltaRefitXChi2 =0.;
    m_deltaRefitRefitXChi2 = 0.;

    m_maxChi2 = -1.;
    m_avgDistance = -10.;
    m_AbsavgDistance = -1.;
    m_nHits = 0;
    m_RefitX = -1;
    m_nAssocX = 0 ;
    m_x0 =0.;
    m_txInf = -0.5;
    m_n1=0;
    m_n2=0;
    m_n3=0;
    m_P =0;
  }
  double m_P;
  double m_Purity;
  double m_Efficiency;
  double m_Chi2ReFit;
  double m_Chi2ReReFit;
  double m_deltaRefitXChi2;
  double m_deltaRefitRefitXChi2;
  double m_Chi2Removed;
  double m_txInf;
  unsigned int m_zone;
  double  m_zRef;
  PrHits m_hits;
  bool   m_valid;
  //LHCb::MCParticle m_mcPart;
  // double m_deltaRefitXChi2;
  // double m_deltaRefitRefitXChi2;
  int m_nHits;
  int m_RefitX;
  double m_ax;
  double m_bx;
  double m_cx;
  double m_ay;
  double m_by;
  double m_chi2;
  int   m_nDoF;
  int m_nAssocX;
  double m_dXCoord;
  double m_meanDy;
  double m_dRatio;
  double m_X0;
  double m_maxDistance;
  double m_avgDistance;
  double m_maxChi2;
  double m_AbsavgDistance;
  double m_Chi2FirstFit ;
  //  double m_Chi2ReFit;
  bool m_assoc;
  bool m_assocX;
  double m_x0;
  int m_n1;
  int m_n2;
  int m_n3;
};

typedef std::vector<PrSeedTrack> PrSeedTracks;

#endif // PRSEEDTRACK_H
