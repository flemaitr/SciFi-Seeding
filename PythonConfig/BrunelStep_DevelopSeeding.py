import os,sys

#TolTySlope = int(os.environ['TOLTYSLOPE'])
#TolTyOffset = int(os.environ['TOLTYOFFSET'])

#Number of Events to process
nEvts = 1000
#SeedingSettings New
#UseFix = True #True by Default
#XOnly= False #Only XProjection part of the Seeding
#PrCheckerSettings
Eta25Cut = True

# Activate the profiler
CallGrind= False

Plotta = True #Produce all the Efficiency plots
ConfigureManager = True
fracPos = 0.125
#managerPlots = False
FixedSize = 3


#To tune the Manager (Only Local Brunel v47r4 & Brunel v47r5)


#Seeding Versions to run
New      = True
Options = "NewSeeding_1000_3rdCases"

#######################################################################
# General imports
#######################################################################
from GaudiKernel.ProcessJobOptions import importOptions
from Gaudi.Configuration import *
from Configurables import Brunel , LHCbApp , DDDBConf


#######################################################################
# Brunel settings
#######################################################################

# Brunel Settings
Brunel().EvtMax     = nEvts
Brunel().PrintFreq  = nEvts/10
Brunel().DataType   = "Upgrade"
Brunel().WithMC     = True 
Brunel().Simulation = True
Brunel().OutputType = "NONE"
#Bsphiphi+NOW-1000ev-Extended_NewFTDet.digi
EventSelector().Input = ["DATAFILE='Bsphiphi+NOW__Digi_0to1_ADCThreshold_3_ClusterMinWidth_1_ClusterMaxWidth_4_-1000ev-Extended.digi'"]
from Configurables import TrackSys
TrackSys().TrackTypes= [ "Seeding","Matching"]#,"Downstream"]
from Configurables import RecSysConf
Brunel().RecoSequence = ["Decoding","Tr"]
# More options: "Vertex","RICH","CALO","MUON","PROTO","SUMMARY" # commented out while waiting for a fix
Brunel().Detectors = [ 'FT', 'Magnet' ]


#, 'UT' ] Load Detectors
if (Plotta):
   HistogramPersistencySvc().OutputFile = "SciFiPlots_"+Options+".root"
   
   
#######################################################################
   # File / tag settings
#######################################################################
   
from Configurables import CondDB
CondDB().Upgrade     = True

LHCbApp.DDDBtag     = "dddb-20131025"
LHCbApp().CondDBtag = "sim-20130830-vc-md100"
CondDB().AllLocalTagsByDataType = ["VP_UVP+RICH_2019+UT_UUT", "FT_StereoAngle5"]
Brunel().InputType  = "DIGI" 


#######################################################################
# Misc settings
#######################################################################

from Configurables import RecMoniConf
RecMoniConf().MoniSequence = [ ]

from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False

#######################################################################
# Define sequences and run
#######################################################################

def doIt():

   
   #------------------------------
   #Configure PrChecker
   #------------------------------
   
   from Configurables import GaudiSequencer
   from Configurables import PrChecker
   from Configurables import IdealStateCreator
   GaudiSequencer("CheckPatSeq").Members += []
   prChecker = PrChecker()
   from Configurables import IdealStateCreator

   if (Plotta):
      prChecker.WriteTTrackHistos = True
      prChecker.Eta25Cut = Eta25Cut
      prChecker.UseElectrons = False
      prChecker.TriggerNumbers = True
   GaudiSequencer("CheckPatSeq").Members +=[prChecker]
   from Configurables import MCParticle2MCHitAlg, IdealStateCreator, PrPlotFTHits
   # Define the algorithms
   FTAssoc = MCParticle2MCHitAlg( "MCP2FTMCHitAlg", 
                                  MCHitPath = "MC/FT/Hits", 
                                  OutputData = "/Event/MC/Particles2MCFTHits" )
   
   # tell the Data On Demand Service about them
   DataOnDemandSvc().AlgMap[ "/Event/Link/MC/Particles2MCFTHits" ]    = FTAssoc
   DataOnDemandSvc().NodeMap[ "/Event/Link" ]    = "DataObject"
   DataOnDemandSvc().NodeMap[ "/Event/Link/MC" ] = "DataObject"

   
   
   #---------------------------------
   #Configure the HitManager
   #---------------------------------
   if (ConfigureManager):
      from Configurables import PrFTHitManager
      manager = PrFTHitManager("PrFTHitManager")
      #   
      #manager.fracPosOffset = fracPos
   #manager.doTuple = manager
   # manager.HackSize1 = False
      manager.FixError = False
      manager.SizeFix = FixedSize

   
   #---------------------------------
   #Configure the Seeding Tracking
   #---------------------------------
   seedingSeq = GaudiSequencer("TrSeedingSeq")
   GaudiSequencer("TrBestSeq").Members = []
   GaudiSequencer("TrSeedingSeq").Members = []
   #if you do truthmatching in the pat reco
   GaudiSequencer("MCLinksUnpackSeq").Members =[]
   GaudiSequencer("RecoTrSeq").Members += [ seedingSeq ]
   from Configurables import PrSeedingNew
   seeding = PrSeedingNew()
   seeding.InputName ="" #Standalone seeding
   seeding.DecodeData = True
   seeding.XOnly = False
   seeding.MinXPlanes = 4
   seeding.NCases = 3
   seeding.PrintSettings = True
   seeding.RemoveClones = True
   seeding.FlagHits = True
   seeding.SizeToFlag = 10
   seeding.Flag_MaxChi2 = 0.5
   seeding.Flag_MaxX0 = 600.
   seeding.Force12 = False
   seeding.RemoveFlagged = True
   seeding.RemoveClonesX = True
   #XLAYERS SEARCH
   #1st last layer
   seeding.HighPAlpha = 120.64
   seeding.TolHp = 250.0
   seeding.HighPAlpha_Tight = 120.64
   seeding.TolHp_Tight = 250.0
   #Look for T2
   seeding.x0Corr = 0.0023
   seeding.yOff1 = 1.0
   seeding.yOff1_Loose = 1.5
   seeding.yOff2 = -1.0
   seeding.yOff2_Loose = -1.5
   seeding.maxParabolaSeedHits = 8
   #Look Up remaining 
   seeding.maxDelta_Par = 1.2
   seeding.maxDelta_Par_Loose = 1.4;
   #Look Up remaining and Track Fit
   seeding.dRatio0 = -0.000246
   seeding.dRatio1 = -4.68e-10
   seeding.dRatio2 = -1.25e-11
   seeding.CConst = 2.458e8
   #seeding.OutputLevel = DEBUG
   #Cut On Chi2 in fit/refit/refit X
   seeding.HitSigmaOffCut_X = 3
   seeding.X0Max_X = 650.
   #UV SEARCH
   seeding.AlphaLinearUV = 0.75; #probably also smaller than 0.75
   seeding.DoAsymm = True;
   seeding.TolXStereo = 2700. #good value ? if you don't want to do a y-segmentation
   seeding.TolCoord = 0.25 #good value //it's also a cut on Y
   seeding.TolTySlope = 0.0035 #to be studied
   seeding.TriangleFix = True
   seeding.TriangleFix2nOrder = True
   seeding.RemoveHole = True
   seeding.TolTriangle = 10.
   seeding.TolYOffset = 100000
   seeding.maxChi2HitsX = 6.5
   seeding.maxChi2PerDoF = 8.0
   seeding.TolTyOffset = 0.0035 #Reduce it will reduce ghost rate (0.0035 seems ok)
   #Clone Removal Settings
   seeding.minNCommonX = 1 #in X
   seeding.minNCommonUV = 4 #in UV
   seeding.nUsed = 1 #Clones in X-layers (no effect) comment out
   #Flag Hits Settings
   seeding.Flag_MaxChi2 = 0.5
   seeding.Flag_MaxX0 = 600


   #TruthMatching Settings (Comment all of them if you want to run the normal seeding 
   seeding.DoTuple = False
   seeding.DoHistos = False
   seeding.Cheat = False
   seeding.DoAssociation = False
   seeding.UseSeedTrack = False
   seeding.UseLong = False
   seeding.UseDown = False
   seeding.NoElectrons = False
   seeding.MinPurity = 0.79
   seeding.MinEfficiency = 0.79
   seeding.MinPCut = 0
   
   
   if(ConfigureManager):
      from Configurables import PrFTHitManager
      seeding.addTool(manager)
      #seeding.PrFTHitManager.SizeFix = FixedSize
      #seeding.PrFTHitManager.FixError = True
   seedingSeq.Members = [seeding] 
   

   
appendPostConfigAction(doIt)
NTupleSvc().Output = ["FILE1 DATAFILE='SciFi-Tuple-"+Options+".root' TYP='ROOT' OPT='NEW'"]



#---------------------------------
# Callgrind configuration
#---------------------------------

from Configurables import CallgrindProfile
def addProfile():
      p = CallgrindProfile()
      p.StartFromEventN = 20
      p.StopAtEventN = 80
      p.DumpAtEventN= 80
      p.DumpName = "PRTEST"
      GaudiSequencer("InitBrunelSeq").Members.insert(0,p )



# Now add the profiling algorithm to the sequence
if (CallGrind):
  appendPostConfigAction(addProfile)

